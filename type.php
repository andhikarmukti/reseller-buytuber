<?php
require 'config.php';

$uri_array = explode("/", $_SERVER['REQUEST_URI']);
$nama_jenis = mysqli_real_escape_string($db, ucwords(str_replace('-', ' ', $uri_array[2])));

$sub_judul = 'Topup game terpercaya - ';
require 'lib/header.php';

$promobox = mysqli_query($db, "SELECT * FROM popup WHERE status = '1'");
$promobox = mysqli_fetch_array($promobox);

$cek_slider = mysqli_query($db, "SELECT * FROM slider WHERE status = '1'");
$cek_slider = mysqli_fetch_array($cek_slider);
?>
<?php if($promobox){ ?>
<script>
    var demoImage = '/assets/img/<?=json_decode($promobox['image'],1)[0];?>';
</script>
<script src="<?= $domain; ?>/assets/js/promobox.js?x"></script>
<?php } ?>
<!-- Main Content -->
<div class="main-content">
    <?php if($cek_slider['id']){ ?>
   <div class="banner mt-md-2">
       
      <div id="slider_banner" class="carousel slide" data-ride="carousel">
         <ol class="carousel-indicators">
            <?php
            $db_slider = mysqli_query($db, "SELECT * FROM slider WHERE status='1'");
            $i = 1;
            while ($slider = mysqli_fetch_array($db_slider)) {
            ?>
            <li data-target="#slider_banner" data-slide-to="<?= $i; ?>"></li>
            <?php $i++;
            } ?>
         </ol>
         <div class="carousel-inner">
            <?php
            $db_slider = mysqli_query($db, "SELECT * FROM slider WHERE status='1'");
            $i = 1;
            $active = "active";
            
            while ($slider = mysqli_fetch_array($db_slider)) {
               if($i>1){$active='';}
            ?>
            <div class="carousel-item <?=$active;?>">
               <img src="<?= $domain; ?>assets/img/<?= json_decode($slider['image'], 1)[0]; ?>" class="img-fluid">
            </div>
            <?php $i++;
            } ?>
         </div>
         <a class="carousel-control-prev" href="#slider_banner" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#slider_banner" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
         </a>
      </div>
   </div>
   <?php } ?>
   <div class="">
      <div class="banner bg-dark ml--4 mb-2" style="display: flex;overflow: auto;">
         <?php $i=1;
            $db_jenis = mysqli_query($db, "SELECT * FROM jenis WHERE status='1'");
            while ($jenis = mysqli_fetch_array($db_jenis)) {
               $active = (ucwords(strtolower($jenis['nama'])) == $nama_jenis)  ? 'color:#ff2c69;' : 'color:#858e96;';
               $active_col = (ucwords(strtolower($jenis['nama'])) == $nama_jenis) ? 'border-bottom: 3px solid #ff2c69;' : '';
         ?>
         <div class="col text-center py-3 mt-3 mt-md-0" style="<?=$active_col;?>">
            <a href="/p/<?= strtolower(str_replace(' ', '-', $jenis['nama'])); ?>" style="text-decoration:none;<?=$active;?>" class="">
               <i class="<?=$jenis['icon'];?> mb-2" style="font-size:25px"></i>
               <br>
               <span style="font-size:11px"><?=$jenis['nama'];?></span>
            </a>
         </div>
         <?php $i++; } ?>
      </div>
   </div>
   <?php
      $db_jenis = mysqli_query($db, "SELECT * FROM jenis WHERE status='1' AND nama = '$nama_jenis'");
      while ($jenis = mysqli_fetch_array($db_jenis)) {
   ?>
   <section class="section">
      <div class="section-body">
         <h2 class="section-title"><?=$jenis['nama'];?></h2>
         <div class="list-product">
            <?php
               $db_kategori = mysqli_query($db, "SELECT * FROM kategori WHERE id_jenis = '$jenis[id]' AND status='1'");
               while ($kategori = mysqli_fetch_array($db_kategori)) {
                  $layanan = mysqli_query($db, "SELECT * FROM layanan WHERE id_kategori = '$kategori[id]' AND status='1' ORDER BY harga ASC");
                  $layanan = mysqli_fetch_array($layanan);
               ?>
               <div class="product">
                  <div class="card-game bg-dark">
                     <div class="card-image">
                        <img src="<?= $domain; ?>assets/img/kategori/<?= json_decode($kategori['gambar'], 1)[0]; ?>" class="img-product">
                     </div>
                     <div class="card-title text-truncate text-white">
                        <?= $kategori['nama']; ?>
                     </div>
                     <div class="card-subtitle text-truncate" style="color: #acacac">
                        <?= $kategori['sub_nama']; ?>
                     </div>
                     <div class="card-link px-md-3 pb-2">
                        <a style="background-color: #ff2c69; color: #FFFFFF" class="btn btn-sm btn-default btn-block stretched-link" href="<?= $domain; ?>beli/<?= strtolower(str_replace(' ', '-', $kategori['nama'])); ?>">Top Up</a>
                     </div>
                  </div>
               </div>
            <?php $i++;
               } ?>
         </div>

      </div>
   </section>
   <?php } ?>
</div>
<?php
   require 'lib/footer.php';
   ?>

<style>
   .carousel-control-next:hover, .carousel-control-prev:hover{
      background-color: #000;
      opacity: 20%;
   }

</style>