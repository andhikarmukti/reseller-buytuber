<?php
require 'config.php';
$sub_judul = "Kebijakan Privasi | ";
require 'lib/header.php';
$privasi = mysqli_query($db, "SELECT * FROM pengaturan");
$data_privasi = mysqli_fetch_array($privasi);
?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <h2 class="section-title">Kebijakan Privasi</h2>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="card-group">
              <?=$data_privasi['privacy']?>
              </div>
            </div>
          </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>