<?php

class Api {
    /** @var false|mixed|object|stdClass|User */
    private $user;
    private mysqli $db;
    private array $level = ['pro', 'gold', 'silver'];

    /**
     * @throws Exception
     */
    public function __construct($db) {
        $this->db = $db;
        $apiKey = mysqli_real_escape_string($db, apache_request_headers()['api_key'] ?: $_POST['key']);
        $user = mysqli_query($db, "SELECT * FROM users u WHERE api_key = '${apiKey}'")->fetch_object(User::class);
        if (!isset($user)) {
            http_response_code(401);
            echo json_encode(['Unauthorized']);
            die();
        } else {
            $this->user = $user;
        }
    }

    public function index(): array {
        return ['title' => 'Jasa Viral V1.0', 'user' => $this->user];
    }

    public function order($serviceId, $link, $target = null): array {
        if (!isset($link)) {
            return ['success' => false, 'message' => 'link is required'];
        }
        $query = mysqli_query($this->db, "SELECT * FROM layanan WHERE id = $serviceId AND status = 1");
        if ($query) $service = $query->fetch_object(Service::class);
        if (!isset($service)) {
            return ['status' => 'error', 'message' => 'id service not found'];
        } elseif ($this->getPrice($service) <= 0) {
            return ['success' => false, 'message' => 'id service not valid'];
        } elseif ($this->user->saldo < $this->getPrice($service)) {
            return ['success' => false, 'message' => 'not enough balance'];
        } else {
            $category = mysqli_query($this->db, "SELECT * FROM kategori WHERE id = '$service->id_kategori' AND status='1'")->fetch_object(Category::class);
            if ($service->id_kategori == 1) {
                $targetData = $target ? $link . '(' . $target . ')' : $link;
            } else {
                $targetData = $target ? $link . '|' . $target : $link;
            }
            $profit = $this->getPrice($service) - $service->harga_asli;
            $invoiceCode = "ORDER" . time() . str_rand(3);
            $date = date('Y-m-d H:i:s');
            mysqli_query($this->db, "UPDATE users SET saldo = saldo - {$this->getPrice($service)} WHERE id = {$this->user->id}");
            mysqli_query($this->db, "INSERT INTO pesanan VALUES(null,
                    '{$this->user->id}',
                    '{$service->id_kategori}', 
                    '{$service->id}',
                    '{$service->id_provider}',
                    '{$this->user->nama}',
                    '{$service->nama}',
                    '{$category->nama}', 
                    '{$this->user->ponsel}',
                    '$targetData', 
                    '',
                    '{$this->getPrice($service)}',
                    '$profit',
                    '2',
                    '0',
                    '',
                    '0',
                    '$invoiceCode',
                    'processing',
                    '$date',
                    '',
                    '0'
                )");
            return ['success' => true, 'order' => mysqli_insert_id($this->db), 'invoice' => $invoiceCode];
        }
    }

    public function status($orderId): array {
        if (!isset($orderId)) {
            return ['success' => false, 'message' => 'order is required'];
        }
        $query = mysqli_query($this->db, "SELECT * FROM pesanan WHERE id = $orderId AND id_user = {$this->user->id}");
        if ($query) $order = $query->fetch_object(Order::class);
        if (!isset($order)) {
            return ['success' => false, 'message' => 'order id not found'];
        }
        return ['success' => true, 'status' => $order->status, 'refund' => $order->refund, 'reorder' => $order->reorder];
    }

    public function services(): array {
        $query = mysqli_query($this->db, "SELECT l.*, k.nama AS category_name FROM layanan l 
    INNER JOIN kategori k ON k.id = l.id_kategori  WHERE l.status = 1");
        $response = [];
        if ($query)
            while ($service = $query->fetch_object(Service::class)) {
                $response[] = [
                    'service' => $service->id,
                    'rate' => $this->getPrice($service),
                    'name' => $service->nama,
                    'category' => $service->category_name,
                ];
            }
        return [
            'success' => true,
            'data' => $response
        ];
    }

    private function getPrice(Service $service): int {
        $price = $service->harga;
        if (in_array($this->user->level, $this->level)) {
            $price = $service->{"harga_{$this->user->level}"};
        }
        return $price;
    }
}

class User {
    public int $id;
    public string $nama;
    public string $email;
    public string $ponsel;
    public int $saldo;
    public string $level;
    public int $status;
    public string $tanggal;
    public string $api_key;
}

class Service {
    public int $id;
    public int $id_kategori;
    public int $id_provider;
    public string $id_oid;
    public string $nama;
    public int $harga;
    public int $harga_silver;
    public int $harga_gold;
    public int $harga_pro;
    public int $harga_asli;
    public string $type_profit;
    public int $profit;
    public int $profit_silver;
    public int $profit_gold;
    public int $profit_pro;
    public int $status;
    public string $tanggal;
}

class Category {
    public int $id;
    public int $id_jenis;
    public string $nama;
    public string $sub_nama;
    public string $detail;
    public string $gambar;
    public string $bantuan;
    public int $status;
    public int $tipe_form;
}

class Order {
    public int $id;
    public int $id_user;
    public int $id_kategori;
    public int $id_layanan;
    public int $id_provider;
    public string $nama;
    public string $nama_layanan;
    public string $nama_kategori;
    public string $kontak;
    public string $data;
    public string $keterangan;
    public int $harga;
    public int $keuntungan;
    public string $metode_pembayaran;
    public string $id_pembayaran;
    public string $res_pembayaran;
    public int $refund;
    public string $invoice;
    public string $status;
    public string $tanggal;
    public string $res_provider;
    public int $reorder;
}