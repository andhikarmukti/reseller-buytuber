<?php
require 'config.php';
$sub_judul = 'Topup Saldo | ';
require 'lib/redirect_not_login.php';
require 'lib/order_config.php';
require 'lib/header.php';
?>
<?php if($web_data['recaptcha_order_status']){ ?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<style type="text/css">
  .grecaptcha-badge {
    width: 70px !important;
    overflow: hidden !important;
    transition: all 0.3s ease !important;
    left: 4px !important;
  }
  
  .grecaptcha-badge:hover {
    width: 256px !important;
  }
</style>
<?php } ?>
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <h2 class="section-title">Topup Saldo</h2>
            <form method="POST" id="topupSaldo">
                <input type="hidden" name="layanan" value="saldo">
                <input type="hidden" name="kategori" value="saldo">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Jumlah Saldo</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon3">Rp</span>
                                        </div>
                                        <input type="number" class="form-control" placeholder="Masukan jumlah saldo"
                                            aria-label="" name="id" id="jumlah_saldo" required autocomplete="false">
                                    </div>
                                </div>
                                <div class="form-groub mb-3">
                                    <?php
                                    $db_metode = mysqli_query($db, "SELECT * FROM metode_pembayaran WHERE status='1' AND id NOT IN (1,2)"); $i=1;
                                    while ($metode = mysqli_fetch_array($db_metode)) {
                                ?>
                                    <input required type="radio" id="method_<?=$i;?>" class="radio-pembayaran"
                                        name="metode" value="<?=$metode['id'];?>">
                                    <label for="method_<?=$i;?>">
                                        <!--<i class="fas fa-check-square position-absolute" style="font-size: 15px;"></i>-->
                                        <div class="row py-3 mx-auto">
                                            <div class="col-6"><img
                                                    src="<?=$domain;?>assets/img/<?=json_decode($metode['gambar'],1)[0];?>"
                                                    width="100px"></div>
                                            <div class="col-6 text-right">
                                                Harga<br>
                                                <b id="<?=str_replace(' ', '_', $metode['nama']);?>">Rp 0</b>
                                            </div>
                                        </div>
                                    </label>
                                    <?php if($metode['pake_rek']) { ?>
                                    <div class="rek" id="rek_<?=$metode['id'];?>" style="display: none;"><input
                                            type="text" class="form-control mt-1 mb-3"
                                            placeholder="<?=ucwords($metode['keterangan_rek']);?>" name="rek"></div>
                                    <?php } ?>
                                    <?php $i++;}?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mb-5">
                        <button class="<?=($web_data['recaptcha_order_status']) ? 'g-recaptcha':'';?> btn btn-primary" type="submit" data-sitekey="<?=$web_data['recaptcha_order_site_key'];?>" data-callback="submitForm"><i class="fas fa-wallet"></i> Topup Sekarang</button>
                    </div>
                </div>
            </form>
    </section>
</div>
<?php
require 'lib/footer.php';
?>
<?=$msg_js;?>
<script>
function submitForm() {
    document.getElementById('topupSaldo').submit();
}
$(document).ready(function() {
    $("input[name$='metode']").click(function() {
        var test = $(this).val();
        $("div.rek").hide();
        $("#rek_" + test).show();
    });
    $('input[type=number][name=id]').keyup(function() {
        var jumlah_saldo = $('#jumlah_saldo').val();
        if (jumlah_saldo) {
            $.ajax({
                url: "<?=$domain;?>ajax/price_topup.php",
                type: "POST",
                dataType: 'json',
                data: {
                    jumlah_saldo: jumlah_saldo,
                    //qty: qty
                },
                success: function(data) {
                    if (data.status == true) {
                        $.each(data.data, function(i, item) {
                            $('#' + item.nama).html(item.harga);
                        });
                    } else {

                    }
                },
                error: function(jqXHR, exception) {

                }
            });
        }
    });
});
</script>