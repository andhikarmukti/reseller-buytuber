<?php
require 'xendit.php';
if ($_POST['layanan']) {
    
    if($web_data['recaptcha_order_status']){
        $recaptcha = recaptcha_invisible($web_data['recaptcha_order_secret_key'], $_POST['g-recaptcha-response']);
    }else{
        $recaptcha = true;
    }
    
    if($recaptcha){
        $id = mysqli_real_escape_string($db, $_POST['layanan']);
        $id_kategori = mysqli_real_escape_string($db, $_POST['kategori']);
        $metode_id = mysqli_real_escape_string($db, $_POST['metode']);
        if($id_kategori == 1) {
            $data_id = ($_POST['other_id']) ? $_POST['id'].'('.$_POST['other_id'].')' : $_POST['id'];
        } else {
           $data_id = ($_POST['other_id']) ? $_POST['id'].'|'.$_POST['other_id'] : $_POST['id']; 
        }
        
        
        // fix htmlspecial $data_db $kontak_db
        $data_id = mysqli_real_escape_string($db, htmlspecialchars($data_id));
        $kontak = mysqli_real_escape_string($db, htmlspecialchars($_POST['kontak']));
        
        $rek = mysqli_real_escape_string($db, $_POST['rek']);
        
        if ($id && $id_kategori && $metode_id && $data_id) {        
			$run_order = false; // 20220317
            $db_layanan = mysqli_query($db, "SELECT * FROM layanan WHERE id = '$id' AND id_kategori = '$id_kategori' AND status='1'");
            $layanan = mysqli_fetch_array($db_layanan);
            
    
            $db_kategori = mysqli_query($db, "SELECT * FROM kategori WHERE id = '$id_kategori' AND status='1'");
            $kategori = mysqli_fetch_array($db_kategori);
    
            $db_metode = mysqli_query($db, "SELECT * FROM metode_pembayaran WHERE id = '$metode_id' AND status='1'");
            $metode = mysqli_fetch_array($db_metode);
    
            $kode_invoice = "ORDER".time().str_rand(3);
    
            $harga = ($layanan['harga']) ? $layanan['harga'] : 0;
            $harga = ($data_user['level'] == 'silver') ? $layanan['harga_silver'] : $harga;
            $harga = ($data_user['level'] == 'gold') ? $layanan['harga_gold'] : $harga;
            $harga = ($data_user['level'] == 'pro') ? $layanan['harga_pro'] : $harga;
            
            $keuntungan = $layanan['harga'] - $layanan['harga_asli'];
    
            if($id == 'saldo'){
                $kategori['id'] = 0;
                $layanan['id'] = 0;
                $layanan['id_provider'] = 0; // fix id_provider gabisa kosong
                $layanan['nama'] = mysqli_real_escape_string($db, 'Saldo Rp '.htmlspecialchars($data_id)); // 20220317
                $kategori['nama'] = 'Topup Saldo';
                $harga = ($data_id) ? $data_id : 0;
                $harga = mysqli_real_escape_string($db, $harga); // 20220317
                
                //fix saldo
                if(is_numeric($data_id) == false){ header('Location: '.$domain); die('tidak di izinkan saldo e1'); }
                if(preg_match('#[^0-9]#',$data_id)){ header('Location: '.$domain); die('tidak di izinkan saldo e2'); }
            }
    
            $rate_persen = ($metode['rate_persen'] > 0) ? ($metode['rate_persen']/100) * $harga : 0;
            $harga = ($metode['rate'] > 0) ? ($harga * $metode['rate']) + $rate_persen : $harga + $rate_persen;
            $harga = $harga + $metode['fee'];
            $id_user = ($data_user['id']) ? $data_user['id'] : 0;
            $nama = ($data_user['nama']) ? $data_user['nama'] : 'Guest';
          
            $nama = mysqli_real_escape_string($db, $nama); // 20220317
    
            $status = 'not_paid';
            $id_pembayaran = 0;
            $res_pembayaran = '';
            
            // metode saldo
            if($metode_id == 2){
                $run_order = false;
                if ($id_user == 0) {
                    $msg = 'Anda tidak dizinkan menggunakan metode ini, mohon login terlebih dahulu';
                }else if($id_user && $harga > $data_user['saldo']){ 
                    $msg = 'Saldo tidak cukup';
                }else if($id_user && $data_user['saldo'] >= $harga){
                    $update_saldo = mysqli_query($db, "UPDATE users SET saldo = saldo - $harga WHERE id = '$id_user'");
                    mysqli_query($db, "INSERT INTO whatsapp_gateway VALUES(null, '".$kode_invoice."', 'pending', 'pending', '$date')");
                    if($update_saldo){
                        $data_user_new = mysqli_query($db, "SELECT * FROM users WHERE id = '$id_user'");
                        $data_user_new = mysqli_fetch_array($data_user_new);
                        $total_saldo_cek = $data_user['saldo'] - $harga;
                        if($total_saldo_cek == $data_user_new['saldo'] && $data_user['saldo'] != $data_user_new['saldo']){
                            $run_order = true;
                            $status = 'pending';
                            if($id == 'saldo'){
                                $run_order = false;
                                $status = 'cancel';
                                $msg = 'Penggunaan saldo tidak diperbolehkan';
                            }
                        }else{
                            $msg = 'Penggunaan saldo sedang error, mohon coba lagi';
                        }
                    }else{
                        $msg = 'Penggunaan saldo sedang error';
                    }
                }else{
                    $msg = 'Metode Saldo sedang error';
                }
            }
            // BCA
            if($metode_id == 4){
              //  $validasi_fee = mysqli_query($db, "SELECT * FROM pesanan WHERE status = 'not_paid'");
                $harga = $harga + rand(000, 999);
                $run_order = true;
            }
            
    
            $config_metode = json_decode($metode['config'],1);
            if($config_metode['provider'] == 'xendit'){
                $payment = new xendit;
                $payment->api_key = $config_metode['api_key'];
                $payment->url_callback = $domain.'payment/xendit?ewallet';
                if($metode['id'] == 3){ $payment->url_callback = $domain.'payment/xendit?qrcode'; $run_payment_ = $payment->qrcode($kode_invoice, $harga);}
                if($metode['id'] == 5){ $payment->url_callback = $domain.'payment/xendit?retail'; $run_payment_ = $payment->alfamart($kode_invoice, $harga, $nama); }
                if($metode['id'] == 6){ 
                    if($rek){
                        $run_payment_ = $payment->ovo($kode_invoice, $harga, $rek);
                    }else{
                        $msg = 'Mohon '.$metode['keterangan_rek'];
                    }
                }
                if($metode['id'] == 7){ $run_payment_ = $payment->dana($kode_invoice, $harga); }
                if($metode['id'] == 8){ $run_payment_ = $payment->shopeepay($kode_invoice, $harga); }
                if($metode['id'] == 9){ $run_payment_ = $payment->linkaja($kode_invoice, $harga); }
                                    if($metode['id'] == 10){ $run_payment_ = $payment->bri($kode_invoice, $harga); }
            if($metode['id'] == 11){ $run_payment_ = $payment->bni($kode_invoice, $harga); }
            if($metode['id'] == 12){ $run_payment_ = $payment->permata($kode_invoice, $harga); }
            if($metode['id'] == 13){ $run_payment_ = $payment->mandiri($kode_invoice, $harga); }
            if($metode['id'] == 14){ $run_payment_ = $payment->bsi($kode_invoice, $harga); }
                $run_payment = json_decode($run_payment_,1);
                if($run_payment['id']){
                    $run_order = true;
                    $id_pembayaran = $run_payment['id'];
                    $res_pembayaran = $run_payment_;
                }else{
                    $run_order = false;
                }
            }
    
            if($metode['min_jumlah'] > $harga){
                $run_order = false;
                $msg = 'Minimum pembayaran dalam metode ini adalah Rp '.$metode['min_jumlah'];
            }
            if(!$layanan['nama'] || $harga == 0 || !$kategori['nama'] || !$metode['id']){
                $run_order = false;
                $msg = 'Form tidak lengkap';
            }
          	//20220317
            if($harga <= 0){
                $run_order = false;
                $msg = 'Layanan tidak diperbolehkan';
            }
          
            // fix saldo
            if($id == 'saldo'){
                if(is_numeric($data_id) == false){
                    $run_order = false;
                    $msg = 'Hanya dapat di isi angka';
                }else if(preg_match('#[^0-9]#',$data_id)){
                    $run_order = false;
                    $msg = 'Hanya dapat di isi angka';
                }
            }
            
            $msg = ($run_order == false && $msg == '') ? 'Mohon periksa kembali formulir anda' : $msg;
            if ($run_order == true) {
                $query = mysqli_query($db, "INSERT INTO pesanan VALUES(null,
                    '$id_user',
                    '$kategori[id]', 
                    '$layanan[id]',
                    '$layanan[id_provider]',
                    '$nama',
                    '$layanan[nama]',
                    '$kategori[nama]', 
                    '$kontak',
                    '$data_id', 
                    '',
                    '$harga',
                    '$keuntungan',
                    '$metode_id',
                    '$id_pembayaran',
                    '".base64_encode($res_pembayaran)."',
                    '0',
                    '$kode_invoice',
                    '$status',
                    '$date',
                    '',
                    '0'
                )");
                header('Location: '.$domain.'inv/'.$kode_invoice);
            }else{
                $msg_js = "<script>swal('$msg', '', 'error');</script>";
            }
        }else{
            $msg_js = "<script>swal('Mohon lengkapi semua form', '', 'error');</script>";
        }
    }else{
        $msg_js = "<script>swal('Anda terdeteksi bot / spam, mohon coba lagi', '', 'error');</script>";
    }
}