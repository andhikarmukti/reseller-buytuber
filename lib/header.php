<!DOCTYPE html>
<html lang="en">

<head>
  <title><?=htmlspecialchars($sub_judul.$judul);?></title>

  <!-- General Meta -->
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="robots" content="index, follow" />
  
  <!-- Primary Meta Tags -->
  <meta name="title" content="<?=htmlspecialchars($sub_judul.$judul);?>">
  <meta name="description" content="<?=$web_data['deskripsi_meta'];?>">
  <meta name="keywords" content="<?=$web_data['keywords'];?>">
  <meta name="author" content="<?=$web_data['author'];?>">



  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/fontawesome/css/all.min.css">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/bs-stepper/dist/css/bs-stepper.min.css">

  <!-- Favicon -->
  <link rel="icon" href="<?=$domain;?>assets/img/<?=$favfav['0'];?>" type="image/png">
  <!-- Fonts -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="<?=$domain;?>assets/css/style_dark.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/css/components_dark.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/css/kiosku.css?xd">
  <link rel="stylesheet" href="<?=$domain;?>assets/css/menu.css?xd">
 <link rel="stylesheet" href="<?=$domain;?>assets/css/orderForm.css?<?=time();?>">
  <link rel="stylesheet" href="<?=$domain;?>assets/css/payment.css?<?=time();?>">

</head>
<div class="preloader" style="display: none;">
  <div class="loading">
    <img src="<?=$domain;?>assets/img/loader.gif" width="80">
  </div>
</div>
<body class="layout-3">
  <div id="app">
    <div class="main-wrapper container">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <div class="container">

          <a href="<?=$domain;?>" class="navbar-brand sidebar-gone-hide">
            <img src="<?=$domain;?>assets/img/<?=$logo['0'];?>" class="img-fluid d-none d-md-block" width="160px">
            <img src="<?=$domain;?>assets/img/<?=$logo['0'];?>" class="img-fluid d-lg-none d-md-inline-block" width="160px">
          </a>
          <a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars mt-2"></i></a>
          <a href="<?=$domain;?>" class="d-lg-none d-md-inline-block"><img src="<?=$domain;?>assets/img/<?=$logo['0'];?>" class="img-fluid" width="155px"></a>

          <ul class="navbar-nav navbar-right">
            <?php if($is_login) { ?>
            <li class="dropdown"><a href="#" data-toggle="dropdown"
                class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <img alt="image" src="<?=$domain;?>assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
                <div class="d-sm-none d-lg-inline-block">Hi, <?=explode(' ', htmlspecialchars($data_user['nama']))[0];?></div>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <a href="<?=$domain;?>user" class="dropdown-item has-icon">
                  <i class="far fa-user"></i> Profil
                </a>
                <a href="<?=$domain;?>pengaturan" class="dropdown-item has-icon">
                  <i class="fas fa-cog"></i> Pengaturan
                </a>
                <div class="dropdown-divider"></div>
                <a href="<?=$domain;?>logout" class="dropdown-item has-icon text-danger">
                  <i class="fas fa-sign-out-alt"></i> Logout
                </a>
              </div>
            </li>
            <?php }else{ ?>
            <li class="dropdown">
              <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <span class="rounded-circle mr-1"> <i class="fas fa-sign-in-alt"></i> </span>
                <div class="d-sm-none d-lg-inline-block">Masuk/Daftar</div>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <a href="<?=$domain;?>login" class="dropdown-item has-icon">
                  <i class="fas fa-sign-in-alt"></i> Masuk
                </a>
                <a href="<?=$domain;?>register" class="dropdown-item has-icon">
                  <i class="fas fa-user-plus"></i> Buat Akun
                </a>
              </div>
            </li>
            <?php } ?>
          </ul>
        </div>
      </nav>

      <nav class="navbar navbar-secondary navbar-expand-lg">
        <div class="container">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a href="<?=$domain;?>" class="nav-link"><i class="fas fa-home"></i><span>Beranda</span></a>
            </li>
            <li class="nav-item">
              <a href="<?=$domain;?>search" class="nav-link"><i class="fas fa-search"></i><span>Periksa Pesanan</span></a>
            </li>
            <li class="nav-item">
              <a href="<?=$domain;?>services" class="nav-link"><i class="fas fa-list"></i><span>Daftar Layanan</span></a>
            </li>
          </ul>
        </div>
      </nav>