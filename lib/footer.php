<?php
$footer = mysqli_query($db, "SELECT * FROM pengaturan");
$data_footer = mysqli_fetch_array($footer);
?>

<div class="fab-container">
    <div class="fab fab-icon-holder bg-danger">
        <i class="fas fa-comment"></i>
    </div>
    <ul class="fab-options">
        <li>
            <a href="https://instagram.com/<?=$data_footer['instagram']; ?>" target="_blank">
                <div class="fab-icon-holder" style="background: radial-gradient(circle farthest-corner at 35% 90%, #fec564, transparent 50%), radial-gradient(circle farthest-corner at 0 140%, #fec564, transparent 50%), radial-gradient(ellipse farthest-corner at 0 -25%, #5258cf, transparent 50%), radial-gradient(ellipse farthest-corner at 20% -50%, #5258cf, transparent 50%), radial-gradient(ellipse farthest-corner at 100% 0, #893dc2, transparent 50%), radial-gradient(ellipse farthest-corner at 60% -20%, #893dc2, transparent 50%), radial-gradient(ellipse farthest-corner at 100% 100%, #d9317a, transparent), linear-gradient(#6559ca, #bc318f 30%, #e33f5f 50%, #f77638 70%, #fec66d 100%);">
                    <i class="fab fa-instagram ml-0"></i>
                </div>
            </a>
        </li>
        <li>
            <a href="https://api.whatsapp.com/send?phone=<?=$data_footer['whatsapp']; ?>" target="_blank">
                <div class="fab-icon-holder" style="background-color: #25D366;">
                    <i class="fab fa-whatsapp ml-0"></i>
                </div>
            </a>
        </li>
        <!-- <li>
            <a href="https://www.facebook.com/<?=$data_footer['facebook']; ?>" target="_blank">
                <div class="fab-icon-holder" style="background-color: #3b5998;">
                    <i class="fab fa-facebook-f ml-0"></i>
                </div>
            </a>
        </li>
        -->
    </ul>
    <div href="#" class="act-btn-top bg-dark mt-2" onclick="toTop()" style="display: none; bottom: 19px;">
        <i class="fas fa-angle-up ml-0" style="font-size: 30px;"></i>
    </div>
</div>

</div>
</div>

<div class="footer">
    <div class="container">
        <div class="row pt-5">
            <div class="col-md-4 col-12 mb-4">
               <h2 class="text-center text-md-left"><img src="<?=$domain;?>assets/img/<?=$logo['0'];?>" class="img-fluid mb-3" width="150px"></h2>
                <p class="text-center text-md-left"> <?= $data_footer['deskripsi_footer'] ?>
                                  <div class="row text-center text-md-left">
                 <div class="col-12 mb-2">
                   <p class="text-center text-md-left"> Temukan Kami</p>
                    <a href="https://instagram.com/<?=$data_footer['instagram']; ?>" rel="noreferrer" target="_blank" data-cy="instagram-icon"><img src="<?= $domain; ?>assets/img/ig.svg" alt="Instagram" width="24" height="24" loading="lazy">&nbsp;</a>
                    <a href="https://api.whatsapp.com/send?phone=<?=$data_footer['whatsapp']; ?>" rel="noreferrer" target="_blank" data-cy="whatsapp-icon"><img src="<?= $domain; ?>assets/img/wa.png" alt="wa" width="24" height="24" loading="lazy">&nbsp;</a>
    
                  <!--  
                  <a href="https://www.facebook.com/<?=$data_footer['facebook']; ?>" rel="noreferrer" target="_blank" data-cy="facebook-icon"><img src="<?= $domain; ?>assets/img/fb.svg" alt="facebook" width="24" height="24" loading="lazy">&nbsp;</a>
                  <a href="#" rel="noreferrer" target="_blank" data-cy="tiktok-icon"><img src="<?= $domain; ?>assets/img/tiktok.svg" alt="Tiktok" width="24" height="24" loading="lazy">&nbsp;</a>
                   <a href="#" rel="noreferrer" target="_blank" data-cy="youtube-icon"><img src="<?= $domain; ?>assets/img/yt.svg" alt="Youtube" width="24" height="24" loading="lazy">&nbsp;</a>
                  -->
                  </div>
            </div>
                </p>

          </div>
            <div class="col-md-4 col-12 mb-4">
                <h3 class="text-center text-md-left">Peta Situs</h3>
                <div class="row text-center text-md-left">
                    <div class="col-12 mb-2">
                        <a href="<?= $domain; ?>about" class="text-white">Tentang</a>
                    </div>
                    <div class="col-12 mb-2">
                        <a href="<?= $domain; ?>privacy" class="text-white">Kebijakan Privasi</a>
                    </div>
                    <div class="col-12 mb-2">
                        <a href="<?= $domain; ?>faq" class="text-white">FAQ</a>
                    </div>
                    <div class="col-12 mb-3">
                        <a href="<?= $domain; ?>terms" class="text-white">Ketentuan Layanan</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-12 mb-3">
                <h3 class="text-center text-md-left">Pembayaran</h3>
                <marquee>
                  <img src="<?= $domain; ?>assets/img/bca_footer.png" width="80px" class="ml-3 bg-white p-1">
                  <img src="<?= $domain; ?>assets/img/linkaja_footer.png" width="80px" class="ml-3 bg-white p-1">
                  <img src="<?= $domain; ?>assets/img/shopay_footer.png" width="80px" class="ml-3 bg-white p-1">
                  <img src="<?= $domain; ?>assets/img/ovo_footer.png" width="80px" class="ml-3 bg-white p-1">
                  <img src="<?= $domain; ?>assets/img/gopay_footer.png" width="80px" class="ml-3 bg-white p-1">
                  <img src="<?= $domain; ?>assets/img/dana_footer.png" width="80px" class="ml-3 bg-white p-1">
                  <img src="<?= $domain; ?>assets/img/qris_footer.png" width="80px" class="ml-3 bg-white p-1">
                  <img src="<?= $domain; ?>assets/img/indomaret_footer.png" width="80px" class="ml-3 bg-white p-1">
              </marquee>
            </div>
            <footer class="main-footer">
                <div class="text-center">
                    Copyright &copy; 2022 <a href="<?= $domain; ?>" class="text-white">Jasaviral</a>
                </div>
            </footer>
        </div>
    </div>



    <!-- General JS Scripts -->
    <script src="<?= $domain; ?>assets/modules/jquery.min.js"></script>
    <script src="<?= $domain; ?>assets/modules/popper.js"></script>
    <script src="<?= $domain; ?>assets/modules/tooltip.js"></script>
    <script src="<?= $domain; ?>assets/modules/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= $domain; ?>assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="<?= $domain; ?>assets/modules/moment.min.js"></script>
    <script src="<?= $domain; ?>assets/js/stisla.js"></script>

    <!-- JS Libraies -->
    <script src="<?= $domain; ?>assets/modules/datatables/datatables.min.js"></script>
    <script src="<?= $domain; ?>assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?= $domain; ?>assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
    <script src="<?= $domain; ?>assets/modules/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?= $domain; ?>assets/modules/sweetalert/sweetalert.min.js"></script>
    <script src="<?= $domain; ?>assets/modules/bs-stepper/dist/js/bs-stepper.min.js"></script>


    <!-- Page Specific JS File -->
    <script src="<?= $domain; ?>assets/js/page/modules-datatables.js"></script>

    <!-- Template JS File -->
    <script src="<?= $domain; ?>assets/js/scripts.js"></script>
    <script src="<?= $domain; ?>assets/js/custom.js"></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        $(document).ready(function() {
            setTimeout(function() {
                $(".preloader").fadeOut();
            }, 200);
        });
    </script>
    <script>
        mybutton = document.querySelector(".act-btn-top");
        window.onscroll = function() {
            scrollFunc()
        };

        function scrollFunc() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }

        function toTop() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
      

    </script>
    </body>

    </html>