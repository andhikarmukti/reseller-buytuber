<?php
function curl($url, $data=0, $time=0, $header=array())
{
    $a = curl_init();
    curl_setopt($a, CURLOPT_URL, $url);
    curl_setopt($a, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($a, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($a, CURLOPT_FOLLOWLOCATION, 1);
    if ($time) {curl_setopt($a, CURLOPT_TIMEOUT, $time);}
    if ($data) {
        curl_setopt($a, CURLOPT_POST, 1);
        curl_setopt($a, CURLOPT_POSTFIELDS, $data);
    }
    if($header){
        curl_setopt($a, CURLOPT_HTTPHEADER, $header);
    }
    $b = curl_exec($a);
    return $b;
}
function str_rand($length = 10) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function send_pesan($url,$api_key,$device_id,$nomor,$pesan){
	$data = "api_key=$api_key&sender=$device_id&number=$nomor&message=$pesan";
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    $output = curl_exec($ch); 
    curl_close($ch);      

    return $output;
}

function send_pesan_grup($url,$api_key,$device_id,$nomor,$pesan){
	$data = "key=$api_key&device=$device_id&group_id=$nomor&message=$pesan";
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    $output = curl_exec($ch); 
    curl_close($ch);      

    return $output;
}

function indoproof($domain, $invoice, $kategori, $gambar_kategori, $layanan, $id_layanan){
    $url = 'https://indoproof.com/pixel-webhook/886d84e4d978bd8324d54cc9fb23b5a6';
    
    $invoice_count = strlen($invoice) - 13;
    $invoice = substr_replace($invoice, str_repeat('*', $invoice_count), 13, $invoice_count);
    
    $data['invoice'] = $invoice;
    $data['kategori'] = $kategori;
    $data['layanan'] = $layanan;
    $data['img'] = $domain.'/assets/img/kategori/'.json_decode($gambar_kategori,1)[0];
    $data['link'] = strtolower(str_replace(' ', '-', $kategori)).'/'.$id_layanan;
    
    $json = json_encode($data);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close ($ch);
    
    return true;
}

function recaptcha_invisible($secret_key, $response){
    $url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$response;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    $data = curl_exec($curl);
    curl_close($curl);
    $responseCaptchaData = json_decode($data);
    if($responseCaptchaData->success) {
        return true;
    } else {
        return false;
    }
}