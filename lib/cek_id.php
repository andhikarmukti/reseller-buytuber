<?php 
class cek_id {
  public function curl($url, $data=0, $time=0)
  {
      $a = curl_init();
      curl_setopt($a, CURLOPT_URL, $url);
      curl_setopt($a, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($a, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($a, CURLOPT_FOLLOWLOCATION, 1);
      if ($time) {curl_setopt($a, CURLOPT_TIMEOUT, $time);}
      if ($data) {
          curl_setopt($a, CURLOPT_POST, 1);
          curl_setopt($a, CURLOPT_POSTFIELDS, $data);
      }
      $b = curl_exec($a);
      return $b;
  }
  public function sausage_man($id){
    $id = explode('|', $id);
    $url = 'https://xdg-hk.xd.com/api/v1/user/get_role?client_id=zuRsHFfcY2KtVql3&server_id=global-release&character_id='.$id[0];
    $page = $this->curl($url);
    $array = json_decode($page,1);
    return $array['data']['name'];
  }
  public function mobile_legends_x($id){
    $id = explode('|', $id);
    $url = 'https://shopee.co.id/digital-product/api/item/mobile_legends/nickname?account='.$id[0].'%28'.$id[1].'%29&item_id=100459';
    $page = $this->curl($url);
    $array = json_decode($page,1);
    return $array['data']['nickname'];
  }
  public function mobile_legends($id) {
    $data = 'kategori=ml&userid='.$id.'';
    $page = $this->curl('https://bisacash.com/api/cek_id', $data);
    $array = json_decode($page,1);
    return $array['data']['username'];
  }
  public function free_fire($id){
    $id = explode('|', $id);
    $url = 'https://shopee.co.id/digital-product/api/item/game-account/validation?account='.$id[0].'&item_id=100434';
    $page = $this->curl($url);
    $array = json_decode($page,1);
    return $array['data']['user_name'];
  }
  public function hago($id){
    $id = explode('|', $id);
    $url = 'https://shopee.co.id/digital-product/api/item/game-account/validation?account='.$id[0].'&item_id=101616';
    $page = $this->curl($url);
    $array = json_decode($page,1);
    return $array['data']['user_name'];
  }
  public function aov($id){
    $id = explode('|', $id);
    $url = 'https://shopee.co.id/digital-product/api/item/game-account/validation?account='.$id[0].'&item_id=100441';
    $page = $this->curl($url);
    $array = json_decode($page,1);
    return $array['data']['user_name'];
  }
  public function point_blank($id){
    $id = explode('|', $id);
    $url = 'https://shopee.co.id/digital-product/api/item/game-account/validation?account='.$id[0].'&item_id=102387';
    $page = $this->curl($url);
    $array = json_decode($page,1);
    return $array['data']['user_name'];
  }
  public function lords_mobile($id){
    $id = explode('|', $id);
    $url = 'https://shopee.co.id/digital-product/api/item/game-account/validation?account='.$id[0].'&item_id=102373';
    $page = $this->curl($url);
    $array = json_decode($page,1);
    return $array['data']['user_name'];
  }
    public function digiflazz_pln($id){
    $url = 'https://api.digiflazz.com/v1/transaction';
    $data['commands'] = 'pln-subscribe';
    $data['customer_no'] = $id;
    $header = array('Content-Type: application/json');
    $page = $this->curl($url, json_encode($data), 0, $header);
    $array = json_decode($page,1);
   // print_r($page);
    return $array['data']['name'] .' - '.$array['data']['segment_power'];
  }
}

//example
$cek_id = new cek_id;
//$user_id = '108541987';
//$get = $cek_id->free_fire($user_id);
//print_r($get);