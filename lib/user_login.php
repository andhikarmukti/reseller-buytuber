<?php
if ($_POST) {
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $password = mysqli_real_escape_string($db, $_POST['password']);
    $level = 'member';
    $date = date('Y-m-d H:i:s');
    if ($email && $password) {
        $login = mysqli_query($db, "SELECT id, email, password, api_key FROM users WHERE email = '$email'");
        list($id_user, $email, $pw, $api_key) = mysqli_fetch_array($login);
        if (mysqli_num_rows($login) > 0) {
            if (password_verify($password, $pw)) {
                $_SESSION['id'] = $id_user;
                $_SESSION['popup'] = true;
                if ($_POST['remember'] == 'on') {
                    setcookie("TOKEN", base64_encode($api_key), time() + 864000, '/');
                }
                header('Location: '.$domain);
                $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Sukses Login.</div></div></div>';
            }
        }else{
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Akun tidak ditemukan.</div></div></div>';
        }
    }else{
        $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Mohon isi semua formulir.</div></div></div>';
    }
}