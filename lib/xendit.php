<?php
// Xendit for native lib by KodeGud.com
class xendit
{
    public $url_base = 'https://api.xendit.co/';
    public $api_key = '';
    public $url_callback = '';

    public function curl($url, $data = array(), $user_pwd = 0, $time = 0)
    {
        $a = curl_init();
        curl_setopt($a, CURLOPT_URL, $url);
        curl_setopt($a, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($a, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($a, CURLOPT_FOLLOWLOCATION, 1);
        if ($time) {curl_setopt($a, CURLOPT_TIMEOUT, $time);}
        if ($data) {
            curl_setopt($a, CURLOPT_POST, 1);
            curl_setopt($a, CURLOPT_POSTFIELDS, json_encode($data));
        }
        if($user_pwd){
            curl_setopt($a, CURLOPT_USERPWD, $user_pwd);
        }
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($a, CURLOPT_HTTPHEADER, $headers);
        $b = curl_exec($a);
        return $b;
    }

    public function qrcode($order_id, $amount)
    {
        $data['external_id'] = $order_id;
        $data['type'] = 'DYNAMIC';
        $data['amount'] = floor($amount);
        $data['callback_url'] = $this->url_callback;
        $page = $this->curl($this->url_base.'qr_codes', $data, $this->api_key);
        return $page;
    }

    public function alfamart($order_id, $amount, $name)
    {
        $data['external_id'] = $order_id;
        $data['retail_outlet_name'] = 'ALFAMART';
        $data['name'] = $name;
        $data['expected_amount'] = floor($amount);
        $data['is_single_use'] = 'true';
        $data['expiration_date'] = $this->set_date(1);
        $page = $this->curl($this->url_base.'fixed_payment_code', $data, $this->api_key);
        return $page;
    }

    public function set_date($num){
        $stop_date = new DateTime(date('Y-m-d H:i:s'));
        $stop_date->modify('+'.$num.' day');
        $iso_date = $stop_date->format('Y-m-d H:i:s');
        $iso_date = date_format(new DateTime($iso_date), 'c');
        return $iso_date;
    }

    public function ovo($order_id, $amount, $phone)
    {
        $data['reference_id'] = $order_id;
        $data['currency'] = 'IDR';
        $data['amount'] = floor($amount);
        $data['checkout_method'] = 'ONE_TIME_PAYMENT';
        $data['channel_code'] = 'ID_OVO';
        $data['channel_properties']['mobile_number'] = $this->phone_62($phone);
        $data['channel_properties']['success_redirect_url'] = 'https://jasaviral.com/inv/'.$order_id;
        $data['channel_properties']['failure_redirect_url'] = 'https://jasaviral.com/inv/'.$order_id;
        $page = $this->curl($this->url_base.'ewallets/charges', $data, $this->api_key);
        return $page;
    }

    public function dana($order_id, $amount)
    {
        $data['reference_id'] = $order_id;
        $data['currency'] = 'IDR';
        $data['amount'] = floor($amount);
        $data['checkout_method'] = 'ONE_TIME_PAYMENT';
        $data['channel_code'] = 'ID_DANA';
        $data['channel_properties']['success_redirect_url'] = 'https://jasaviral.com/inv/'.$order_id;
        $data['channel_properties']['failure_redirect_url'] = 'https://jasaviral.com/inv/'.$order_id;
        $page = $this->curl($this->url_base.'ewallets/charges', $data, $this->api_key);
        
        return $page;
    }

    public function shopeepay($order_id, $amount)
    {
        $data['reference_id'] = $order_id;
        $data['currency'] = 'IDR';
        $data['amount'] = floor($amount);
        $data['checkout_method'] = 'ONE_TIME_PAYMENT';
        $data['channel_code'] = 'ID_SHOPEEPAY';
        $data['channel_properties']['success_redirect_url'] = 'https://jasaviral.com/inv/'.$order_id;
        $data['channel_properties']['failure_redirect_url'] = 'https://jasaviral.com/inv/'.$order_id;
        $page = $this->curl($this->url_base.'ewallets/charges', $data, $this->api_key);
        return $page;
    }

    public function linkaja($order_id, $amount)
    {
        $data['reference_id'] = $order_id;
        $data['currency'] = 'IDR';
        $data['amount'] = floor($amount);
        $data['checkout_method'] = 'ONE_TIME_PAYMENT';
        $data['channel_code'] = 'ID_LINKAJA';
        $data['channel_properties']['success_redirect_url'] = 'https://jasaviral.com/inv/'.$order_id;
        $data['channel_properties']['failure_redirect_url'] = 'https://jasaviral.com/inv/'.$order_id;
        $page = $this->curl($this->url_base.'ewallets/charges', $data, $this->api_key);
        return $page;
    }
    public function bri($order_id, $amount)
    {
        $data['external_id'] = $order_id;
        $data['bank_code'] = 'BRI';
        $data['name'] = 'JASAVIRAL';
        $data['is_closed'] = true;
        $data['expected_amount'] = floor($amount);
        $data['is_single_use'] = true;
        $page = $this->curl($this->url_base.'callback_virtual_accounts', $data, $this->api_key);
        return $page;
    }
    public function bni($order_id, $amount)
    {
        $data['external_id'] = $order_id;
        $data['bank_code'] = 'BNI';
        $data['name'] = 'JASAVIRAL';
        $data['is_closed'] = true;
        $data['expected_amount'] = floor($amount);
        $data['is_single_use'] = true;
        $page = $this->curl($this->url_base.'callback_virtual_accounts', $data, $this->api_key);
        return $page;
    }
        public function permata($order_id, $amount)
    {
        $data['external_id'] = $order_id;
        $data['bank_code'] = 'PERMATA';
        $data['name'] = 'JASAVIRAL';
        $data['is_closed'] = true;
        $data['expected_amount'] = floor($amount);
        $data['is_single_use'] = true;
        $page = $this->curl($this->url_base.'callback_virtual_accounts', $data, $this->api_key);
        return $page;
    }
            public function mandiri($order_id, $amount)
    {
        $data['external_id'] = $order_id;
        $data['bank_code'] = 'MANDIRI';
        $data['name'] = 'JASAVIRAL';
        $data['is_closed'] = true;
        $data['expected_amount'] = floor($amount);
        $data['is_single_use'] = true;
        $page = $this->curl($this->url_base.'callback_virtual_accounts', $data, $this->api_key);
        return $page;
    }
                public function bsi($order_id, $amount)
    {
        $data['external_id'] = $order_id;
        $data['bank_code'] = 'BSI';
        $data['name'] = 'JASAVIRAL';
        $data['is_closed'] = true;
        $data['expected_amount'] = floor($amount);
        $data['is_single_use'] = true;
        $page = $this->curl($this->url_base.'callback_virtual_accounts', $data, $this->api_key);
        return $page;
    }
    public function phone_62($nohp) {
        $nohp = str_replace(" ","",$nohp);
        $nohp = str_replace("(","",$nohp);
        $nohp = str_replace(")","",$nohp);
        $nohp = str_replace(".","",$nohp);
        if(!preg_match('/[^+0-9]/',trim($nohp))){
            if(substr(trim($nohp), 0, 3)=='+62'){
                $hp = trim($nohp);
            } else if(substr(trim($nohp), 0, 1)=='0'){
                $hp = '+62'.substr(trim($nohp), 1);
            } else if(substr(trim($nohp), 0, 1)=='8'){
                $hp = '+628'.substr(trim($nohp), 1);
            } else if(substr(trim($nohp), 0, 2)=='62'){
                $hp = '+62'.substr(trim($nohp), 2);
            }
        }
        return $hp;
    }
}