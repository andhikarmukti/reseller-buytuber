<?php
require 'config.php';
$sub_judul = 'Buat Akun | ';
require 'lib/user_register.php';
require 'lib/header.php';

?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <h2 class="section-title">Daftar</h2>
      <form method="POST">
        <div class="row">
          <?=$msg;?>
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Lengkap</label>
                  <input type="text" name="name" class="form-control" placeholder="Masukan nama lengkap anda" required>
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="email" class="form-control" placeholder="Masukan email anda" required>
                </div>
                <div class="form-group">
                  <label>Nomor Whatsapp</label>
                  <input type="number" name="phone" class="form-control" placeholder="628xxxxxxxx" required>
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" class="form-control" placeholder="Masukan password baru" required>
                </div>
                <?php  if ($recaptcha['recaptcha_status'] == 1) {?>
                <div class="form-group">
                    <div class="g-recaptcha" data-sitekey="<?=$recaptcha['recaptcha_site_key'];?>"></div>
                </div>
                <?php } else { ?>
                
                <?php } ?>
                <div class="mb-3">
                  Dengan mendaftar otomatis anda telah menyetujui <a href="terms">Ketentuan Layanan</a> kami.<br>
                  Sudah memiliki akun? <a href="login">Masuk Sekarang</a>
                </div>
                <button class="btn btn-primary" type="submit">Daftar</button>
              </div>
            </div>
          </div>
        </div>
      </form>
  </section>
</div>
<?php
require 'lib/footer.php';
?>