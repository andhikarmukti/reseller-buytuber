<?php
# pengaman cronjob
$base_dir = dirname(dirname(__FILE__));
$file_flag = "status_digiflazz.log";
$path_flag = $base_dir . "/cron/flag/";
$cron_flag = $path_flag . $file_flag;
if (is_file($cron_flag)) { die("cronjob sedang berjalan.."); }else{ touch($cron_flag); }

# require cron
require $base_dir.'/config.php';

# script
$id_provider = 5;

$provider = mysqli_query($db, "SELECT * FROM provider WHERE id = '$id_provider'");
$provider = mysqli_fetch_array($provider);
$provider_config = json_decode($provider['config'], 1);

$orders = mysqli_query($db, "SELECT * FROM pesanan WHERE status = 'processing' AND id_provider = '$id_provider' ORDER BY rand() LIMIT 1");
$i = 0;
while ($is_order = mysqli_fetch_array($orders)) {
    $idorder = $is_order['id'];
    $api_key = $provider['config'];
    
    $res_provider_data_en = $is_order['res_provider'];
    
    if($res_provider_data_en && $api_key && $idorder){
        $res_provider_data = json_decode($res_provider_data_en, 1);
        
        $poid = $res_provider_data['data']['ref_id'];
        $sku_code = $res_provider_data['data']['buyer_sku_code'];
        $cus_no = $res_provider_data['data']['customer_no'];
        $status_res = $res_provider_data['data']['status'];
        $response_code = $res_provider_data['data']['rc'];
        
        // blacklist rc karena tidak tercatat dalam digi
        $rc_blacklist = array('40','41','42','43','44','45','47','49','61','62','63','64','65','66','67','68','69','80','81','82',);
        
        if($poid && $sku_code && $cus_no && $status_res == 'Pending' && $response_code && in_array($response_code, $rc_blacklist) == false){
            $url = 'https://api.digiflazz.com/v1/transaction';
            $data_json['username'] = $provider_config['username'];
            $data_json['buyer_sku_code'] = $sku_code;
            $data_json['customer_no'] = $cus_no;
            $data_json['ref_id'] = $poid;
            $data_json['sign'] = md5($provider_config['username'] . $provider_config['api_key'] . $poid);
            $header = array('Content-Type: application/json');
    
            $run_order = curl($url, json_encode($data_json), 0, $header);
            $run_result = json_decode($run_order, 1);
    
            $json_digi = json_decode($run_order, 1);
            $json_digi['data']['sn'] = rawurlencode($json_digi['data']['sn']);
            $encode = json_encode($json_digi);
    
            //buat dapetin SN
            $json_sn = json_decode($encode, 1);
            $sn = $json_sn['data']['sn'];
            //print_r($encode);
            //print_r($is_order);
    
            if ($run_result['data']['status']) {
                $status = $run_result['data']['status'];
                if ($status == 'Sukses') {
                    $status = 'success';
                } else if ($status == 'Gagal') {
                    $status = 'paid';
                } else {
                    $status = 'processing';
                }
                //fix
                $save_encode = mysqli_real_escape_string($db, $encode);
                mysqli_query($db, "UPDATE pesanan SET status = '$status', keterangan = '$sn', res_provider = '$save_encode' WHERE id = '$idorder'");
                echo 'sukses, ' . $idorder . ' <br>';
            } else {
                echo 'gagal, ' . $run_result['data']['message'] . ' <br>';
            }
        }else{
            echo "res tidak di izinkan <br>";
        }
    }else{
        echo "res kosong <br>";
    }
}

# hapus pengaman jika cronjob telah selesai dijalankan
unlink($cron_flag);