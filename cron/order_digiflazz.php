<?php
# pengaman cronjob
$base_dir = dirname(dirname(__FILE__));
$file_flag = "order_digiflazz.log";
$path_flag = $base_dir . "/cron/flag/";
$cron_flag = $path_flag . $file_flag;
if (is_file($cron_flag)) { die("cronjob sedang berjalan.."); }else{ touch($cron_flag); }

# require cron
require $base_dir.'/config.php';

# script
$id_provider = 5;

$provider = mysqli_query($db, "SELECT * FROM provider WHERE id = '$id_provider'");
$provider = mysqli_fetch_array($provider);

$orders = mysqli_query($db, "SELECT * FROM pesanan WHERE status = 'pending' AND id_provider = '$id_provider' ORDER BY rand() LIMIT 1");
$i = 0;
while ($is_order = mysqli_fetch_array($orders)) {
    mysqli_query($db, "UPDATE pesanan SET status = 'processing' WHERE id = '$is_order[id]'");
    
    $data = explode('|', str_replace(' ', '', $is_order['data']));
    $userid = $data[0];
    $zoneid = $data[1];
    
    $layanan = mysqli_query($db, "SELECT * FROM layanan WHERE id = '$is_order[id_layanan]'");
    $layanan = mysqli_fetch_array($layanan);
    
    $provider_config = json_decode($provider['config'],1);
    $service_id = $layanan['id_oid'];
    
    $url = 'https://api.digiflazz.com/v1/transaction';
    
    $data_json['username'] = $provider_config['username'];
    $data_json['buyer_sku_code'] = $service_id;
    $data_json['customer_no'] = $userid.$zoneid;
    $data_json['customer_no'] = ($data_json['customer_no'] == 'Keterangan') ? $is_order['kontak'] : $data_json['customer_no'];
    $data_json['ref_id'] = $is_order['invoice'].'-'.$is_order['reorder']; // mod invoice
    $data_json['sign'] = md5($provider_config['username'] . $provider_config['api_key'] . $data_json['ref_id']); // mod invoice
    $data_json['testing'] = $provider_config['testing'];
    
    $header = array('Content-Type: application/json');

    $run_order = curl($url, json_encode($data_json),0,$header);
    $run_result = json_decode($run_order,1);
    
    $run_order_save = mysqli_real_escape_string($db, $run_order);
    mysqli_query($db, "UPDATE pesanan SET res_provider = '$run_order_save' WHERE id = '$is_order[id]'");
    
    if($run_result['data']['status'] == 'Pending'){
        $poid = $run_result['data']['ref_id'];
        mysqli_query($db, "UPDATE pesanan SET status = 'processing' WHERE id = '$is_order[id]'");
        echo "pending <br>";
    }else if($run_result['data']['status'] == 'Sukses'){
        $poid = $run_result['data']['ref_id'];
        $sn = ($run_result['data']['sn']) ? $run_result['data']['sn'] : 'sn';
        $sn = mysqli_real_escape_string($db, $sn);
        mysqli_query($db, "UPDATE pesanan SET status = 'success', keterangan = '$sn' WHERE id = '$is_order[id]'");
        echo "sukses <br>";
    }else if($run_result['data']['status'] == 'Gagal'){
        mysqli_query($db, "UPDATE pesanan SET status = 'paid' WHERE id = '$is_order[id]'");
        echo 'gagal, '.$run_result['data']['message'].' <br>';
    }
}

# hapus pengaman jika cronjob telah selesai dijalankan
unlink($cron_flag);