<?php
# pengaman cronjob
$base_dir = dirname(dirname(__FILE__));
$file_flag = "service_digiflazz.log";
$path_flag = $base_dir . "/cron/flag/";
$cron_flag = $path_flag . $file_flag;
if (is_file($cron_flag)) { die("cronjob sedang berjalan.."); }else{ touch($cron_flag); }

# require cron
require $base_dir.'/config.php';

# script
$date = date('Y-m-d H:i:s');

$id_provider = 5;

$provider = mysqli_query($db, "SELECT * FROM provider WHERE id = '$id_provider'");
$provider = mysqli_fetch_array($provider);
$provider_config = json_decode($provider['config'],1);

$url = 'https://api.digiflazz.com/v1/price-list';
    
$data_json['cmd'] = 'prepaid';
$data_json['username'] = $provider_config['username'];
$data_json['sign'] = md5($provider_config['username'] . $provider_config['api_key'] . "pricelist");
$header = array('Content-Type: application/json');

$run_order = curl($url, json_encode($data_json),0,$header);

//print_r($data_json);
//print_r($run_order); 

$run_result = json_decode($run_order,1);
//print_r($run_result); die();
foreach($run_result['data'] as $data){
    $code = $data['buyer_sku_code'];
    $price = $data['price'];
    $cekcek = $data['buyer_product_status'];
    if($code){
        $layanan = mysqli_query($db, "SELECT * FROM layanan WHERE id_oid = '$code'");
        $layanan = mysqli_fetch_array($layanan);
        if($layanan['id']){
            $set_keuntungan = ($layanan['profit']) ? $layanan['profit'] : 1;
            $set_keuntungan_silver = ($layanan['profit_silver']) ? $layanan['profit_silver'] : 1;
            $set_keuntungan_gold = ($layanan['profit_gold']) ? $layanan['profit_gold'] : 1;
            $set_keuntungan_pro = ($layanan['profit_pro']) ? $layanan['profit_pro'] : 1;
            
            if($layanan['type_profit'] == 'percent'){
                $ambil_keuntungan = $price * ($set_keuntungan / 100);
                $ambil_keuntungan_silver = $price * ($set_keuntungan_silver / 100);
                $ambil_keuntungan_gold = $price * ($set_keuntungan_gold / 100);
                $ambil_keuntungan_pro = $price * ($set_keuntungan_pro / 100);
            }else if($layanan['type_profit'] == 'flat'){
                $ambil_keuntungan = $set_keuntungan;
                $ambil_keuntungan_silver = $set_keuntungan_silver;
                $ambil_keuntungan_gold = $set_keuntungan_gold;
                $ambil_keuntungan_pro = $set_keuntungan_pro;
            }else{
                $ambil_keuntungan = 0;
                $ambil_keuntungan_silver = 0;
                $ambil_keuntungan_gold = 0;
                $ambil_keuntungan_pro = 0;
            }
            
            $status = ($data['seller_product_status'] == 1 && $data['buyer_product_status'] == 1) ? 1 : 0;
            
            mysqli_query($db, "UPDATE layanan SET 
                            harga_asli = '$price', 
                            harga = '".ceil($price + $ambil_keuntungan)."', 
                            harga_silver = '".ceil($price + $ambil_keuntungan_silver)."', 
                            harga_gold = '".ceil($price + $ambil_keuntungan_gold)."', 
                            harga_pro = '".ceil($price + $ambil_keuntungan_pro)."', 
                            status = '$status' 
                            WHERE id_oid = '$code' AND id_provider = '$id_provider'");
        }
    }
    echo "$code = $status <br>\n";
    flush();
}

# hapus pengaman jika cronjob telah selesai dijalankan
unlink($cron_flag);