<?php
# pengaman cronjob
$base_dir = dirname(dirname(__FILE__));
//$file_flag = "cron_whatsapp.log";
$path_flag = $base_dir . "/cron/flag/";
$cron_flag = $path_flag . $file_flag;
if (is_file($cron_flag)) { die("cronjob sedang berjalan.."); }else{ touch($cron_flag); }

# require cron
require $base_dir.'/config.php';

# script
// config wa gateway
$wa = mysqli_query($db, "SELECT * FROM whatsapp_config WHERE id='1'");
$data_wa = mysqli_fetch_assoc($wa);

// proses ke buyer
$data = mysqli_query($db, "SELECT * FROM whatsapp_gateway WHERE status='pending'");
while ($data_target = mysqli_fetch_assoc($data)) {
    // set tanggal expired
    $start_date = new DateTime($data_target['tanggal']);
    $end_date = new DateTime(date('Y-m-d'));
    $interval = $start_date->diff($end_date);

    if ($interval->days >= 1) {
        echo "Kirim Pesan dibatalkan karena sudah/lebih dari 1 hari tidak dikirim.<br />";
        mysqli_query($db, "UPDATE whatsapp_gateway SET status = 'expired' WHERE id = '$data_target[id]'");
    } else {
        $invoice = mysqli_query($db, "SELECT * FROM pesanan WHERE invoice = '" . $data_target['invoice'] . "' AND status='success'");
        $invoice = mysqli_fetch_array($invoice);
        if (!$invoice['id']) {
            echo "Invoice tidak ditemukan.<br />";
            //mysqli_query($db, "UPDATE whatsapp_gateway SET status = 'not_found' WHERE id = '$data_target[id]'");
        } else {
            //ambil data kategori
            $kategorinya = mysqli_query($db, "SELECT * FROM kategori WHERE id = '" . $invoice['id_kategori'] . "'");
            $data_kategorinya = mysqli_fetch_assoc($kategorinya);

            // atur pesan
            $hasil_rupiah = "Rp " . number_format($invoice['harga'], 0, ',', ',');
            $pesan = $data_wa['text'];
            $pesan2 = $data_wa['text_grup'];
            $api_key = $data_wa['api_key'];
            $device_id = $data_wa['device_id'];
            $url = $data_wa['api_url'];
            $nomor = $invoice['kontak'];

            // replace command pesan
            $searchVal = array("[invoice]", "[nama_layanan]", "[hasil_rupiah]", "[kategori]");
            $replaceVal = array("$invoice[invoice]", "$invoice[nama_layanan]", "$hasil_rupiah", "$data_kategorinya[nama]");
            $res = str_replace($searchVal, $replaceVal, $pesan);

            // proses kirim pesan
            $kirim = send_pesan($url, $api_key, $device_id, $nomor, $res);
            $json_kirim = json_decode($kirim, true);
            //print_r($json_kirim);

            // periksa apakah kirim pesan sukses / tidak
            if ($json_kirim['status'] == 1) {
                echo "Pesan dengan invoice : " . $data_target['invoice'] . " telah Dikirim.<br />";
                mysqli_query($db, "UPDATE whatsapp_gateway SET status = 'success' WHERE id = '" . $data_target['id'] . "'");
            } else {
                echo "Pesan dengan invoice : " . $data_target['invoice'] . " GAGAL dikirim.<br />";
                mysqli_query($db, "UPDATE whatsapp_gateway SET status = 'gagal' WHERE id = '" . $data_target['id'] . "'");
            }

            // tambahan
            indoproof($domain, $data_target['invoice'], $data_kategorinya['nama'], $data_kategorinya['gambar'], $invoice['nama_layanan'], $invoice['id_layanan']);
        }
    }
}


// proses ada orderan manual kirim ke grub
$data_grup = mysqli_query($db, "SELECT * FROM whatsapp_gateway WHERE status_grup='pending'");
while ($data_target = mysqli_fetch_assoc($data_grup)) {
    // set tanggal expired
    $start_date = new DateTime($data_target['tanggal']);
    $end_date = new DateTime(date('Y-m-d'));
    $interval = $start_date->diff($end_date);

    if ($interval->days >= 1) {
        echo "Kirim Pesan dibatalkan karena sudah/lebih dari 1 hari tidak dikirim.<br />";
        mysqli_query($db, "UPDATE whatsapp_gateway SET status_grup = 'expired' WHERE id = '$data_target[id]'");
    } else {
        $invoice = mysqli_query($db, "SELECT * FROM pesanan WHERE invoice = '" . $data_target['invoice'] . "' AND status='pending' AND id_provider = '1'");
        $invoice = mysqli_fetch_array($invoice);
        if (!$invoice['id']) {
            echo "Invoice tidak ditemukan.<br />";
            //mysqli_query($db, "UPDATE whatsapp_gateway SET status_grup = 'not_found' WHERE id = '$data_target[id]'");
        } else {
            //ambil data kategori
            $kategorinya = mysqli_query($db, "SELECT * FROM kategori WHERE id = '" . $invoice['id_kategori'] . "'");
            $data_kategorinya = mysqli_fetch_assoc($kategorinya);

            // atur pesan
            $hasil_rupiah = "Rp " . number_format($invoice['harga'], 0, ',', ',');
            $pesan = $data_wa['text'];
            $pesan2 = $data_wa['text_grup'];
            $api_key = $data_wa['api_key'];
            $device_id = $data_wa['device_id'];
            $url = $data_wa['api_url'];
           // $nomor = $invoice['kontak'];

            // replace command pesan
			$searchVal = array("[invoice]", "[nama_layanan]", "[kategori]", "[id]", "[hasil_rupiah]", "[kontak]");
			$replaceVal = array("$invoice[invoice]", "$invoice[nama_layanan]", "$data_kategorinya[nama]", "$invoice[id]", "$hasil_rupiah", "$invoice[kontak]");
			$res = str_replace($searchVal, $replaceVal, $pesan2);

            // proses kirim pesan
            $kirim = send_pesan($url, $api_key, $device_id, $data_wa['group_id'], $res);
            $json_kirim = json_decode($kirim, true);
            //print_r($json_kirim);

            // periksa apakah kirim pesan sukses / tidak
            if ($json_kirim['status'] == 1) {
                echo "Pesan dengan invoice : " . $data_target['invoice'] . " telah Dikirim.<br />";
                mysqli_query($db, "UPDATE whatsapp_gateway SET status_grup = 'success' WHERE id = '" . $data_target['id'] . "'");
            } else {
                echo "Pesan dengan invoice : " . $data_target['invoice'] . " GAGAL dikirim.<br />";
                mysqli_query($db, "UPDATE whatsapp_gateway SET status_grup = 'gagal' WHERE id = '" . $data_target['id'] . "'");
            }
        }
    }
}

# hapus pengaman jika cronjob telah selesai dijalankan
unlink($cron_flag);