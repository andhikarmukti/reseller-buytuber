<?php
date_default_timezone_set('Asia/Jakarta');

$delete_time = 2;
$type = 'm'; // h = hours, m = minute

$path    = dirname(__FILE__).'/flag/';
$files = scandir($path);

foreach($files as $file) 
{
  if(is_file($path.$file))
  {
    $mod_date = date("Y-m-d H:i:s", filemtime($path.$file));
    
    $waktu_awal = strtotime($mod_date);
    $waktu_akhir = strtotime(date("Y-m-d H:i:s"));
    
    $diff = $waktu_akhir - $waktu_awal;
    $jam = floor($diff / (60 * 60));
    $menit = floor(($diff - $jam * (60 * 60)) / 60);
    
    $msg = "";
    if($type == 'm'){
        if($menit > $delete_time){
            $msg = "[M] $file ($mod_date -> ".date("Y-m-d H:i:s").") : ($menit -> $delete_time) = deleted, penyebab timeout <br> \n";
            
            $fp = fopen('flag_delete_log.txt', 'a');
            fwrite($fp, $msg);
            fclose($fp);
            
            unlink($path.$file);
        }
    }else if($type == 'h'){
        if($jam > $delete_time){
            $msg = "[H] $file ($mod_date -> ".date("Y-m-d H:i:s").") : ($jam -> $delete_time) = deleted, penyebab timeout <br> \n";
            
            $fp = fopen('flag_delete_log.txt', 'a');
            fwrite($fp, $msg);
            fclose($fp);
            
            unlink($path.$file);
        }
    }
    echo $msg;
  }
}