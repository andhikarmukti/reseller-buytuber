<?php
# pengaman cronjob
$base_dir = dirname(dirname(__FILE__));
$file_flag = "cron_saldo.log";
$path_flag = $base_dir . "/cron/flag/";
$cron_flag = $path_flag . $file_flag;
if (is_file($cron_flag)) { die("cronjob sedang berjalan.."); }else{ touch($cron_flag); }

# require cron
require $base_dir.'/config.php';

# script
function send_pesans($nomor,$pesan){
	$data = "api_key=YOUR_APIKEY&sender=YOUR_PHONE&number=$nomor&message=$pesan";
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, 'https://piwapi.com/send-message');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    $output = curl_exec($ch); 
    curl_close($ch);      

    return $output;
}
$orders = mysqli_query($db, "SELECT * FROM pesanan WHERE status = 'pending' AND nama_kategori = 'Topup Saldo' LIMIT 1");
$i = 0;
while ($is_order = mysqli_fetch_array($orders)) {
    mysqli_query($db, "UPDATE pesanan SET status = 'processing' WHERE id = '$is_order[id]'"); // fix
    
    $saldo =  mysqli_real_escape_string($db, $is_order['data']);
    if(is_numeric($saldo) == false){ 
        mysqli_query($db, "UPDATE pesanan SET status = 'cancel' WHERE id = '$is_order[id]'"); 
    }else if(preg_match('#[^0-9]#',$saldo)){
        mysqli_query($db, "UPDATE pesanan SET status = 'cancel' WHERE id = '$is_order[id]'"); 
    }else{
        mysqli_query($db, "UPDATE pesanan SET status = 'success' WHERE id = '$is_order[id]'");
        mysqli_query($db, "UPDATE users SET saldo = saldo + $saldo WHERE id = '$is_order[id_user]'");
        mysqli_query($db, "UPDATE whatsapp_gateway SET status = 'success', status_grup = 'success' WHERE invoice = '$is_order[invoice]'");
        $cek_nomor = mysqli_query($db, "SELECT * FROM users WHERE id = '$is_order[id_user]'");
        $data_nomor = mysqli_fetch_array($cek_nomor);
        $hasil_rupiah = "Rp " . number_format($is_order['data'], 0, ',', ',');
    $pesan = 
"*Selamat ! Pembayaran Kamu Berhasil !*

Jumlah : $hasil_rupiah
Status : Sudah Dibayar			

Silahkan Masuk website untuk melakukan pesanan.

*Selamat Berbelanja* 

			
Jika Kamu memiliki pertanyaan, jangan ragu untuk menghubungi kami. 

Salam, 
*Jasaviral*";
        $nomor = $data_nomor['ponsel'];
    	$kirim = send_pesans($nomor,$pesan);   
        echo "$is_order[id] => Sukses <br>";
        flush();
    }
}

# hapus pengaman jika cronjob telah selesai dijalankan
unlink($cron_flag);
?>