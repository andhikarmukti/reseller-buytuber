<?php
# pengaman cronjob
$base_dir = dirname(dirname(__FILE__));
$file_flag = "cron_order_bca.log";
$path_flag = $base_dir . "/cron/flag/";
$cron_flag = $path_flag . $file_flag;
if (is_file($cron_flag)) { die("cronjob sedang berjalan.."); }else{ touch($cron_flag); }

# require cron
require $base_dir.'/config.php';

# script
$data = mysqli_query($db, "SELECT * FROM pesanan WHERE metode_pembayaran = '4' AND status = 'not_paid'");
while ($data_target = mysqli_fetch_assoc($data)) {
	if ($data_target['tanggal'])
	$start_date = new DateTime($data_target['tanggal']);
	$end_date = new DateTime(date('Y-m-d'));
	$interval = $start_date->diff($end_date);
	echo $data_target['kode'];

	if ($interval->days >= 1) {
		$update = mysqli_query($db, "UPDATE pesanan SET status = 'cancel' WHERE id = '$data_target[id]'");
		echo "Pesanan dibatalkan karena sudah/lebih dari 1 hari.<br />";
	} else {
		$mutasi = mysqli_query($db, "SELECT * FROM mutasi_bca WHERE amount = '".$data_target['harga']."' AND status = 'Pending' AND type = 'CR'");
		if (mysqli_num_rows($mutasi) == 0) {
			echo "Transaksi tidak ditemukan.<br />";
		} else {
			$tanggal = date('Y-m-d H:i:s');
			$mutasi = mysqli_fetch_array($mutasi);
			$data_pesanan = mysqli_query($db, "SELECT * FROM pesanan WHERE id = '".$data_target['id']."'");
			if (mysqli_num_rows($data_pesanan) > 0) {
				$data_pesanan = mysqli_fetch_array($data_pesanan);
				$c = "UPDATE pesanan SET status = 'pending' WHERE id = '".$data_target['id']."'";
				mysqli_query($db, $c);
				mysqli_query($db, "INSERT INTO whatsapp_gateway VALUES(null, '".$data_pesanan['invoice']."', 'pending', 'pending', '$tanggal')");
				$d = "UPDATE mutasi_bca SET status = 'Success' WHERE id = '".$mutasi['id']."'";
				mysqli_query($db, $d);
				echo "Pesanan dengan invoice : ".$data_target['invoice']." telah diterima.<br />";
				
				
			}
		}
	}
}

# hapus pengaman jika cronjob telah selesai dijalankan
unlink($cron_flag);