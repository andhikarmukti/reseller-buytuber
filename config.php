<?php
date_default_timezone_set('Asia/Jakarta');
error_reporting(0);
#ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
session_start();

$judul = 'DevSosMed.com';
$domain = 'http://localhost/jv-master/';

$db_server = "localhost";
$db_username = "root";
$db_password = "";
$db_name = "jv-master";


$db = mysqli_connect($db_server, $db_username, $db_password) or die("Error in connection!");
mysqli_select_db($db, $db_name ) or die("Could not select database");

$date = date('Y-m-d H:i:s');

require dirname(__FILE__).'/lib/remember.php';
require dirname(__FILE__).'/lib/cek_login.php';
require dirname(__FILE__).'/lib/function.php';

$web_data = mysqli_query($db, "SELECT * FROM pengaturan");
$web_data = mysqli_fetch_array($web_data);

$logo = json_decode($web_data['logo'], 1);
$favfav = json_decode($web_data['favicon'], 1);
?>
