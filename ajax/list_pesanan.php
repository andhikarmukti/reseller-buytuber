<?php
require '../config.php';

	require('../lib/ssp.class.php');
	$table = 'pesanan';
	$primaryKey = 'id';

	$columns = array(
     
        // fix xss
        array( 'db' => 'invoice',  'dt' => 0, 'formatter' => function($i) { return '<a href="inv/'.htmlspecialchars($i).'" class="btn btn-sm btn-outline-info">'.htmlspecialchars($i).' <i class="fas fa-share"></i></a>'; }),
		array( 'db' => 'nama_kategori', 'dt' => 1, 'formatter' => function($i) { return htmlspecialchars($i); }),
		array( 'db' => 'nama_layanan', 'dt' => 2, 'formatter' => function($i) { return htmlspecialchars($i); }),
      
		array( 'db' => 'harga',  'dt' => 3, 'formatter' => function($i) {
			return "Rp ".number_format($i,0,',','.');
		}),
        array( 'db' => 'tanggal', 'dt' => 4),
		array( 'db' => 'status', 'dt' => 5, 'formatter' => function($i) {
            if ($i == 'success') {
                $label = 'primary';
            } else if($i == 'not_paid'){
				$label = 'dark';
			}  else {
				$label = 'warning';
			}
			return '<span class="badge badge-'.$label.'">'.$i.'</span>';
		}),
        array( 'db' => 'id',  'dt' => 6, 'formatter' => function($i) {
        	$filter = htmlspecialchars($i);
			return '<button class="btn btn-default bg-dark text-white btn-pesanan" onclick="detail_pesanan(\''.$filter.'\')">Detail</button>';
		}),
		
	);
	$sql_details = array(
        'user' => $db_username,
        'pass' => $db_password,
        'db'   => $db_name,
        'host' => $db_server
    );
	$joinQuery = null;
	$extraWhere = "id_user = '".$_SESSION['id']."'";
	$groupBy = '';
	$having = '';
	print(json_encode(
		SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
	));
