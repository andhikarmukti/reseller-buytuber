<?php
require '../config.php';
require '../lib/cek_id.php';

$id = mysqli_real_escape_string($db, $_POST['layanan']);
$kategori = mysqli_real_escape_string($db, $_POST['kategori']);
$metode_id = mysqli_real_escape_string($db, $_POST['metode']);
$data_id = ($_POST['other_id']) ? $_POST['id'].'|'.$_POST['other_id'] : $_POST['id'];
$data_id = mysqli_real_escape_string($db, $data_id); // data_id filternnick sebagian ada yg keliru

$array['status'] = false;
$use_nickname = false;
$dont_use_nickname = false;

$db_layanan = mysqli_query($db, "SELECT * FROM layanan WHERE id = '$id' AND status='1'");
$layanan = mysqli_fetch_array($db_layanan);

$db_metode = mysqli_query($db, "SELECT * FROM metode_pembayaran WHERE id = '$metode_id' AND status='1'");
$metode = mysqli_fetch_array($db_metode);

if ($layanan['id']) {

    if($kategori == 1){ $use_nickname = true; $nickname = $cek_id->mobile_legends($data_id);}
   if($kategori == 2){ $use_nickname = true; $nickname = $cek_id->free_fire($data_id);}
    //if($kategori == 4){ $dont_use_nickname = true; }
    if($kategori == 5){ $use_nickname = true; $nickname = $cek_id->sausage_man($data_id);}
    //if($kategori == 6){ $dont_use_nickname = true; }
    //if($kategori == 7){ $dont_use_nickname = true; }
    //if($kategori == 8){ $dont_use_nickname = true; }
    //if($kategori == 9){ $dont_use_nickname = true; }
    //if($kategori == 9){ $dont_use_nickname = true; }
    if($kategori == 11){ $use_nickname = true; $nickname = $cek_id->hago($data_id);}
    if($kategori == 12){ $use_nickname = true; $nickname = $cek_id->aov($data_id);}
    //if($kategori == 13){ $use_nickname = true; $nickname = $cek_id->point_blank($data_id);}
    if($kategori == 14){ $use_nickname = true; $nickname = $cek_id->lords_mobile($data_id);}
  //  if($kategori == 67){ $use_nickname = true; $nickname = $cek_id->digiflazz_pln($data_id);}
    $nickname = ($nickname) ? "Nickname : $nickname" : "";
    
    $harga = ($layanan['harga']) ? $layanan['harga'] : 0;
    $harga = ($data_user['level'] == 'silver') ? $layanan['harga_silver'] : $harga;
    $harga = ($data_user['level'] == 'gold') ? $layanan['harga_gold'] : $harga;
    $harga = ($data_user['level'] == 'pro') ? $layanan['harga_pro'] : $harga;
    
    $rate_persen = ($metode['rate_persen'] > 0) ? ($metode['rate_persen']/100) * $harga : 0;
    $harga = ($metode['rate'] > 0) ? ($harga * $metode['rate']) + $rate_persen : $harga + $rate_persen;
    $harga = "Rp ". number_format($harga, 0, ',', ',');

    $metode_name = ($metode['nama']) ? $metode['nama'] : 'Metode pembayaran tidak ditemukan';
    $filternick = htmlspecialchars($nickname);
    $array['msg'] = "
       $filternick
        Pembayaran : $metode_name
        Harga : $harga
    ";

    if ($use_nickname && $nickname) { $array['status'] = true; }else if($use_nickname){ $array['msg'] = 'ID anda salah'; }
    if ($use_nickname == false) { $array['status'] = true; }
}
print_r(json_encode($array));