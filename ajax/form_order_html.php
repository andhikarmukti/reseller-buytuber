<?php 
require '../config.php';
$id = mysqli_real_escape_string($db, $_GET['id']);

$db_kategori = mysqli_query($db, "SELECT * FROM kategori WHERE id='$id'");
$kategori = mysqli_fetch_array($db_kategori);
$gambar_bantuan = json_decode($kategori['bantuan'], 1);

if($gambar_bantuan[0]){
  $bantuan = '<div class="col-md-2 col-12 mt-md-2 mt-3"><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal"><font color="white"><i class="fas fa-question"></i> Bantuan</a></font></div>';
}else{
  $bantuan = '';
}

$id = $kategori['tipe_form'];

if ($id == 1) {
    echo '
    <h5><span class="badge badge-primary badge-square mr-1"> </span> Lengkapi Data</h5>
    <div class="card">
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-10 col-12">
            <input type="number" class="form-control" id="id" placeholder="User ID" name="id" required>
          </div>
          '.$bantuan.'
        </div>
      </div>
    </div>';
}else if($id == 2){
  echo '
    <h5><span class="badge badge-primary badge-square mr-1"> </span> Lengkapi Data</h5>
    <div class="card">
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-5 col-6">
            <input type="number" class="form-control" id="id" placeholder="User ID" name="id" required>
          </div>
          <div class="col-md-5 col-6">
            <input type="number" class="form-control" id="other_id" placeholder="Server ID" name="other_id" required>
          </div>
          '.$bantuan.'
        </div>
      </div>
    </div>';
}else if($id == 3){
  echo '
    <h5><span class="badge badge-primary badge-square mr-1"> </span> Lengkapi Data</h5>
    <div class="card">
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-10 col-12">
            <input type="text" class="form-control" id="id" placeholder="User ID" name="id" required>
          </div>
          '.$bantuan.'
        </div>
      </div>
    </div>';
}else if($id == 4){
  echo '
    <h5><span class="badge badge-primary badge-square mr-1"> </span> Lengkapi Data</h5>
    <div class="card">
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-10 col-12">
            <input type="number" class="form-control" id="id" placeholder="Masukan nomor" name="id" required>
          </div>
          '.$bantuan.'
        </div>
      </div>
    </div>';
      
} else if($id == 5){
  echo '
    <h5><span class="badge badge-primary badge-square mr-1"> </span> Lengkapi Data</h5>
    <div class="card">
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-10 col-12">
            <input type="text" class="form-control" id="id" placeholder="Url Postingan / Username" name="id" required>
          </div>
          '.$bantuan.'
        </div>
      </div>
    </div>';
} else if($id == 6){
  echo '
    <h5 class="text-black"><span class="badge badge-primary badge-square mr-1"> </span> Lengkapi Data</h5>
    <div class="card bg-dark-2">
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-5 col-6">
            <input type="number" class="form-control" id="id" placeholder="User ID" name="id" required>
          </div>
          <div class="col-md-5 col-6">
             <select class="form-control" id="other_id" name="other_id" required>
              <option>Pilih Server</option>
                <option value="US">America</option>
                <option value="EU">Europe</option>
                <option value="SI">Asia</option>
                <option value="HK">TW_HK_MO</option>
            </select>
          </div>
          '.$bantuan.'
        </div>
      </div>
    </div>';
}else if($id == 7){
  echo '
    <h5><span class="badge badge-primary badge-square mr-1"> </span> Lengkapi Data</h5>
    <div class="card">
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-10 col-12">
            <input type="text" class="form-control" id="id" placeholder="Url Postingan" name="id" required>
          </div>
          '.$bantuan.'
        </div>
      </div>
    </div>';
}else if($id == 8){
  echo '
    <h5 class="text-black"><span class="badge badge-primary badge-square mr-1"> </span> Lengkapi Data</h5>
    <div class="card bg-dark-2">
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-5 col-6">
            <input type="number" class="form-control" id="id" placeholder="User ID" name="id" required>
          </div>
          <div class="col-md-5 col-6">
             <select class="form-control" id="other_id" name="other_id" required>
              <option>Pilih Server</option>
                <option value="DeathQuay">DeathQuay</option>
                <option value="CrosRiver">CrosRiver</option>
                <option value="Buckland">Buckland</option>
                <option value="BurntPlan">BurntPlan</option>
                <option value="JadeCoast">JadeCoast</option>
                <option value="PadHill">PadHill</option>
                <option value="RoniLand">RoniLand</option>
                <option value="BeautyLake">BeautyLake</option>
                <option value="BlizzardBay">BlizzardBay</option>
                <option value="LushField">LushField</option>
                <option value="DustyPlan">DustyPlan</option>
                <option value="IceRiver">IceRiver</option>
                <option value="GreenGuilty">GreenGuilty</option>
                <option value="AzureField">AzureField</option>
                <option value="EosBeach">EosBeach</option>
                <option value="TwilightBay">TwilightBay</option>
                <option value="Gray Plain">Gray Plain</option>
                <option value="SandSnow">SandSnow</option>
            </select>
          </div>
          '.$bantuan.'
        </div>
      </div>
    </div>';
}else if($id == 9){
  echo '
    <h5><span class="badge badge-primary badge-square mr-1"> </span> Lengkapi Data</h5>
    <div class="card">
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-10 col-12">
            <input type="text" class="form-control" id="id" placeholder="Url Postingan" name="id" required>
          </div>
          '.$bantuan.'
        </div>
      </div>
    </div>';
}else {
    echo '<input type="hidden" class="form-control" id="id" placeholder="User ID" name="id" value="Keterangan" required>';
}
?>