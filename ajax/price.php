<?php
require '../config.php';

$id = mysqli_real_escape_string($db, $_POST['layanan']);

$array['status'] = false;

$db_layanan = mysqli_query($db, "SELECT * FROM layanan WHERE id = '$id' AND status='1'");
$layanan = mysqli_fetch_array($db_layanan);

if ($layanan['id']) {
    $array['status'] = true;
    $db_metode = mysqli_query($db, "SELECT * FROM metode_pembayaran WHERE status='1'");
    $i=0;
    while ($metode = mysqli_fetch_array($db_metode)) {
        $harga = ($layanan['harga']) ? $layanan['harga'] : 0;
        $harga = ($data_user['level'] == 'silver') ? $layanan['harga_silver'] : $harga;
        $harga = ($data_user['level'] == 'gold') ? $layanan['harga_gold'] : $harga;
        $harga = ($data_user['level'] == 'pro') ? $layanan['harga_pro'] : $harga;

        $rate_persen = ($metode['rate_persen'] > 0) ? ($metode['rate_persen']/100) * $harga : 0;
        $harga = ($metode['rate'] > 0) ? ($harga * $metode['rate']) + $rate_persen : $harga + $rate_persen;
        $harga = $harga + $metode['fee'];

        $array['data'][$i]['nama'] = str_replace(' ', '_', $metode['nama']);
        $array['data'][$i]['harga'] = "Rp ". number_format($harga, 0, ',', ',');
        $i++;
    }
}

print_r(json_encode($array));