<?php
require '../config.php';

$jumlah_saldo = mysqli_real_escape_string($db, $_POST['jumlah_saldo']);

$array['status'] = false;

if ($jumlah_saldo) {
    if(is_numeric($jumlah_saldo) == false){
        $array['status'] = false;
    }else if(preg_match('#[^0-9]#',$jumlah_saldo)){
        $array['status'] = false;
    }else{
        $array['status'] = true;
        $db_metode = mysqli_query($db, "SELECT * FROM metode_pembayaran WHERE status='1'");
        $i=0;
        while ($metode = mysqli_fetch_array($db_metode)) {
            $harga = ($jumlah_saldo) ? $jumlah_saldo : 0;
    
            $rate_persen = ($metode['rate_persen'] > 0) ? ($metode['rate_persen']/100) * $harga : 0;
            $harga = ($metode['rate'] > 0) ? ($harga * $metode['rate']) + $rate_persen : $harga + $rate_persen;
            $harga = $harga + $metode['fee'];
    
            $array['data'][$i]['nama'] = str_replace(' ', '_', $metode['nama']);
            $array['data'][$i]['harga'] = "Rp ". number_format($harga, 0, ',', ',');
            $i++;
        }
    }
}

print_r(json_encode($array));