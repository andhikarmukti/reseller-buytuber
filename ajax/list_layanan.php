<?php
require '../config.php';
require('../lib/ssp.class.php');
$table = 'layanan';
$primaryKey = 'id';

$columns = array(
    array('db' => '`a`.`nama`', 'dt' => 0, 'field' => 'nama'),
    array('db' => '`b`.`nama`', 'dt' => 1, 'field' => 'nama_kategori', 'as' => 'nama_kategori'),
    array('db' => '`a`.`harga`', 'dt' => 2,  'field' => 'harga', 'formatter' => function($i) {
        return "Rp ".number_format($i,0,',','.');
    }),
    array('db' => '`a`.`harga_silver`', 'dt' => 3,  'field' => 'harga_silver', 'formatter' => function($i) {
        return "Rp ".number_format($i,0,',','.');
    }),
    array('db' => '`a`.`harga_gold`', 'dt' => 4,  'field' => 'harga_gold', 'formatter' => function($i) {
        return "Rp ".number_format($i,0,',','.');
    }),
    array('db' => '`a`.`harga_pro`', 'dt' => 5,  'field' => 'harga_pro', 'formatter' => function($i) {
        return "Rp ".number_format($i,0,',','.');
    }),
    array('db' => '`a`.`status`', 'dt' => 6, 'field' => 'status', 'formatter' => function($x, $r) use ($db){
        $data_kategori = mysqli_query($db, "SELECT * FROM kategori WHERE nama = '$r[1]'");
        $data_kategori = mysqli_fetch_array($data_kategori);

        if($x){
            $badge = 'primary';
            $txt = 'Aktif';
        }else{
            $badge = 'danger';
            $txt = 'Habis';
        }
        if($data_kategori['status'] == 0){
            $badge = 'dark';
            $txt = 'Gangguan';
        }
        return "<span class=\"badge badge-$badge\">$txt</span>";
    }),
    array('db' => '`b`.`nama`', 'dt' => 7, 'field' => 'nama_kategori', 'as' => 'nama_kategori', 'formatter' => function($x, $r) use($domain){
        $link = $domain.'beli/'.strtolower(str_replace(' ', '-', $x)).'/'.$r[0];
        return "<a href=\"$link\" class=\"btn -btn-sm btn-primary\"><i class=\"fas fa-shopping-cart\"></i> Beli</span>";
    }),
);

$sql_details = array(
    'user' => $db_username,
    'pass' => $db_password,
    'db'   => $db_name,
    'host' => $db_server
);

$joinQuery = "FROM `{$table}` AS `a` LEFT JOIN `kategori` AS `b` ON (`b`.`id` = `a`.`id_kategori`)";

if($_SESSION['set_category'] == 'Semua Kategori'){
    $extraWhere = "";
}else if($_SESSION['set_category']){
    $nama_kategori = mysqli_real_escape_string($db, $_SESSION['set_category']);
    $kategori = mysqli_query($db, "SELECT * FROM kategori WHERE nama = '$nama_kategori'");
    $kategori = mysqli_fetch_array($kategori);
    $extraWhere = "id_kategori='$kategori[id]'";
}else{
    $extraWhere = "";
}

$groupBy = '';
$having = '';

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
);