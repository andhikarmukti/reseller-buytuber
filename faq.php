<?php
require 'config.php';
$sub_judul = "FAQ | ";
require 'lib/header.php';
?>


<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <h2 class="section-title">Pertanyaan Umum</h2>
      <div class="row">
        <div class="col-12">
          
        <div id="accordion">
        <?php
               $faq = mysqli_query($db, "SELECT * FROM faq"); $i=1;
               while ($data_faq = mysqli_fetch_array($faq)) {
            ?>
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <a href="" data-toggle="collapse" data-target="#collapse<?=$data_faq['id']?>"
                      aria-expanded="true" aria-controls="collapse<?=$data_faq['id']?>">
                      <?=$data_faq['pertanyaan']?>
                    </a>
                  </div>

                  <div id="collapse<?=$data_faq['id']?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                    <?=$data_faq['jawaban']?>
                    </div>
                  </div>
                </div>
                <?php $i++; } ?>
      
              </div>

        </div>
      </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>