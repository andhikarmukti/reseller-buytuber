<?php
require 'config.php';
$sub_judul = "Ketentuan Layanan | ";
require 'lib/header.php';
$terms = mysqli_query($db, "SELECT * FROM pengaturan");
$data_terms = mysqli_fetch_array($terms);
?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <h2 class="section-title">Ketentuan Layanan</h2>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
            <?=$data_terms['terms'];?>
            </div>
          </div>
        </div>
      </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>