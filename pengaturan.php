<?php
require 'config.php';
require 'lib/redirect_not_login.php';
require 'lib/header.php';
$id_user = $_SESSION['id'];
$pengguna = mysqli_query($db, "SELECT * FROM users WHERE id = '$id_user'");
$data_pengguna = mysqli_fetch_array($pengguna);
$passwordz = $data_pengguna['password'];
require 'lib/model.php';
$model = new Model();
if ($_GET['action'] == 'profile') {
    if ($_POST) {
        $input_data = array('nama', 'password');
        if (check_input($_POST, $input_data) == false) {
            $msg = '<div class="alert alert-danger bg-danger text-white border-0 mt-4" role="alert"><strong>Fail!</strong> Input Salah.</div>';
        } else {
            $input_post = array(
                'nama' => mysqli_real_escape_string($db, htmlspecialchars($_POST['nama'])),
                'ponsel' => mysqli_real_escape_string($db, htmlspecialchars($_POST['ponsel'])),
            );
            $cek_pasw = $_POST['password'];
            if (check_empty($input_post) == true) {
                $msg = '<div class="alert alert-danger bg-danger text-white border-0 mt-4" role="alert"><strong>Gagal!</strong> Input tidak boleh kosong.</div>';
            } else {
                if (password_verify($cek_pasw, $passwordz) == false) {
                    $msg = '<div class="alert alert-danger bg-danger text-white border-0 mt-4" role="alert"><strong>Gagal!</strong> Password Salah.</div>';
                } else {
                    if ($model->db_update($db, "users", $input_post, "id = '" . $id_user . "'")) {
                        $msg = '<div class="alert alert-success bg-success text-white border-0 mt-4" role="alert"><strong>Sukses!</strong> Profil Berhasil diubah.</div>';

                    } else {
                        $msg = '<div class="alert alert-danger bg-danger text-white border-0 mt-4" role="alert"><strong>Gagal!</strong> Profil Gagal diubah.</div>';
                    }
                }
            }
        }
    }
}

if ($_GET['action'] == 'changepassword') {
    if ($_POST) {
        $input_data = array('password', 'new_password', 'new_password2');
        if (check_input($_POST, $input_data) == false) {
            $msg2 = '<div class="alert alert-danger bg-danger text-white border-0 mt-4" role="alert"><strong>Gagal !</strong> Gagal.</div>';
        } else {
            $input_post = array(
                'password' => mysqli_real_escape_string($db, htmlspecialchars($_POST['password'])),
                'new_password' => mysqli_real_escape_string($db, htmlspecialchars($_POST['new_password'])),
                'new_password2' => mysqli_real_escape_string($db, htmlspecialchars($_POST['new_password2'])),
            );
            if (check_empty($input_post) == true) {
                $msg2 = '<div class="alert alert-danger bg-danger text-white border-0 mt-4" role="alert">
                                                            <strong>Gagal!</strong> Input tidak boleh kosong
                                                        </div>';
            } else {
                if (password_verify($input_post['password'], $passwordz) == false) {
                    $msg2 = '<div class="alert alert-danger bg-danger text-white border-0 mt-4" role="alert"><strong>Gagal!</strong> Password Salah.</div>';
                } elseif (strlen($input_post['new_password']) < 5) {
                    $msg2 = '<div class="alert alert-danger bg-danger text-white border-0 mt-4" role="alert"><strong>Gagal!</strong> Password Minimum 5 characters.</div>';
                } elseif ($input_post['new_password'] != $input_post['new_password2']) {
                    $msg2 = '<div class="alert alert-danger bg-danger text-white border-0 mt-4" role="alert"><strong>Gagal!</strong> Konfirmasi Password Salah.</div>';
                } else {
                    if ($model->db_update($db, "users", array('password' => password_hash($input_post['new_password'], PASSWORD_DEFAULT)), "id = '" . $id_user . "'")) {
                        $msg2 = '<div class="alert alert-success bg-success text-white border-0 mt-4" role="alert"><strong>Sukses!</strong> Password Berhasil diubah.</div>';
                    } else {
                        $msg2 = '<div class="alert alert-danger bg-danger text-white border-0 mt-4" role="alert"><strong>Gagal!</strong> Password Gagal diubah.</div>';
                    }
                }
            }
        }
    }
}
?>
<br><br>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <div class="row">       
        <div class="col-md-6">
        
          <div class="card">
            <div class="card-header">
              <h5>Pengaturan</h5>
            </div>
            <div class="card-body">
            <?=$msg;?>
              <form method="POST" action="<?=$domain;?>pengaturan?action=profile">
                <div class="form-group">
                  <label>Email</label>
                  <input type="text" class="form-control" name="email"  value="<?=htmlspecialchars($data_pengguna['email']);?>" readonly disabled>
                </div>
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama" placeholder="nama" value="<?=htmlspecialchars($data_pengguna['nama']);?>">
                </div>
                <div class="form-group">
                  <label>Ponsel</label>
                  <input type="number" class="form-control" name="ponsel" placeholder="ponsel" value="<?=htmlspecialchars($data_pengguna['ponsel']);?>">
                </div>
                <div class="form-group">
                  <label>Kata Sandi</label>
                  <input type="password" class="form-control" name="password" placeholder="Password">
                  <small class="text-danger">*Kata Sandi dibutuhkan saat pergantian Profil</small>
                </div>
               
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-danger" href="?">Tutup</a>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-6">
       
          <div class="card">
            <div class="card-header">
              <h5>Ubah Kata Sandi</h5>
            </div>
            <div class="card-body">
            <?=$msg2;?>
              <form method="POST" action="<?=$domain;?>pengaturan?action=changepassword">
                <div class="form-group">
                  <label>Kata Sandi Saat ini</label>
                  <input type="password" class="form-control" name="password" placeholder="Kata Sandi Saat ini">
                </div>
                <div class="form-group">
                  <label>Kata Sandi Baru</label>
                  <input type="password" class="form-control" name="new_password" placeholder="Kata Sandi Baru">
                </div>
                <div class="form-group">
                  <label>Ulangi Kata Sandi Baru</label>
                  <input type="password" class="form-control" name="new_password2" placeholder="Ulangi Kata Sandi Baru">
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-danger" href="?">Tutup</a>
              </form>
            </div>
          </div>
        </div>
       
      </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>

<script>
  $(document).ready(function () {
    $('#table_ajax').DataTable({
      "processing": false,
      "serverSide": true,
      "bInfo": false,
      "ajax": "<?=$domain;?>ajax/list_pesanan.php",
      "language": {
        "paginate": {
          "previous": "<i class='fas fa-angle-left'></i>",
          "next": "<i class='fas fa-angle-right'></i>",
        }
      }
    });
  });
</script>