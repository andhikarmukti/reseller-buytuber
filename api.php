<?php
header('Content-Type: application/json; charset=utf-8');
require 'config.php';
require 'controllers/Api.php';

$api = new Api($db);
$action = $_POST['action'];
$response = [];

switch ($action) {
    case 'order':
        $serviceId = $_POST['service'];
        $link = $_POST['link'];
        $target = $_POST['target'];
        $response = $api->order($serviceId, $link, $target);
        break;

    case 'status':
        $orderId = $_POST['order'];
        $response = $api->status($orderId);
        break;

    case 'services':
        $response = $api->services();
        break;

    default:
        $response = $api->index();
        break;
}

echo json_encode($response);