<?php
require 'config.php';
$sub_judul = 'Masuk | ';
require 'lib/user_login.php';
require 'lib/header.php';
?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <h2 class="section-title">Masuk</h2>
      <form method="POST">
        <div class="row">
          <?=$msg;?>
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="email" class="form-control" placeholder="Masukan alamat email" required>
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <span class="float-right"><a href="register">Lupa Password</a></span>
                  <input type="password" name="password" class="form-control" placeholder="Masukan password baru" required>
                </div>
                <div class="custom-control custom-control-alternative custom-checkbox">
                  <input class="custom-control-input" id="remember" type="checkbox" name="remember">
                  <label class="custom-control-label" for="remember">
                      <span class="text">Ingat Saya</span>
                  </label>
                </div>
                <div class="mt-2 mb-3">
                  Belum memiliki akun? <a href="register">Daftar Sekarang</a>
                </div>
                <button class="btn btn-primary" type="submit">Masuk</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>