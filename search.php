<?php
require 'config.php';
$sub_judul = 'Periksa Pesanan | ';
if($_POST){
  $id = mysqli_real_escape_string($db, $_POST['id']);
  $cek_invoice = mysqli_query($db, "SELECT * FROM pesanan WHERE invoice = '$id'");
  $cek_invoice = mysqli_fetch_array($cek_invoice);
  if ($cek_invoice['id']) {
      header('Location: inv/'.$_POST['id']);
  }else{
    $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal kode invoice tidak ditemukan.</div></div></div>';
  }
}
require 'lib/header.php';
?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <h2 class="section-title">Periksa Pesanan</h2>
      <form method="POST">
        <div class="row">
          <?=$msg;?>
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <div class="form-group">
                  <label>Kode Invoice</label>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Masukan kode invoice" aria-label="" name="id" required autocomplete="false">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="submit"><i class="fas fa-search"></i> Periksa</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
  </section>
</div>
<?php
require 'lib/footer.php';
?>