<?php
require 'config.php';
$sub_judul = "Tentang | ";
require 'lib/header.php';
$tentang = mysqli_query($db, "SELECT * FROM tentang");
$data_tentang = mysqli_fetch_array($tentang);
?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <h2 class="section-title">Tentang Jasaviral</h2>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
            <?=$data_tentang['isi'];?>
           </div>
          </div>
        </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>