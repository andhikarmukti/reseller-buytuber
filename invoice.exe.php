<?php
require 'config.php';
include "lib/phpqrcode/qrlib.php";
$sub_judul = 'Invoice - ';

$uri_array = explode("/", $_SERVER['REQUEST_URI']);
$kode_invoice = mysqli_real_escape_string($db, $uri_array[2]); // fix + htmlspecial dibawah

$db_order = mysqli_query($db, "SELECT * FROM pesanan WHERE invoice = '$kode_invoice'");
$order = mysqli_fetch_array($db_order);

$order_user = mysqli_query($db, "SELECT * FROM users WHERE id = '$order[id_user]'");
$order_user = mysqli_fetch_array($order_user);

$db_layanan = mysqli_query($db, "SELECT * FROM layanan WHERE id = '$order[id_layanan]' AND status='1'");
$layanan = mysqli_fetch_array($db_layanan);

$harga_layanan = ($layanan['harga']) ? $layanan['harga'] : 0;
$harga_layanan = ($order_user['level'] == 'silver') ? $layanan['harga_silver'] : $harga_layanan;
$harga_layanan = ($order_user['level'] == 'gold') ? $layanan['harga_gold'] : $harga_layanan;
$harga_layanan = ($order_user['level'] == 'pro') ? $layanan['harga_pro'] : $harga_layanan;
$harga_layanan = $harga_layanan;

if($order['nama_kategori'] == 'Topup Saldo'){
  $layanan['harga'] = $order['data'];
}

if(!$order['id']){header('Location: ../'); die(); } // fix
$tambahan_keterangan = '';
$db_metode = mysqli_query($db, "SELECT * FROM metode_pembayaran WHERE id = '$order[metode_pembayaran]'");
$metode = mysqli_fetch_array($db_metode);

if($metode['id'] == 5){
  $res_metode = json_decode(base64_decode($order['res_pembayaran']), 1);
  $tambahan_keterangan .= 'Kode Pembayaran : <b>'.$res_metode['payment_code'].'</b><br><br>';
  $tambah_gambar = '<div class="text-md-left text-center"><img src="'.$domain.'assets/img/barcode.php?text='.$res_metode['payment_code'].'&print=true&size=60" class="rounded img-fluid" style="border-radius: 10px;" width="200px"></div>';
}

if($metode['id'] == 3){
  $res_metode = json_decode(base64_decode($order['res_pembayaran']), 1);
  QRcode::png($res_metode['qr_string'], $kode_invoice.'.png', QR_ECLEVEL_L, 3, 10);
  $qrImage = file_get_contents($kode_invoice.'.png'); unlink($kode_invoice.'.png');
  $tambah_gambar = '<div class="text-md-left text-center"><img src="data:image/png;base64,'.base64_encode($qrImage).'" class="rounded img-fluid" style="border-radius: 10px;" width="220px"></div>';
}

if($metode['id'] == 7){
  $res_metode = json_decode(base64_decode($order['res_pembayaran']), 1);
  $tambahan_keterangan .= '<a href="'.$res_metode['actions']['mobile_web_checkout_url'].'" class="btn btn-primary">Bayar Sekarang</a>';
}

if($metode['id'] == 8){
  $res_metode = json_decode(base64_decode($order['res_pembayaran']), 1);
  $tambahan_keterangan .= '<a href="'.$res_metode['actions']['mobile_deeplink_checkout_url'].'" class="btn btn-primary">Bayar Sekarang</a>';
}

if($metode['id'] == 9){
  $res_metode = json_decode(base64_decode($order['res_pembayaran']), 1);
  $tambahan_keterangan .= '<a href="'.$res_metode['actions']['mobile_web_checkout_url'].'" class="btn btn-primary">Bayar Sekarang</a>';
}
if($metode['id'] == 10 || $metode['id'] == 11 || $metode['id'] == 12 || $metode['id'] == 13 || $metode['id'] == 14){
  $res_metode = json_decode(base64_decode($order['res_pembayaran']), 1);
  $tambahan_keterangan .= 'Nomor Virtual Account Anda : <b>'.$res_metode['account_number'].'</b><br><br>';

}
if($order['status'] != 'not_paid' && $order['status'] != 'cancel'){
  $tambah_gambar = '';
  $tambahan_keterangan = '<h1 class="text-white bg-primary text-center p-1">LUNAS</h1>';
}

require 'lib/header.php';
?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <div class="row">
        <div class="col-12 mt-3 mb-3">
          <button class="btn btn-sm btn-primary" id="btn_print"><i class="fas fa-print"></i> Print Invoice</button>
        </div>
        <div class="col-12" id="area_print">
          <div class="card bg-dark-3">
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <h4>INVOICE</h4>
                </div>
                <div class="col-md-6 col-12">
                  <div class="mb-2">Kode : <?=htmlspecialchars($order['invoice']);?></div>
                  <div class="mb-2">Tanggal : <?=$order['tanggal'];?></div>
                </div>
                <div class="col-md-6 col-12">Status :
                  <span class="badge badge-primary"><?=ucwords($order['status']);?></span>
                </div>
                <div class="col-12">
                  <div class="bs-stepper">
                    <div class="bs-stepper-header" role="tablist">
                      <!-- your steps here -->
                      <div class="step <?=($order['status'] == 'not_paid') ? 'active' : '';?>" data-target="#logins-part">
                        <button type="button" class="step-trigger" role="tab" aria-controls="logins-part"
                          id="logins-part-trigger">
                          <span class="bs-stepper-circle"><i class="fas fa-money-bill-wave" style="margin-top: 2px;"></i></span>
                        </button>
                      </div>
                      <div class="line"></div>
                      <div class="step <?=($order['status'] == 'pending' || $order['status'] == 'processing') ? 'active' : '';?>" data-target="#information-part">
                        <button type="button" class="step-trigger" role="tab" aria-controls="information-part"
                          id="information-part-trigger">
                          <span class="bs-stepper-circle"><i class="fas fa-redo-alt" style="margin-top: 2px;"></i></span>
                        </button>
                      </div>
                      <div class="line"></div>
                      <div class="step <?=($order['status'] == 'success') ? 'active' : '';?>" data-target="#information-part">
                        <button type="button" class="step-trigger" role="tab" aria-controls="information-part"
                          id="information-part-trigger">
                          <span class="bs-stepper-circle"><i class="fas fa-check" style="margin-top: 2px;"></i></span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 mt-2">
                  <div class="alert alert-primary alert-dismissible show fade">
                    <div class="alert-body">
                      <button class="close" data-dismiss="alert">
                        <span>×</span>
                      </button>
                      Mohon transfer sesuai nominal yang ditentukan, supaya terproses secara otomatis.
                    </div>
                  </div>
                </div>
                <div class="col-12 table-responsive">
                  <table class="table table-dark table-striped">
                    <thead>
                      <th>Kategori</th>
                      <th>Layanan</th>
                      <th>Data</th>
                    </thead>
                    <tbody>
                      <tr>
                        <td><?=htmlspecialchars($order['nama_kategori']);?></td>
                        <td><?=htmlspecialchars($order['nama_layanan']);?></td>
                        <td><?=htmlspecialchars($order['data']);?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <?php if($order['keterangan']){ ?>
                <div class="col-12 mb-3">
                  <label>Keterangan</label>
                  <textarea class="form-control" rows="10" readonly><?=htmlspecialchars(rawurldecode($order['keterangan']));?></textarea>
                </div>
                <?php } ?>
                <div class="col-md-6 col-12 mb-3">
                  <?php if($metode['gambar']) { ?>
                    <img src="<?=$domain;?>assets/img/<?=json_decode($metode['gambar'], 1)[0];?>" style="width: 150px;" class="mb-2">
                  <?php } else { ?>
                    Metode Pembayaran : <?=$metode['nama'];?>
                  <?php } ?>
                  <p><?=$metode['detail'];?></p>
                  <?=$tambahan_keterangan;?>
                  <?=$tambah_gambar;?>
                </div>
                <div class="col-md-6 col-12">
                  <table class="table table-dark table-striped">
                    <tr>
                      <td>Harga</td>
                      <td>
                        <?="Rp ". number_format($order['harga'] - ($order['harga'] - $harga_layanan), 0, ',', ',');?>
                      </td>
                    </tr>
                    <tr>
                      <td>Fee Merchant</td>
                      <td><?="Rp ". number_format($order['harga'] - $harga_layanan, 0, ',', ',');?></td>
                    </tr>
                    <tr class="bg-primary font-weight-bold">
                      <td>Total yang harus dibayar</td>
                      <td><?="Rp ". number_format($order['harga'], 0, ',', ',');?></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>
<script>
  document.getElementById("btn_print").addEventListener("click", function () {
    var printContents = document.getElementById('area_print').innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  });
</script>