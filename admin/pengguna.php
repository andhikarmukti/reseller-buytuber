<?php
require '../config.php';
$sub_judul = ' - Kelola Pengguna';
require 'lib/header.php';
$aksi = key($_GET);



if($aksi == 'edit'){
  $id = mysqli_real_escape_string($db, $_GET['edit']);
  if ($_POST) {
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $nama = mysqli_real_escape_string($db, $_POST['nama']);
    $ponsel = mysqli_real_escape_string($db, $_POST['ponsel']);
    $saldo = mysqli_real_escape_string($db, $_POST['saldo']);
    $level = mysqli_real_escape_string($db, $_POST['level']);
    $status_akun = mysqli_real_escape_string($db, $_POST['status_akun']);
    
    $email = htmlspecialchars($email);
    $nama = htmlspecialchars($nama);
    $ponsel = htmlspecialchars($ponsel);
    $saldo = htmlspecialchars($saldo);
    $status_akun = htmlspecialchars($status_akun);
    $level = htmlspecialchars($level);
    if ($nama) {
        $log_msg = "Merubah user $email, ponsel $ponsel, level $level dan saldo $saldo";
        mysqli_query($db, "INSERT INTO log VALUES(null, '$data_user[email]', '$log_msg', '$_SERVER[REMOTE_ADDR]', '".date('Y-m-d H:i:s')."')");
        
        $query = mysqli_query($db,"UPDATE users SET email = '$email', nama = '$nama', ponsel = '$ponsel', saldo = '$saldo', level = '$level', status = '$status_akun' WHERE id = '$id'");
        $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
    } else {
        $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
    }
  }
  $data_edit = mysqli_query($db,"SELECT * FROM users WHERE id = '$id'");
  $data_edit = mysqli_fetch_array($data_edit);
}


if($aksi == 'hapus'){
  $id = mysqli_real_escape_string($db, $_GET['hapus']);
  $query = mysqli_query($db,"DELETE FROM users WHERE id = '$id'");
}

?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
  <h2 class="section-title">Kelola Pengguna</h2>

    <div class="section-body">
      <div class="row">
        <?=$msg;?>
        <?php if($aksi == 'edit'){ ?>
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h5><?=ucwords($aksi);?></h5>
            </div>
            <div class="card-body">
              <form method="POST">
                <div class="form-group">
                  <label>Email</label>
                  <input type="text" class="form-control" name="email"  value="<?=htmlspecialchars($data_edit['email']);?>" readonly>
                </div>
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama" placeholder="nama" value="<?=htmlspecialchars($data_edit['nama']);?>">
                </div>
                <div class="form-group">
                  <label>Ponsel</label>
                  <input type="number" class="form-control" name="ponsel" placeholder="ponsel" value="<?=htmlspecialchars($data_edit['ponsel']);?>">
                </div>
                <div class="form-group">
                  <label>Saldo</label>
                  <input type="text" class="form-control" name="saldo" placeholder="saldo" value="<?=htmlspecialchars($data_edit['saldo']);?>">
                </div>
                <div class="form-group">
                  <label>Level</label>
                  <select name="level" class="form-control">
                    <option value="0">Pilih Level</option>
                   <option <?=($data_edit['level'] == 'member') ? 'selected' : '';?>>member</option> 
                   <option <?=($data_edit['level'] == 'silver') ? 'selected' : '';?>>silver</option> 
                   <option <?=($data_edit['level'] == 'gold') ? 'selected' : '';?>>gold</option> 
                   <option <?=($data_edit['level'] == 'pro') ? 'selected' : '';?>>pro</option> 
                   <option <?=($data_edit['level'] == 'admin') ? 'selected' : '';?>>admin</option> 
                    </select>
                </div>
                <div class="form-group">
                  <label>Status Akun</label>
                  <select name="status_akun" class="form-control">
                       <option value="0">Pilih Status</option>
                       <option value="1" <?=($data_edit['status'] == 1) ? 'selected' : '';?>>Aktif</option> 
                       <option value="0" <?=($data_edit['status'] == 0) ? 'selected' : '';?>>Suspend</option> 
                    </select>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-danger" href="?">Tutup</a>
              </form>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive table-body" id="messages">
                <table class="table table-striped" id="table_ajax" style="width: 100%">
                  <thead class="thead-light">
                    <tr>
                      <th>ID</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Ponsel</th>
                      <th>Saldo</th>
                      <th>Level</th>
                      <th>Status</th>
                      <th>Tanggal Daftar</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>
<script>
  $(document).ready(function () {
    $('#table_ajax').DataTable({
      "order": [[0, 'desc']],
      "processing": false,
      "serverSide": true,
      "bInfo": false,
      "ajax": "ajax/list_pengguna.php",
      "language": {
        "paginate": {
          "previous": "<i class='fas fa-angle-left'></i>",
          "next": "<i class='fas fa-angle-right'></i>",
        }
      }
    });
  });
   function confirmation(ev) {
        ev.preventDefault();
        var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
        console.log(urlToRedirect); // verify if this is the right URL
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Your Data has been deleted!", {
               icon: "success",
               
              })
              window.location.href = urlToRedirect;
            } else {
              swal("Your Data is safe!");
            }
        });
    }
</script>