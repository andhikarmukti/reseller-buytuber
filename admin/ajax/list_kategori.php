<?php
require '../../config.php';
require '../lib/cek_admin.php';
require '../../lib/ssp.class.php';
$table = 'kategori';
$primaryKey = 'id';

$columns = array(
    array('db' => '`a`.`id`', 'dt' => 0, 'field' => 'id'),
    array('db' => '`a`.`id_jenis`', 'dt' => 1, 'field' => 'id_jenis', 'formatter' => function($i) use ($db){
        $jenis = mysqli_query($db, "SELECT * FROM jenis WHERE id = '$i'");
        $jenis_data = mysqli_fetch_array($jenis);

        $nama = $jenis_data['nama'];
        return $nama;
    }),
    array('db' => '`a`.`nama`', 'dt' => 2, 'field' => 'nama'),
    array('db' => '`a`.`sub_nama`', 'dt' => 3, 'field' => 'sub_nama'),
    array('db' => '`a`.`detail`', 'dt' => 4, 'field' => 'detail'),
    array('db' => '`a`.`status`', 'dt' => 5, 'field' => 'status', 'formatter' => function($x, $r){
        $format = ($x) ? '<a class="text-primary" href="?on='.$r[0].'"><i class="fas fa-toggle-on" style="font-size: 30px;"></i></a>' : '<a class="text-danger" href="?off='.$r[0].'"><i class="fas fa-toggle-off" style="font-size: 30px;"></i></a>';
        return $format;
    }),
    array('db' => '`a`.`id`', 'dt' => 6, 'field' => 'id', 'formatter' => function($x){
        $format = '
            <a class="btn btn-sm btn-primary text-white" href="?edit='.$x.'"><i class="fas fa-pen"></i> Edit</a>
            <a class="btn btn-sm btn-danger text-white" href="?hapus='.$x.'" onclick="confirmation(event)"><i class="fas fa-trash"></i> Hapus</a>
        ';
        return $format;
    }),
);

$sql_details = array(
    'user' => $db_username,
    'pass' => $db_password,
    'db'   => $db_name,
    'host' => $db_server
);

$joinQuery = "FROM `{$table}` AS `a` LEFT JOIN `jenis` AS `b` ON (`b`.`id` = `a`.`id_jenis`)";
$extraWhere = "";
$groupBy = '';
$having = '';

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
);