<?php
require '../../config.php';
require '../lib/cek_admin.php';
require '../../lib/ssp.class.php';
$table = 'jenis';
$primaryKey = 'id';

$columns = array(
    array( 'db' => 'id', 'dt' => 0),
    array( 'db' => 'nama', 'dt' => 1),
    array( 'db' => 'icon','dt' => 2, 'formatter' => function($x, $r){
        $format = '<i class="'.$x.'" style="font-size:20px;"></i>';
        return $format;
    }),
    array( 'db' => 'status','dt' => 3, 'formatter' => function($x, $r){
        $format = ($x) ? '<a class="text-primary" href="?on='.$r[0].'"><i class="fas fa-toggle-on" style="font-size: 30px;"></i></a>' : '<a class="text-danger" href="?off='.$r[0].'"><i class="fas fa-toggle-off" style="font-size: 30px;"></i></a>';
        return $format;
    }),
    array( 'db' => 'id','dt' => 4, 'formatter' => function($x){
        $format = '
            <a class="btn btn-sm btn-primary text-white" href="?edit='.$x.'"><i class="fas fa-pen"></i> Edit</a>
            <a class="btn btn-sm btn-danger text-white" href="?hapus='.$x.'" onclick="confirmation(event)"><i class="fas fa-trash"></i> Hapus</a>
        ';
        return $format;
    }),
);

$sql_details = array(
    'user' => $db_username,
    'pass' => $db_password,
    'db'   => $db_name,
    'host' => $db_server
);

$joinQuery = "";
$extraWhere = "";
$groupBy = '';
$having = '';

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
);