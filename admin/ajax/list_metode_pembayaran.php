<?php
require '../../config.php';
require '../lib/cek_admin.php';
require '../../lib/ssp.class.php';
session_start();
$user_id = $_SESSION['id'];
$table = 'metode_pembayaran';
$primaryKey = 'id';

$columns = array(
    array( 'db' => 'id', 'dt' => 0),
    array( 'db' => 'nama', 'dt' => 1), 
    array( 'db' => 'tipe', 'dt' => 2), 
    array( 'db' => 'detail', 'dt' => 3),
    array( 'db' => 'rate', 'dt' => 4), 
    array( 'db' => 'rate_persen', 'dt' => 5), 
    array('db' => 'status', 'dt' => 6, 'formatter' => function($x, $r){
        $format = ($x) ? '<a class="text-primary" href="?on='.$r[0].'"><i class="fas fa-toggle-on" style="font-size: 30px;"></i></a>' : '<a class="text-danger" href="?off='.$r[0].'"><i class="fas fa-toggle-off" style="font-size: 30px;"></i></a>';
        return $format;
    }),
    array( 'db' => 'id','dt' => 7, 'formatter' => function($x){
        $format = '
            <a class="btn btn-sm btn-primary text-white" href="?edit='.$x.'"><i class="fas fa-pen"></i> Edit</a>
            <a class="btn btn-sm btn-danger text-white" href="?hapus='.$x.'" onclick="confirmation(event)"><i class="fas fa-trash"></i> Hapus</a>
        ';
        return $format;
    }),
);

$sql_details = array(
    'user' => $db_username,
    'pass' => $db_password,
    'db'   => $db_name,
    'host' => $db_server
);

$joinQuery = "";
$extraWhere = "";
$groupBy = '';
$having = '';

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
);