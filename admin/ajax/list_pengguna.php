<?php
require '../../config.php';
require '../lib/cek_admin.php';
require '../../lib/ssp.class.php';
session_start();
$user_id = $_SESSION['id'];
$table = 'users';
$primaryKey = 'id';

$columns = array(
    array( 'db' => 'id', 'dt' => 0),
    array( 'db' => 'nama', 'dt' => 1, 'formatter' => function($i) { return htmlspecialchars($i); }),
    array( 'db' => 'email', 'dt' => 2, 'formatter' => function($i) { return htmlspecialchars($i); }),
    array( 'db' => 'ponsel', 'dt' => 3, 'formatter' => function($i) { return htmlspecialchars($i); }),
    array( 'db' => 'saldo',  'dt' => 4, 'formatter' => function($i) {
        return "Rp ".number_format($i,0,',','.');
    }),
    array( 'db' => 'level', 'dt' => 5),
    array( 'db' => 'status', 'dt' => 6, 'formatter' => function($x) {
        if ($x == '1') {
            $label = 'primary';
            $ket = "Aktif";
        } else {
            $label = 'danger';
            $ket = "Suspend";
        }
        return '<span class="badge badge-'.$label.'">'.$ket.'</span>';
    }),
    array( 'db' => 'tanggal', 'dt' => 7),
    array( 'db' => 'id','dt' => 8, 'formatter' => function($x){
        $format = '
        <a href="?edit='.$x.'" class="btn btn-sm btn-primary" data-toggle="tooltip" data-original-title="Edit Pengguna">
        <i class="fas fa-user-edit"></i>
      </a>
      <a href="?hapus='.$x.'" class="btn btn-sm btn-danger" data-toggle="tooltip" data-original-title="Delete Pengguna" onclick="confirmation(event)">
      <i class="fas fa-trash"></i>
    </a>
        ';
        return $format;
    }),
);

$sql_details = array(
    'user' => $db_username,
    'pass' => $db_password,
    'db'   => $db_name,
    'host' => $db_server
);

$joinQuery = "";
$extraWhere = "";
$groupBy = '';
$having = '';

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
);