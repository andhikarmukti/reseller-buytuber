<?php
require '../../config.php';
require '../lib/cek_admin.php';
require '../../lib/ssp.class.php';
session_start();
$user_id = $_SESSION['id'];
$table = 'pesanan';
$primaryKey = 'id';

$columns = array(
    array( 'db' => 'id', 'dt' => 0),
    array( 'db' => 'nama', 'dt' => 1, 'formatter' => function($i) { return htmlspecialchars($i); }),
    array( 'db' => 'nama_layanan', 'dt' => 2, 'formatter' => function($i) { return htmlspecialchars($i); }), // special chr
    array( 'db' => 'nama_kategori', 'dt' => 3, 'formatter' => function($i) { return htmlspecialchars($i); }), // special chr
    array( 'db' => 'data', 'dt' => 4, 'formatter' => function($i) { return '<input style="width:200px" readonly type="text" class="form-control" value="'.htmlspecialchars($i).'">'; }),
    array( 'db' => 'keterangan', 'dt' => 5, 'formatter' => function($i) { return '<input style="width:200px" readonly type="text" class="form-control" value="'.htmlspecialchars($i).'">'; }),
    // sampe sini
    array( 'db' => 'harga',  'dt' => 6, 'formatter' => function($i) {
        return "Rp ".number_format($i,0,',','.');
    }),
    array('db' => 'metode_pembayaran', 'dt' => 7, 'formatter' => function($i) use ($db){
        $metode_pembayaran = mysqli_query($db, "SELECT * FROM metode_pembayaran WHERE id = '$i'");
        $metode_pembayaran_data = mysqli_fetch_array($metode_pembayaran);

        $nama = $metode_pembayaran_data['nama'];
        return $nama;
    }), 
    array( 'db' => 'invoice', 'dt' => 8), 
    array( 'db' => 'kontak', 'dt' => 9, 'formatter' => function($i) { return htmlspecialchars($i); }), 
    array( 'db' => 'status', 'dt' => 10, 'formatter' => function($i, $r) use ($conn){
        $status = 'Null';
        $disabled = '';
        if($i == 'not_paid'){
            $status = 'Not Paid';
            $alert_type = 'dark';
        }else if($i == 'pending'){
            $status = 'Pending';
            $alert_type = 'warning';
        }else if($i == 'processing'){
            $status = 'Processing';
            $alert_type = 'info';
        }else if($i == 'success'){
            $disabled = 'disabled';
            $status = 'Success';
            $alert_type = 'primary';
        }else if($i == 'cancel'){
            $disabled = 'disabled';
            $status = 'Canceled';
            $alert_type = 'danger';
        }else if($i == 'refund'){
            $disabled = 'disabled';
            $status = 'Refund';
            $alert_type = 'warning';
        }else if($i == 'paid'){
            $status = 'Paid';
            $alert_type = 'success';
        }
        $btn = '
        <div class="btn-group" role="group"><button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-'.$alert_type.' dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" '.$disabled.'>'.$status.'</button>
        <div class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -2px, 0px);">
            <a class="dropdown-item" href="#" onclick="set_status(\'set_status_pesanan\', \''.$r[0].'\', \'pending\')">Pending</a>
            <a class="dropdown-item" href="#" onclick="set_status(\'set_status_pesanan\', \''.$r[0].'\', \'processing\')">Processing</a>
            <a class="dropdown-item" href="#" onclick="set_status(\'set_status_pesanan\', \''.$r[0].'\', \'success\')">Success</a>
            <a class="dropdown-item" href="#" onclick="set_status(\'set_status_pesanan\', \''.$r[0].'\', \'cancel\')">Canceled</a>
        </div></div>
        ';
        return $btn;
    }),
    array( 'db' => 'tanggal', 'dt' => 11), 
    array( 'db' => 'id','dt' => 12, 'formatter' => function($x){
        $format = '
            <a class="btn btn-sm btn-dark text-white" href="pesanan_provider?id='.$x.'" target="_blank"><i class="fas fa-fire"></i></a>
            <a class="btn btn-sm btn-primary text-white" href="?edit='.$x.'"><i class="fas fa-pen"></i></a>
            <a class="btn btn-sm btn-danger text-white" href="?hapus='.$x.'" onclick="confirmation(event)"><i class="fas fa-trash"></i></a>
        ';
        return $format;
    }),
);

$sql_details = array(
    'user' => $db_username,
    'pass' => $db_password,
    'db'   => $db_name,
    'host' => $db_server
);

$joinQuery = "";
$extraWhere = "";
$groupBy = '';
$having = '';

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
);