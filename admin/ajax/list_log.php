<?php
require '../../config.php';
require '../lib/cek_admin.php';
require '../../lib/ssp.class.php';
session_start();
$user_id = $_SESSION['id'];
$table = 'log';
$primaryKey = 'id';

$columns = array(
    array( 'db' => 'created_at', 'dt' => 0),
    array( 'db' => 'email', 'dt' => 1, 'formatter' => function($i) { return htmlspecialchars($i); }),
    array( 'db' => 'ip', 'dt' => 2, 'formatter' => function($i) { return htmlspecialchars($i); }),
    array( 'db' => 'msg', 'dt' => 3, 'formatter' => function($i) { return htmlspecialchars($i); }),
);

$sql_details = array(
    'user' => $db_username,
    'pass' => $db_password,
    'db'   => $db_name,
    'host' => $db_server
);

$joinQuery = "";
$extraWhere = "";
$groupBy = '';
$having = '';

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
);