<?php
require '../../config.php';
require '../lib/cek_admin.php';
require '../../lib/ssp.class.php';
$table = 'layanan';
$primaryKey = 'id';

$columns = array(
    array( 'db' => 'id', 'dt' => 0),
    array( 'db' => 'id_kategori',  'dt' => 1, 'formatter' => function($i) use ($db) {
  		 $kategori= mysqli_query($db, 'SELECT * FROM kategori WHERE id="'.$i.'"');
        $data_kategori = mysqli_fetch_array($kategori);
        $data = $data_kategori['nama'];
        return $data;
    }),
    array( 'db' => 'id_provider',  'dt' => 2, 'formatter' => function($i) use ($db) {
  		$provider = mysqli_query($db, 'SELECT * FROM provider WHERE id="'.$i.'"');
        $data_provider = mysqli_fetch_array($provider);
        $data = $data_provider['nama'];
        return $data;
    }),
    array( 'db' => 'nama', 'dt' => 3),
    array( 'db' => 'harga',  'dt' => 4, 'formatter' => function($i) {
        return "Rp ".number_format($i,0,',','.');
    }),
    array( 'db' => 'harga_silver',  'dt' => 5, 'formatter' => function($i) {
        return "Rp ".number_format($i,0,',','.');
    }),
    array( 'db' => 'harga_gold',  'dt' => 6, 'formatter' => function($i) {
        return "Rp ".number_format($i,0,',','.');
    }),
    array( 'db' => 'harga_pro',  'dt' => 7, 'formatter' => function($i) {
        return "Rp ".number_format($i,0,',','.');
    }),
    array( 'db' => 'harga_asli',  'dt' => 8, 'formatter' => function($i) {
        return "Rp ".number_format($i,0,',','.');
    }),
    array( 'db' => 'status','dt' => 9, 'formatter' => function($x, $r){
        $format = ($x) ? '<a class="text-primary" href="?on='.$r[0].'"><i class="fas fa-toggle-on" style="font-size: 30px;"></i></a>' : '<a class="text-danger" href="?off='.$r[0].'"><i class="fas fa-toggle-off" style="font-size: 30px;"></i></a>';
        return $format;
    }),
    array( 'db' => 'tanggal', 'dt' => 10),
    array( 'db' => 'id','dt' => 11, 'formatter' => function($x){
        $format = '
            <a class="btn btn-sm btn-primary text-white" href="?edit='.$x.'"><i class="fas fa-pen"></i> Edit</a>
            <a class="btn btn-sm btn-danger text-white" href="?hapus='.$x.'" onclick="confirmation(event)"><i class="fas fa-trash"></i> Hapus</a>
        ';
        return $format;
    }),
);

$sql_details = array(
    'user' => $db_username,
    'pass' => $db_password,
    'db'   => $db_name,
    'host' => $db_server
);

$joinQuery = "";
if($_SESSION['set_category'] == 'Semua Kategori'){
    $extraWhere = "";
}else if($_SESSION['set_category']){
    $nama_kategori = mysqli_real_escape_string($db, $_SESSION['set_category']);
    $kategori = mysqli_query($db, "SELECT * FROM kategori WHERE nama = '$nama_kategori'");
    $kategori = mysqli_fetch_array($kategori);
    $extraWhere = "id_kategori='$kategori[id]'";
}else{
    $extraWhere = "";
}
$groupBy = '';
$having = '';

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
);