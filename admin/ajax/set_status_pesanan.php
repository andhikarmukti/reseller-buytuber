<?php
require '../../config.php';
require '../lib/cek_admin.php';

$id = mysqli_real_escape_string($db, $_POST['id']);
$status = mysqli_real_escape_string($db, $_POST['val']);

$res['status'] = false;
if($id && $status){
    
    $is_order = mysqli_query($db, "SELECT * FROM pesanan WHERE id = '$id'");
    $is_order = mysqli_fetch_array($is_order);
    
    $log_msg = "Merubah status pesanan $id ke $status";
    mysqli_query($db, "INSERT INTO log VALUES(null, '$data_user[email]', '$log_msg', '$_SERVER[REMOTE_ADDR]', '".date('Y-m-d H:i:s')."')");
    
    $status_allowed_pending = array('paid', 'processing', 'not_paid');
    $status_allowed = array('pending', 'processing', 'success', 'cancel');
    
    if(in_array($status, $status_allowed)){

        if($status == 'pending'){
            if(in_array($is_order['status'], $status_allowed_pending)){
                mysqli_query($db, "UPDATE pesanan SET status = '$status', reorder = reorder + 1, res_provider = '' WHERE id='$id'");
                $res['status'] = true;
                $res['msg'] = 'success set status to '.$status;
            }else{
                $res['msg'] = 'Dikarenakan status orderan saat ini anda tidak dapat merubah status ke pending';
            }
        }else{
            mysqli_query($db, "UPDATE pesanan SET status = '$status' WHERE id='$id'");
            $res['status'] = true;
            $res['msg'] = 'success set status to '.$status;
        }
    }else{
        $res['msg'] = 'Hanya status tertentu yang dapat diubah';
    }
}else{
    $res['msg'] = 'paramenter tidak lengkap';
}

print_r(json_encode($res));