<?php
require 'cek_admin.php';

for ($i = 0; $i < count($_FILES['aplikasi']); $i++) {
    $time        = time();
    $nama_aplikasi = $_FILES['aplikasi']['name'][$i];
    $size        = $_FILES['aplikasi']['size'][$i];
    $error       = $_FILES['aplikasi']['error'][$i];
    $tipe_video  = $_FILES['aplikasi']['type'][$i];
    $folder      = "../assets/img/kategori/";
    $valid       = array('jpg','png','gif','jpeg','JPG','PNG','GIF','JPEG');
    if (strlen($nama_aplikasi)) {
        list($txt, $ext) = explode(".", $nama_aplikasi);
        if (in_array($ext, $valid)) {
            if ($size<5000000) {
                $gambarnya_aplikasi = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
                $gmbr  = $folder.$gambarnya;
                $tmp = $_FILES['aplikasi']['tmp_name'][$i];
                if (move_uploaded_file($tmp, $folder.$gambarnya_aplikasi)) {
                    $data_gambar_aplikasi[] = $gambarnya_aplikasi;
                } else {
                    //echo "gagal";
                }
            } else {
                //echo "Max 5mb";
            }
        } else {
            //echo "Extensi file tidak didukung";
        }
    } else {
        //echo 'Gambar belum dipilih';
    }
}
$result_gambar_aplikasi = json_encode($data_gambar_aplikasi);