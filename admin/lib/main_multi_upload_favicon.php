<?php
require 'cek_admin.php';

for ($i = 0; $i < count($_FILES['favicon']); $i++) {
    $time        = time();
    $nama_favicon = $_FILES['favicon']['name'][$i];
    $size        = $_FILES['favicon']['size'][$i];
    $error       = $_FILES['favicon']['error'][$i];
    $tipe_video  = $_FILES['favicon']['type'][$i];
    $folder      = "../assets/img/";
    $valid       = array('jpg','png','gif','jpeg','JPG','PNG','GIF','JPEG');
    if (strlen($nama_favicon)) {
        list($txt, $ext) = explode(".", $nama_favicon);
        if (in_array($ext, $valid)) {
            if ($size<5000000) {
                $faviconnya = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
                $gmbr  = $folder.$faviconnya;
                $tmp = $_FILES['favicon']['tmp_name'][$i];
                if (move_uploaded_file($tmp, $folder.$faviconnya)) {
                    $data_favicon[] = $faviconnya;
                } else {
                    //echo "gagal";
                }
            } else {
                //echo "Max 5mb";
            }
        } else {
            //echo "Extensi file tidak didukung";
        }
    } else {
        //echo 'favicon belum dipilih';
    }
}
$result_favicon = json_encode($data_favicon);