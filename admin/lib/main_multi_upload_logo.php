<?php
require 'cek_admin.php';

for ($i = 0; $i < count($_FILES['logo']); $i++) {
    $time        = time();
    $nama_logo = $_FILES['logo']['name'][$i];
    $size        = $_FILES['logo']['size'][$i];
    $error       = $_FILES['logo']['error'][$i];
    $tipe_video  = $_FILES['logo']['type'][$i];
    $folder      = "../assets/img/";
    $valid       = array('jpg','png','gif','jpeg','JPG','PNG','GIF','JPEG');
    if (strlen($nama_logo)) {
        list($txt, $ext) = explode(".", $nama_logo);
        if (in_array($ext, $valid)) {
            if ($size<5000000) {
                $logonya = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
                $gmbr  = $folder.$logonya;
                $tmp = $_FILES['logo']['tmp_name'][$i];
                if (move_uploaded_file($tmp, $folder.$logonya)) {
                    $data_logo[] = $logonya;
                } else {
                    //echo "gagal";
                }
            } else {
                //echo "Max 5mb";
            }
        } else {
            //echo "Extensi file tidak didukung";
        }
    } else {
        //echo 'logo belum dipilih';
    }
}
$result_logo = json_encode($data_logo);