  </div>
  </div>
  <div class="footer">
    <footer class="main-footer">
      <div class="text-left">
        Copyright &copy; 2021 <a href="<?=$domain;?>" class="text-white">BisaCash</a>
      </div>
    </footer>

    <!-- General JS Scripts -->
    <script src="<?=$domain;?>assets/modules/jquery.min.js"></script>
    <script src="<?=$domain;?>assets/modules/popper.js"></script>
    <script src="<?=$domain;?>assets/modules/tooltip.js"></script>
    <script src="<?=$domain;?>assets/modules/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=$domain;?>assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="<?=$domain;?>assets/modules/moment.min.js"></script>
    <script src="<?=$domain;?>assets/js/stisla.js"></script>



    <!-- JS Libraies -->
    <script src="<?=$domain;?>assets/modules/datatables/datatables.min.js"></script>
    <script src="<?=$domain;?>assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?=$domain;?>assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
    <script src="<?=$domain;?>assets/modules/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=$domain;?>assets/modules/select2/dist/js/select2.full.min.js"></script>
    <script src="<?=$domain;?>assets/modules/jquery-selectric/jquery.selectric.min.js"></script>
    <script src="<?=$domain;?>assets/modules/chocolat/dist/js/jquery.chocolat.min.js"></script>
    <script src="<?=$domain;?>assets/modules/sweetalert/sweetalert.min.js"></script>
    <script src="<?=$domain;?>assets/modules/izitoast/js/iziToast.min.js"></script>
    <script src="<?=$domain;?>assets/modules/easy-autocomplete/jquery.easy-autocomplete.min.js"></script>

    <!-- Template JS File -->
    <script src="<?=$domain;?>assets/js/scripts.js"></script>
    <script src="<?=$domain;?>assets/js/custom.js"></script>
    </body>

</html>
<script>
  function set_status(url, id, val) {
    $.ajax({
      data: 'val=' + val + '&id=' + id,
      type: "post",
      url: "<?=$domain;?>admin/ajax/" + url,
      success: function (dataResult) {
        var dataResult = JSON.parse(dataResult);
        if (dataResult.status) {
          iziToast.success({
            title: 'Success',
            message: dataResult.msg,
            position: 'topRight'
          });
          $('#table_ajax').DataTable().ajax.reload(null, false);
        } else {
          iziToast.warning({
            title: 'Failed',
            message: dataResult.msg,
            position: 'topRight'
          });
        }
      },
      beforeSend: function (data) {

      },
      error: function (data) {
        iziToast.danger({
          title: 'Error',
          message: 'System error',
          position: 'topRight'
        });
      }
    });
  }
</script>