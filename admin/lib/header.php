<?php
  require 'cek_admin.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title><?=$judul.$sub_judul;?></title>
  <meta name="robots" content="index, follow" />
  <meta name="description" content="<?php echo $config['web']['meta']['description'] ?>">
  <meta name="keywords" content="<?php echo $config['web']['meta']['keywords'] ?>">
  <meta name="author" content="<?php echo $config['web']['meta']['author'] ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/fontawesome/css/all.min.css">

  <!-- Favicon -->
  <link rel="icon" href="<?=$domain;?>assets/img/<?=$favfav['0'];?>" type="image/png">
  <!-- Fonts -->
  <!-- CSS Libraries -->
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/jquery-selectric/selectric.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/chocolat/dist/css/chocolat.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/izitoast/css/iziToast.min.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/modules/easy-autocomplete/easy-autocomplete.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?=$domain;?>assets/css/style.css">
  <link rel="stylesheet" href="<?=$domain;?>assets/css/components.css">
  <style>
    .navbar-bg {
      height: 67px;
    }
    body {
      background-color: #f0f6ff;
    }
  </style>
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown"
              class="nav-link dropdown-toggle nav-link-lg nav-link-user">
              <img alt="image" src="../assets/img/avatar/avatar-1.png" width="30" class="rounded-circle mr-1">
              <div class="d-sm-none d-lg-inline-block">Hi, Admin</div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a href="<?=$domain;?>" class="dropdown-item has-icon">
                <i class="fas fa-arrow-left"></i> Kembali ke Web
              </a>
              <a href="<?=$domain;?>logout" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href=".">Admin Panel</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href=".">AP</a>
          </div>
          <ul class="sidebar-menu">
            <li><a class="nav-link" href="index.php"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-box-open"></i><span>Layanan</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?=$domain;?>admin/provider">Provider</a></li>
                <li><a class="nav-link" href="<?=$domain;?>admin/jenis">Jenis</a></li>
                <li><a class="nav-link" href="<?=$domain;?>admin/kategori">Kategori</a></li>
                <li><a class="nav-link" href="<?=$domain;?>admin/layanan">Layanan</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-shopping-cart"></i><span>Pesanan</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?=$domain;?>admin/pesanan">Kelola Pesanan</a></li>
                <li><a class="nav-link" href="<?=$domain;?>admin/laporan_pesanan">Laporan</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-money-bill-wave"></i><span>Pembayaran</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?=$domain;?>admin/metode_pembayaran">Metode Pembayaran</a></li>

              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>Pengguna</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?=$domain;?>admin/pengguna">Kelola Pengguna</a></li>
                <li><a class="nav-link" href="<?=$domain;?>admin/transfer_pengguna">Transfer Saldo</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-cog"></i><span>Pengelola Web</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?=$domain;?>admin/slider">Slider</a></li>
                <li><a class="nav-link" href="<?=$domain;?>admin/popup">Popup</a></li>
                <li><a class="nav-link" href="<?=$domain;?>admin/pengaturan">Pengaturan</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-columns"></i><span>Halaman</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?=$domain;?>admin/tentang">Tentang</a></li>
                <li><a class="nav-link" href="<?=$domain;?>admin/kebijakan">Kebijakan Privasi</a></li>
                <li><a class="nav-link" href="<?=$domain;?>admin/faq">FAQ</a></li>
                <li><a class="nav-link" href="<?=$domain;?>admin/ketentuan">Ketentuan Layanan</a></li>
              </ul>
            </li>
            <li><a class="nav-link" href="<?=$domain;?>admin/log"><i class="fas fa-list"></i> <span>Log Aktivitas</span></a></li>
          </ul>
        </aside>
      </div>
      <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_admin"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
          </div>
        </div>
      </div>