<?php
require 'cek_admin.php';

for ($i = 0; $i < count($_FILES['gambar_2']); $i++) {
    $time        = time();
    $nama_gambar = $_FILES['gambar_2']['name'][$i];
    $size        = $_FILES['gambar_2']['size'][$i];
    $error       = $_FILES['gambar_2']['error'][$i];
    $tipe_video  = $_FILES['gambar_2']['type'][$i];
    $folder      = "../assets/img/idserver/";
    $valid       = array('jpg','png','gif','jpeg','JPG','PNG','GIF','JPEG');
    if (strlen($nama_gambar)) {
        list($txt, $ext) = explode(".", $nama_gambar);
        if (in_array($ext, $valid)) {
            if ($size<5000000) {
                $gambarnya = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
                $gmbr  = $folder.$gambarnya;
                $tmp = $_FILES['gambar_2']['tmp_name'][$i];
                if (move_uploaded_file($tmp, $folder.$gambarnya)) {
                    $data_gambar[] = $gambarnya;
                } else {
                    //echo "gagal";
                }
            } else {
                //echo "Max 5mb";
            }
        } else {
            //echo "Extensi file tidak didukung";
        }
    } else {
        //echo 'Gambar belum dipilih';
    }
}
$result_gambar_dua = json_encode($data_gambar);