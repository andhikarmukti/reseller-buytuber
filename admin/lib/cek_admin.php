<?php 
if ($data_user['level'] != 'admin') {
    header('Location: '.$domain.'404');
    die('404 not found');
}