<?php
date_default_timezone_set('Asia/Jakarta');

require '../config.php';

include '../lib/model.php';
$model = new Model();


function validate_date($date)
{
	$d = DateTime::createFromFormat('Y-m-d', $date);
	return $d && $d->format('Y-m-d') == $date;
}
function format_date($date)
{
	$split = explode("-", $date);
	$month = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
	if (in_array($split[1], array_keys($month)) == false) {
		return "Something went wrong.";
	}
	return $split[2] . ' ' . $month[$split[1]] . ' ' . $split[0];
}

$deposit = $model->db_query($db, "*", "pesanan");
$success = $model->db_query($db, "SUM(harga) AS total", "pesanan", "status IN ('success')");
$opit = $model->db_query($db, "SUM(keuntungan) AS total", "pesanan", "status IN ('success')");
if (isset($_GET['start_date']) and isset($_GET['end_date'])) {
	if (validate_date($_GET['start_date']) == false or validate_date($_GET['end_date']) == false) {
		exit('Input tidak sesuai.');
	}
	$deposit = $model->db_query($db, "*", "pesanan", "DATE(tanggal) BETWEEN '".mysqli_real_escape_string($db, $_GET['start_date'])."' AND '".mysqli_real_escape_string($db, $_GET['end_date'])."'");
	$success = $model->db_query($db, "SUM(harga) AS total", "pesanan", "status IN ('success') AND DATE(tanggal) BETWEEN '" . mysqli_real_escape_string($db, $_GET['start_date']) . "' AND '" . mysqli_real_escape_string($db, $_GET['end_date']) . "'");
	$opit = $model->db_query($db, "SUM(keuntungan) AS total", "pesanan", "status IN ('success') AND DATE(tanggal) BETWEEN '" . mysqli_real_escape_string($db, $_GET['start_date']) . "' AND '" . mysqli_real_escape_string($db, $_GET['end_date']) . "'");
}
$sub_judul = ' - Laporan Pesanan';
require 'lib/header.php';

?>
<!-- Main Content -->
<div class="main-content">
	<section class="section">
		<h2 class="section-title">Laporan Pesanan</h2>

		<div class="section-body">

			<div class="col-md-12">
				<div class="card">
					<!-- Card header -->
					<div class="card-header">
						<h4><i class="fa fa-info-circle"></i> Menampilkan Informasi: <?php echo (isset($_GET['start_date']) and isset($_GET['end_date'])) ? 'Tanggal ' . format_date($_GET['start_date']) . ' sampai ' . format_date($_GET['end_date']) : 'Seluruh Pesanan' ?></h4>
					</div>
					<!-- Card body -->

					<form method="get">
						<div class="card-body">
							<div class="row">

								<div class="col-md-4">
									<div class="form-group row">
										<div class="col-md-12">
											<input type="date" class="form-control" name="start_date" value="<?php echo (isset($_GET['start_date'])) ? $_GET['start_date'] : date('Y-m-d') ?>">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group row">
										<div class="col-md-12">
											<input type="date" class="form-control" name="end_date" value="<?php echo (isset($_GET['end_date'])) ? $_GET['end_date'] : date('Y-m-d') ?>">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<button type="submit" class="btn btn-block btn-primary">Lihat</button>
									</div>
								</div>
							</div>
					</form>
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-inverse">

								<div class="panel-body">

								</div>
								<div class="ibox-content">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover">
											<tr>
												<th>Total Pesanan</th>
												<th>Total Terjual</th>
												<th>Total Keuntungan</th>
											</tr>
											<tr>
												<td><?php echo number_format($deposit['count'], 0, ',', '.') ?></td>
												<td><?php echo "Rp " . number_format($success['rows']['total'], 0, ',', '.'); ?></td>
												<td><?php echo "Rp " . number_format($opit['rows']['total'], 0, ',', '.'); ?></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>



<?php
require 'lib/footer.php';
?>