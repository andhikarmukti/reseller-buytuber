<?php

require '../config.php';
$sub_judul = ' - Kategori';
require 'lib/header.php';
$aksi = key($_GET);

if ($aksi == 'edit') {
    $id = mysqli_real_escape_string($db, $_GET['edit']);
    if ($_POST) {
        $id_jenis = mysqli_real_escape_string($db, $_POST['id_jenis']);
        $nama = mysqli_real_escape_string($db, $_POST['nama']);
        $sub_nama = mysqli_real_escape_string($db, $_POST['sub_nama']);
        $detail = mysqli_real_escape_string($db, $_POST['detail']);
        $status = mysqli_real_escape_string($db, $_POST['status']);
        $tipe_form = mysqli_real_escape_string($db, $_POST['tipe_form']);
        
        $id_jenis = htmlspecialchars($id_jenis);
        $nama = htmlspecialchars($nama);
        $sub_nama = htmlspecialchars($sub_nama);
     //   $detail = htmlspecialchars($detail);
        $status = htmlspecialchars($status);
        $tipe_form = htmlspecialchars($tipe_form);
        
        
        if ($nama) {
            $query = mysqli_query($db, "UPDATE kategori SET 
                id_jenis = '$id_jenis',
                nama = '$nama',
                sub_nama = '$sub_nama',
                detail = '$detail',
                tipe_form = '$tipe_form',
                status = '$status'
              WHERE id = '$id'");
              if($_FILES['gambar']['name'][0]){
                include 'lib/main_multi_upload.php';
                $query = mysqli_query($db, "UPDATE kategori SET gambar = '$result_gambar' WHERE id = '$id'");
              }
              if($_FILES['gambar_2']['name'][0]){
                include 'lib/main_multi_upload_bantuan.php';
                $query = mysqli_query($db, "UPDATE kategori SET bantuan = '$result_gambar_dua' WHERE id = '$id'");
              }
            $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
        } else {
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
     
        
        } 
        
    }
    $data_edit = mysqli_query($db, "SELECT * FROM kategori WHERE id = '$id'");
    $data_edit = mysqli_fetch_array($data_edit);
    $list_gambar = json_decode($data_edit['gambar'], 1);
    $list_gambar_dua = json_decode($data_edit['bantuan'], 1);
}

if ($aksi == 'tambah') {
    if ($_POST) {
        $id_jenis = mysqli_real_escape_string($db, $_POST['id_jenis']);
        $nama = mysqli_real_escape_string($db, $_POST['nama']);
        $sub_nama = mysqli_real_escape_string($db, $_POST['sub_nama']);
        $detail = mysqli_real_escape_string($db, $_POST['detail']);
        $status = mysqli_real_escape_string($db, $_POST['status']);
        $tipe_form = mysqli_real_escape_string($db, $_POST['tipe_form']);
        
        $id_jenis = htmlspecialchars($id_jenis);
        $nama = htmlspecialchars($nama);
        $sub_nama = htmlspecialchars($sub_nama);
        $detail = htmlspecialchars($detail);
        $status = htmlspecialchars($status);
        $tipe_form = htmlspecialchars($tipe_form);
        if ($nama) {
            include 'lib/main_multi_upload.php';
            $query = mysqli_query($db, "INSERT INTO kategori VALUES(null, '$id_jenis', '$nama', '$sub_nama', '$detail', '$result_gambar', '$result_gambar_dua', '$status','$tipe_form')");
            $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil '.$nama_produk.'</div></div></div>';
        } else {
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
        }
    }
}


if($aksi == 'hapus'){
  $id = mysqli_real_escape_string($db, $_GET['hapus']);
  $query = mysqli_query($db,"DELETE FROM kategori WHERE id = '$id'");
}

if ($aksi == 'on') {
    $id = mysqli_real_escape_string($db, $_GET['on']);
    $query = mysqli_query($db, "UPDATE kategori SET status = 0 WHERE id = '$id'");
  }

if ($aksi == 'off') {
    $id = mysqli_real_escape_string($db, $_GET['off']);
    $query = mysqli_query($db, "UPDATE kategori SET status = 1 WHERE id = '$id'");
  }

?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
  <h2 class="section-title">Kategori</h2>

    <div class="section-body">
      <div class="row">
        <?=$msg;?>
        <?php if($aksi == 'edit' || $aksi == 'tambah'){ ?>
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h5><?=ucwords($aksi);?></h5>
            </div>
            <div class="card-body">
            <form method="POST" enctype="multipart/form-data">
                <div class="form-group">
                  <label>Jenis</label>
                  <select class="form-control select2" name="id_jenis">
                  <option value="0">Pilih Jenis</option>
                  <?php
                    $jenis = mysqli_query($db, "SELECT * FROM jenis");
                    while ($jenis_data = mysqli_fetch_assoc($jenis)) {
                        $selected = ($data_edit['id_jenis'] == $jenis_data['id']) ? 'selected' : '';
                        echo "<option value='$jenis_data[id]' $selected>$jenis_data[nama]</option>";
                    }
                  ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama" placeholder="Masukan Nama" value="<?=htmlspecialchars($data_edit['nama']);?>">
                </div>
                <div class="form-group">
                  <label>Sub Nama</label>
                  <input type="text" class="form-control" name="sub_nama" placeholder="Masukan Sub Nama" value="<?=htmlspecialchars($data_edit['sub_nama']);?>">
                </div>
                <div class="form-group">
                  <label>Tipe Form</label>
                  <select name="tipe_form" class="form-control">
                    <option value="0" <?=($data_edit['tipe_form'] == 0) ? 'selected' : '';?>>Default</option>
                    <option value="1" <?=($data_edit['tipe_form'] == 1) ? 'selected' : '';?>>UserID</option>
                    <option value="2" <?=($data_edit['tipe_form'] == 2) ? 'selected' : '';?>>UserID + OtherID</option>
                    <option value="3" <?=($data_edit['tipe_form'] == 3) ? 'selected' : '';?>>UserName</option>
                    <option value="4" <?=($data_edit['tipe_form'] == 4) ? 'selected' : '';?>>Nomor</option>
                    <option value="5" <?=($data_edit['tipe_form'] == 5) ? 'selected' : '';?>>SMM</option>
                    <option value="6" <?=($data_edit['tipe_form'] == 6) ? 'selected' : '';?>>Genshin</option>
                    <option value="7" <?=($data_edit['tipe_form'] == 7) ? 'selected' : '';?>>Url Posting</option>
                    <option value="8" <?=($data_edit['tipe_form'] == 8) ? 'selected' : '';?>>Chimeraland</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Detail</label>
                  <textarea class="form-control my-textarea" rows="10" name="detail" placeholder="Masukan Detail"><?=htmlspecialchars($data_edit['detail']);?></textarea>
                </div>
                <div class="form-group">
                  <label>Gambar</label>
                  
                  <?php if($list_gambar[0]){ ?>
                    <div>
                      <small>Note : Jangan upload bila tidak ingin mengganti gambar</small>
                    <div class="gallery gallery-md mt-2">
                      <?php
                        foreach ($list_gambar as $ini_gambar) {
                            echo '<div class="gallery-item" data-image="../assets/img/kategori/'.$ini_gambar.'"></div>';
                        }
                      ?>
                    </div>
                    </div>
                  <?php } ?>                  
                  <div class="input_fields_wrap">
                    <div class="input-group">
                      <input type="file" class="form-control" id="customFile" name="gambar[]">
                      <div class="input-group-append">
                        <button type="button" class="btn btn-primary add_field_button"><i
                            class="fas fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Bantuan</label>
                  
                  <?php if($list_gambar_dua[0]){ ?>
                    <div>
                      <small>Note : Jangan upload bila tidak ingin mengganti gambar</small>
                    <div class="gallery gallery-md mt-2">
                      <?php
                        foreach ($list_gambar_dua as $ini_gambar_dua) {
                            echo '<div class="gallery-item" data-image="../assets/img/idserver/'.$ini_gambar_dua.'"></div>';
                        }
                      ?>
                    </div>
                    </div>
                  <?php } ?>
                  <div class="input_fields_wrap">
                    <div class="input-group">
                      <input type="file" class="form-control" id="customFile" name="gambar_2[]">
                      <div class="input-group-append">
                        <button type="button" class="btn btn-primary add_field_button"><i
                            class="fas fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Status</label>
                  <div class="selectgroup selectgroup-pills">
                    <label class="selectgroup-item">
                      <input type="radio" name="status" value="1" class="selectgroup-input" <?=$checked = ($data_edit['status'] == 1) ? 'checked' : '';?>>
                      <span class="selectgroup-button selectgroup-button-icon"><b>ON</b></span>
                    </label>
                    <label class="selectgroup-item">
                      <input type="radio" name="status" value="0" class="selectgroup-input" <?=$checked = ($data_edit['status'] == 0) ? 'checked' : '';?>>
                      <span class="selectgroup-button selectgroup-button-icon"><b>OFF</b></span>
                    </label>
                  </div>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-danger" href="?">Tutup</a>
              </form>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a href="?tambah" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</a>
            </div>
            <div class="card-body">
              <div class="table-responsive table-body" id="messages">
                <table class="table table-striped" id="table_ajax" style="width: 99%;">
                  <thead class="thead-light">
                    <tr>
                      <th>ID</th>
                      <th>Jenis</th>
                      <th>Nama</th>
                      <th>Sub Nama</th>
                      <th>Detail</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>
<script>
  $(document).ready(function () {
    $('#table_ajax').DataTable({
      "processing": false,
      "serverSide": true,
      "stateSave": true,
      "bInfo": false,
      "ajax": "ajax/list_kategori.php",
      "language": {
        "paginate": {
          "previous": "<i class='fas fa-angle-left'></i>",
          "next": "<i class='fas fa-angle-right'></i>",
        }
      }
    });
  });
  var html_upload = '<div class="input-group"><input type="file" class="form-control" id="customFile" name="gambar[]"><button class="btn btn-danger remove_field" type="button"><i class="fas fa-minus"></i></button></div>';
  $(document).ready(function () {
    var max_fields = 5; //maximum input boxes allowed
    var wrapper = $(".input_fields_wrap"); //Fields wrapper
    var add_button = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function (e) { //on add input button click
      e.preventDefault();
      if (x < max_fields) { //max input box allowed
        x++; //text box increment
        $(wrapper).append(html_upload); //add input box
      }
    });

    $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
      e.preventDefault();
      $(this).parent('div').remove();
      x--;
    })
  });
    function confirmation(ev) {
        ev.preventDefault();
        var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
        console.log(urlToRedirect); // verify if this is the right URL
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Your Data has been deleted!", {
               icon: "success",
               
              })
              window.location.href = urlToRedirect;
            } else {
              swal("Your Data is safe!");
            }
        });
    }
</script>