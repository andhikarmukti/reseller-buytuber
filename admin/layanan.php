<?php

require '../config.php';
$sub_judul = ' - Layanan';
require 'lib/header.php';
$aksi = key($_GET);

if($_POST){
 	$id_kategori = mysqli_real_escape_string($db, $_POST['id_kategori']);
    $id_provider = mysqli_real_escape_string($db, $_POST['id_provider']);
    $id_oid = mysqli_real_escape_string($db, $_POST['id_oid']);
    $nama = mysqli_real_escape_string($db, $_POST['nama']);
    $harga = mysqli_real_escape_string($db, $_POST['harga']);

    $harga_silver = mysqli_real_escape_string($db, $_POST['harga_silver']);
    $harga_gold = mysqli_real_escape_string($db, $_POST['harga_gold']);
    $harga_pro = mysqli_real_escape_string($db, $_POST['harga_pro']);

    $harga_asli = mysqli_real_escape_string($db, $_POST['harga_asli']);
    $status = mysqli_real_escape_string($db, $_POST['status']);
    $type_profit = mysqli_real_escape_string($db, $_POST['type_profit']);
    $profit = mysqli_real_escape_string($db, $_POST['profit']);

    $id_kategori = htmlspecialchars($id_kategori);
    $id_provider = htmlspecialchars($id_provider);
    $id_oid = htmlspecialchars($id_oid);
    $nama = htmlspecialchars($nama);
    $harga = htmlspecialchars($harga);
    $harga_asli = htmlspecialchars($harga_asli);
    $status = htmlspecialchars($status);
    $harga_reseller = htmlspecialchars($harga_reseller);
    $profit = htmlspecialchars($profit);

    $profit_silver = mysqli_real_escape_string($db, $_POST['profit_silver']);
    $profit_gold = mysqli_real_escape_string($db, $_POST['profit_gold']);
    $profit_pro = mysqli_real_escape_string($db, $_POST['profit_pro']);

    $profit = ($profit > 0) ? $profit : 0;
    $profit_silver = ($profit_silver > 0) ? $profit_silver : 0;
    $profit_gold = ($profit_gold > 0) ? $profit_gold : 0;
    $profit_pro = ($profit_pro > 0) ? $profit_pro : 0;

    $harga = ($harga > 0) ? $harga : 999999;
    $harga_silver = ($harga_silver > 0) ? $harga_silver : 999999;
    $harga_gold = ($harga_gold > 0) ? $harga_gold : 999999;
    $harga_pro = ($harga_pro > 0) ? $harga_pro : 999999; 
    $harga_asli = ($harga_asli > 0) ? $harga_asli : 999999; 
}

if ($aksi == 'edit') {
    $id = mysqli_real_escape_string($db, $_GET['edit']);
    if ($_POST) {
        $date = date('Y-m-d H:i:s');
        if ($nama) {
            
            if($_FILES['aplikasi']['name'][0]){
                include 'lib/main_multi_upload_aplikasi.php';
                $query = mysqli_query($db, "UPDATE layanan SET icon_layanan = '$result_gambar_aplikasi' WHERE id = '$id'");
              }
              
              
            $query = mysqli_query($db, "UPDATE layanan SET 
                id_kategori = '$id_kategori',
                id_provider = '$id_provider',
                id_oid = '$id_oid',
                nama = '$nama',
                harga = '$harga',
                harga_silver = '$harga_silver',
                harga_gold = '$harga_gold',
                harga_pro = '$harga_pro',
                harga_asli = '$harga_asli',
                type_profit = '$type_profit',
                profit_silver = '$profit_silver',
                profit_gold = '$profit_gold',
                profit_pro = '$profit_pro',
                profit = '$profit',
                status = '$status',
                tanggal = '$date'
              WHERE id = '$id'");
            $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
        } else {
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
     
        
        } 
        
    }
    $data_edit = mysqli_query($db, "SELECT * FROM layanan WHERE id = '$id'");
    $data_edit = mysqli_fetch_array($data_edit);
    $list_gambar_tiga = json_decode($data_edit['icon_layanan'], 1);
}

if ($aksi == 'tambah') {
    
    if ($_POST) {
                //cek Kategori
$is_kategori = mysqli_query($db, "SELECT * FROM kategori WHERE id=".$id_kategori."");
$is_kategori = mysqli_fetch_array($is_kategori);
        $date = date('Y-m-d H:i:s');
        if ($nama) {
            include 'lib/main_multi_upload_aplikasi.php';
            $query = mysqli_query($db, "INSERT INTO layanan VALUES(null, '$id_kategori', '$id_provider', '$id_oid', '$nama', '$harga', '$harga_silver', '$harga_gold', '$harga_pro', '$harga_asli', '$type_profit', '$profit', '$profit_silver', '$profit_gold', '$profit_pro', '$status', '$date', '$result_gambar_aplikasi')");
            $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span></span></button>Berhasil '.$nama_produk.'</div></div></div>';
          
                      $insert_token = mysqli_query($db, "INSERT INTO notifikasi_aplikasi VALUES (null, '-', 'layanan', 'all', 'Penambahan Layanan $nama $is_kategori[nama] di Kustore','0','0','$date')");
          
          print_r(mysqli_error($db));
        } else {
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span></span></button>Gagal data tidak lengkap.</div></div></div>';
        }
    }
}


if($aksi == 'hapus'){
  $id = mysqli_real_escape_string($db, $_GET['hapus']);
  $query = mysqli_query($db,"DELETE FROM layanan WHERE id = '$id'");
}

if ($aksi == 'on') {
    $id = mysqli_real_escape_string($db, $_GET['on']);
    $query = mysqli_query($db, "UPDATE layanan SET status = 0 WHERE id = '$id'");
  }

if ($aksi == 'off') {
    $id = mysqli_real_escape_string($db, $_GET['off']);
    $query = mysqli_query($db, "UPDATE layanan SET status = 1 WHERE id = '$id'");
  }
   if ($aksi == 'hapusgambar') {
    $id = mysqli_real_escape_string($db, $_GET['hapusgambar']);
    $query = mysqli_query($db, "UPDATE layanan SET icon_layanan = '' WHERE id = '$id'");
  }
    
?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
  <h2 class="section-title">Layanan</h2>

    <div class="section-body">
      <div class="row">
        <?=$msg;?>
        <?php if($aksi == 'edit' || $aksi == 'tambah'){ ?>
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h5><?=ucwords($aksi);?></h5>
            </div>
            <div class="card-body">
            <form method="POST" enctype="multipart/form-data">
                <div class="form-group">
                  <label>Kategori</label>
                  <select class="form-control select2" name="id_kategori">
                  <option value="0">Pilih Kategori</option>
                  <?php
                    $kategori = mysqli_query($db, "SELECT * FROM kategori");
                    while ($kategori_data = mysqli_fetch_assoc($kategori)) {
                        $selected = ($data_edit['id_kategori'] == $kategori_data['id']) ? 'selected' : '';
                        echo "<option value='$kategori_data[id]' $selected>$kategori_data[nama]</option>";
                    }
                  ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Provider</label>
                  <select class="form-control select2" name="id_provider">
                  <option value="0">Pilih Provider</option>
                  <?php
                    $provider = mysqli_query($db, "SELECT * FROM provider");
                    while ($provider_data = mysqli_fetch_assoc($provider)) {
                        $selected = ($data_edit['id_provider'] == $provider_data['id']) ? 'selected' : '';
                        echo "<option value='$provider_data[id]' $selected>$provider_data[nama]</option>";
                    }
                  ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Provider Order ID</label>
                  <input type="text" class="form-control" name="id_oid" placeholder="Masukan provider order id" value="<?=htmlspecialchars($data_edit['id_oid']);?>">
                </div>
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama" placeholder="Masukan Nama" value="<?=htmlspecialchars($data_edit['nama']);?>">
                </div>
                <div class="form-group">
                  <label>Auto Update Harga</label>
                  <select name="type_profit" class="form-control" id="type_profit">
                       <option value="none">Tidak, Tulis harga manual</option>
                       <option value="percent" <?=($data_edit['type_profit'] == 'percent') ? 'selected' : '';?>>Iya, auto dengan Profit Percent (%)</option> 
                       <option value="flat" <?=($data_edit['type_profit'] == 'flat') ? 'selected' : '';?>>Iya, auto dengan Profit Flat</option> 
                    </select>
                  <small>* Hanya provider tertentu yang mendukung, manual dan smile.one tidak bisa auto update harga</small>
                </div>
                <div class="row" id="harga" <?=($data_edit['type_profit'] == 'none' || $data_edit['type_profit'] == '') ? '' : 'style="display:none"';?>>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label>Harga</label>
                          <input type="number" class="form-control" name="harga" placeholder="Masukan Harga" value="<?=htmlspecialchars($data_edit['harga']);?>">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label>Harga Silver</label>
                          <input type="number" class="form-control" name="harga_silver" placeholder="Masukan Harga Silver" value="<?=htmlspecialchars($data_edit['harga_silver']);?>">
                        </div>
                    </div>
                  <div class="col-md-3">
                        <div class="form-group">
                          <label>Harga Gold</label>
                          <input type="number" class="form-control" name="harga_gold" placeholder="Masukan Harga Gold" value="<?=htmlspecialchars($data_edit['harga_gold']);?>">
                        </div>
                    </div>
                  <div class="col-md-3">
                        <div class="form-group">
                          <label>Harga Pro</label>
                          <input type="number" class="form-control" name="harga_pro" placeholder="Masukan Harga Pro" value="<?=htmlspecialchars($data_edit['harga_pro']);?>">
                        </div>
                    </div>
                  <div class="col-12">
                  <label>Harga Asli</label>
                  <input type="text" class="form-control" name="harga_asli" placeholder="Masukan Harga Asli" value="<?=htmlspecialchars($data_edit['harga_asli']);?>">
                </div>
                </div>
                
                <div class="row" id="profit" <?=($data_edit['type_profit'] == 'flat' || $data_edit['type_profit'] == 'percent') ? '' : 'style="display:none"';?>>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label>Profit</label>
                          <input type="text" class="form-control" name="profit" placeholder="Masukan profit persen" value="<?=htmlspecialchars($data_edit['profit']);?>">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label>Profit Silver</label>
                          <input type="text" class="form-control" name="profit_silver" placeholder="Masukan profit persen silver" value="<?=htmlspecialchars($data_edit['profit_silver']);?>">
                        </div>
                    </div>
                  <div class="col-md-3">
                        <div class="form-group">
                          <label>Profit Gold</label>
                          <input type="text" class="form-control" name="profit_gold" placeholder="Masukan profit persen gold" value="<?=htmlspecialchars($data_edit['profit_gold']);?>">
                        </div>
                    </div>
                  <div class="col-md-3">
                        <div class="form-group">
                          <label>Profit Pro</label>
                          <input type="text" class="form-control" name="profit_pro" placeholder="Masukan profit persen pro" value="<?=htmlspecialchars($data_edit['profit_pro']);?>">
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                  <label>Status</label>
                  <div class="selectgroup selectgroup-pills">
                    <label class="selectgroup-item">
                      <input type="radio" name="status" value="1" class="selectgroup-input" <?=$checked = ($data_edit['status'] == 1) ? 'checked' : '';?>>
                      <span class="selectgroup-button selectgroup-button-icon"><b>ON</b></span>
                    </label>
                    <label class="selectgroup-item">
                      <input type="radio" name="status" value="0" class="selectgroup-input" <?=$checked = ($data_edit['status'] == 0) ? 'checked' : '';?>>
                      <span class="selectgroup-button selectgroup-button-icon"><b>OFF</b></span>
                    </label>
                  </div>
                </div>
                                                <div class="form-group">
                  <label>Icon Layanan (100px x 100px)</label>
                  
                  <?php if($list_gambar_tiga[0]){ ?>
                    <div>
                      <small>Note : Jangan upload bila tidak ingin mengganti gambar</small>
                    <div class="gallery gallery-md mt-2">
                      <?php
                        foreach ($list_gambar_tiga as $ini_gambar_tiga) {
                            echo '<div class="gallery-item" data-image="../assets/img/kategori/'.$ini_gambar_tiga.'"></div>';
                        }
                      ?>
                    </div>
                    </div>
                  <?php } ?>
                  <div class="input_fields_wrap">
                    <div class="input-group">
                      <input type="file" class="form-control" id="customFile" name="aplikasi[]">
                      <div class="input-group-append">
                        <button type="button" class="btn btn-primary add_field_button"><i
                            class="fas fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
                    <br>
                    <a class="btn btn-warning" href="?hapusgambar=<?=$id?>">Hapus Gambar</a>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-danger" href="?">Tutup</a>
              </form>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="col-12 mb-2">
          <select name="kategori" id="kategori" class="form-control" onChange="setCategory(this)">
            <option>Semua Kategori</option>
            <?php
              $db_kategori = mysqli_query($db, "SELECT * FROM kategori WHERE status='1'");
               while ($kategori = mysqli_fetch_array($db_kategori)) {
                 $selected = ($kategori['nama'] == $_SESSION['set_category']) ? 'selected' : '';
                 echo "<option $selected>$kategori[nama]</option>";
               }
            ?>
          </select>
        </div>
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a href="?tambah" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</a>
            </div>
            <div class="card-body">
              <div class="table-responsive table-body" id="messages">
                <table class="table table-striped" id="table_ajax" style="width: 99%;">
                  <thead class="thead-light">
                    <tr>
                      <th>ID</th>
                      <th>Kategori</th>
                      <th>Provider</th>
                      <th>Nama</th>
                      <th>Harga</th>
                      <th>Harga_Silver</th>
                      <th>Harga_Gold</th>
                      <th>Harga_Pro</th>
                      <th>Harga Asli</th>
                      <th>Status</th>
                      <th>Tanggal</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>
<script>
  $(document).ready(function () {
    $('#table_ajax').DataTable({
      "processing": false,
      "serverSide": true,
      "stateSave": true,
      "bInfo": false,
      "ajax": "ajax/list_layanan.php",
      "language": {
        "paginate": {
          "previous": "<i class='fas fa-angle-left'></i>",
          "next": "<i class='fas fa-angle-right'></i>",
        }
      }
    });
    $('#type_profit').on('change', function() {
      var value = $(this).val();
      if(value == 'none'){
       $("#profit").slideUp();
       $("#harga").slideDown(); 
      }else{
       $("#harga").slideUp();
       $("#profit").slideDown();  
      }
    });
  });

    function confirmation(ev) {
        ev.preventDefault();
        var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
        console.log(urlToRedirect); // verify if this is the right URL
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Your Data has been deleted!", {
               icon: "success",
               
              })
              window.location.href = urlToRedirect;
            } else {
              swal("Your Data is safe!");
            }
        });
    }
      var html_upload = '<div class="input-group"><input type="file" class="form-control" id="customFile" name="gambar[]"><button class="btn btn-danger remove_field" type="button"><i class="fas fa-minus"></i></button></div>';
  $(document).ready(function () {
    var max_fields = 5; //maximum input boxes allowed
    var wrapper = $(".input_fields_wrap"); //Fields wrapper
    var add_button = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function (e) { //on add input button click
      e.preventDefault();
      if (x < max_fields) { //max input box allowed
        x++; //text box increment
        $(wrapper).append(html_upload); //add input box
      }
    });

    $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
      e.preventDefault();
      $(this).parent('div').remove();
      x--;
    })
  });
    function setCategory(cat) {
        $.ajax({
          url: "ajax/set_layanan.php",
          type: "post",
          data: 'kategori='+cat.value,
          dataType: 'json',
          success: function (response) {
            if (response.status) {
              $('#table_ajax').DataTable().ajax.reload();
            }else{
              $('#table_ajax').DataTable().ajax.reload();
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
             console.log(textStatus, errorThrown);
          }
      });
    }
</script>