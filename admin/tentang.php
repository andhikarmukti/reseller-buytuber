<?php
require '../config.php';
$sub_judul = ' - Tentang';
require 'lib/header.php';
$id = 1;
$aksi = key($_GET);
$tentang = mysqli_query($db, "SELECT * FROM tentang");
$data_tentang = mysqli_fetch_array($tentang);
if ($_POST) {
    $tentang = mysqli_real_escape_string($db, $_POST['tentang']);
    $tentang = htmlspecialchars($tentang);
    if (isset($_POST['about'])) {
        if ($tentang) {
            $query = mysqli_query($db, "UPDATE tentang SET isi = '$tentang' WHERE id = '$id'");          
            $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
        } else {
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
            //$msg = "Error: " . $query . "<br>" . mysqli_error($db);
        }
    }

}


?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
  <h2 class="section-title">Tentang</h2>

    <div class="section-body">
      <div class="row">
        <?=$msg;?>
        <div class="col-md-12 col-12">
          <div class="card">
            <div class="card-body">
              <form method="POST">

                <div class="form-group">
                  <label>Tentang</label>
                  <textarea class="form-control" rows="10" name="tentang" placeholder="tentang" style="height: 100px;"><?=htmlspecialchars($data_tentang['isi']);?></textarea>
                </div>
                <button type="submit" class="btn btn-primary" name="about">Submit</button>
              </form>
            </div>
          </div>
        </div>
      
  
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>