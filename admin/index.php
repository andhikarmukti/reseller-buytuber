<?php
require '../config.php';
$months = array(
  'All Month',
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July ',
  'August',
  'September',
  'October',
  'November',
  'December',
);
$month = ($months[$_GET['m']]) ? $_GET['m'] : date('n');
$year = date('Y');

if($month){
  $sql_date = "AND MONTH(tanggal) BETWEEN '".$month."' AND '".$month."' AND YEAR(tanggal) BETWEEN '".$year."' AND '".$year."'";
}else{
  $sql_date = "";
}

$sub_judul = ' - Dashboard';
require 'lib/header.php';

// Pengguna
$user = mysqli_query($db, "SELECT * FROM users WHERE level='member'");
$users = mysqli_num_rows($user);
// orders
$order = mysqli_query($db, "SELECT * FROM pesanan WHERE tanggal IS NOT NULL $sql_date");
$orders = mysqli_num_rows($order);

// total Payment
  $payment = mysqli_query($db,"SELECT SUM(keuntungan) AS total FROM pesanan WHERE status IN ('pending','success') $sql_date");
  $payments = mysqli_fetch_array($payment);
  $count_payment = $payments['total'];

  // pending
  $pending = mysqli_query($db,"SELECT * FROM pesanan WHERE status = 'pending' $sql_date");
  $pending = mysqli_num_rows($pending);
  // batal
  $batal = mysqli_query($db,"SELECT * FROM pesanan WHERE status = 'cancel' $sql_date");
  $batal = mysqli_num_rows($batal);
    // sukses
  $sukses = mysqli_query($db,"SELECT * FROM pesanan WHERE status = 'success' $sql_date");
  $sukses = mysqli_num_rows($sukses);
?>

        <!-- Main Content -->
        <div class="main-content">
          <section class="section">
            <div class="section-body">
              <h2 class="section-title">Dashboard</h2>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
            <div class="card-stats-title p-0">Filter - <div class="dropdown d-inline">
                      <a class="font-weight-600 dropdown-toggle" data-toggle="dropdown" href="#" id="orders-month"><?=$months[$month];?></a>
                      <ul class="dropdown-menu dropdown-menu-sm">
                        <li class="dropdown-title">Select Month</li>
                        <?php foreach($months as $mid => $m){
                          $active = ($month == $mid) ? 'active' : '';
                          echo '<li><a href="?m='.$mid.'" class="dropdown-item '.$active.'">'.$m.'</a></li>';
                        }
                        ?>
                      </ul>
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-4 col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="x card-statistic-2">
                <div class="card-stats">
                  
                  <div class="card-stats-items">
                    <div class="card-stats-item">
                      <div class="card-stats-item-count"><?=$pending?></div>
                      <div class="card-stats-item-label">Pending</div>
                    </div>
                    <div class="card-stats-item">
                      <div class="card-stats-item-count"><?=$batal?></div>
                      <div class="card-stats-item-label">Batal</div>
                    </div>
                    <div class="card-stats-item">
                      <div class="card-stats-item-count"><?=$sukses?></div>
                      <div class="card-stats-item-label">Sukses</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12">
          <div class="card card-statistic-2">
            <div class="card-icon shadow-primary bg-primary">
              <i class="fas fa-users"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Pengguna</h4>
              </div>
              <div class="card-body"> <?=$users?> </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12">
          <div class="card card-statistic-2">
            <div class="card-icon shadow-primary bg-primary">
              <i class="fas fa-archive"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Pesanan</h4>
              </div>
              <div class="card-body"> <?=$orders?> </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12">
          <div class="card card-statistic-2">
            <div class="card-icon shadow-primary bg-primary">
              <i class="fas fa-dollar-sign"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Keuntungan</h4>
              </div>
              <div class="card-body">Rp <?php echo number_format($count_payment, 0, ',', ','); ?></div>
            </div>
          </div>
        </div>
      </div>

            </div>
          </section>
<?php 
require 'lib/footer.php';
?>