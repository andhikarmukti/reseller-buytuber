<?php
require '../config.php';
$sub_judul = ' - Payment Method';
require 'lib/header.php';
$aksi = key($_GET);

if ($aksi == 'edit') {
  $id = mysqli_real_escape_string($db, $_GET['edit']);
  if ($_POST) {
      $payment = mysqli_real_escape_string($db, $_POST['payment']);
      $status = mysqli_real_escape_string($db, $_POST['status']);
      $config = mysqli_real_escape_string($db, $_POST['config']);
      $tipe = mysqli_real_escape_string($db, $_POST['tipe']);
      $detail = mysqli_real_escape_string($db, $_POST['detail']);
      $rate = mysqli_real_escape_string($db, $_POST['rate']);
      $rate_persen = mysqli_real_escape_string($db, $_POST['rate_persen']);
      $fee = mysqli_real_escape_string($db, $_POST['fee']);
      $min_jumlah = mysqli_real_escape_string($db, $_POST['min_jumlah']);
      $pake_rek = mysqli_real_escape_string($db, $_POST['pake_rek']);
      $keterangan_rek = mysqli_real_escape_string($db, $_POST['keterangan_rek']);
      
      
      $payment = htmlspecialchars($payment);
      $status = htmlspecialchars($status);
      $tipe = htmlspecialchars($tipe);
    //  $detail = htmlspecialchars($detail);
      $rate = htmlspecialchars($rate);
      $rate_persen = htmlspecialchars($rate_persen);
      $fee = htmlspecialchars($fee);
      $min_jumlah = htmlspecialchars($min_jumlah);
      $pake_rek = htmlspecialchars($pake_rek);
      $keterangan_rek = htmlspecialchars($keterangan_rek);
      if ($payment) {
          $query = mysqli_query($db, "UPDATE metode_pembayaran SET 
              nama = '$payment',
              tipe = '$tipe',
              detail = '$detail',
              status = '$status',
              config = '$config',
              rate = '$rate',
              rate_persen = '$rate_persen',
              fee = '$fee',
              min_jumlah = '$min_jumlah',
              pake_rek = '$pake_rek',
              keterangan_rek = '$keterangan_rek'
            WHERE id = '$id'");
            //print_r(mysqli_error($db));
            if($_FILES['gambar']['name'][0]){
              include 'lib/main_multi_upload_public.php';
              $query = mysqli_query($db, "UPDATE metode_pembayaran SET gambar = '$result_gambar' WHERE id = '$id'");
            }
          $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
      } else {
          $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
      }
  }
  $data_edit = mysqli_query($db, "SELECT * FROM metode_pembayaran WHERE id = '$id'");
  $data_edit = mysqli_fetch_array($data_edit);
  $list_gambar = json_decode($data_edit['gambar'], 1);
}

if ($aksi == 'tambah') {
  if ($_POST) {
      $payment = mysqli_real_escape_string($db, $_POST['payment']);
      $status = mysqli_real_escape_string($db, $_POST['status']);
      $config = mysqli_real_escape_string($db, $_POST['config']);
      $tipe = mysqli_real_escape_string($db, $_POST['tipe']);
      $detail = mysqli_real_escape_string($db, $_POST['detail']);
      $rate = mysqli_real_escape_string($db, $_POST['rate']);
      $rate_persen = mysqli_real_escape_string($db, $_POST['rate_persen']);
      $fee = mysqli_real_escape_string($db, $_POST['fee']);
      $min_jumlah = mysqli_real_escape_string($db, $_POST['min_jumlah']);
      $pake_rek = mysqli_real_escape_string($db, $_POST['pake_rek']);
      $keterangan_rek = mysqli_real_escape_string($db, $_POST['keterangan_rek']);
      $rate = ($rate) ? $rate : 1;
      $rate_persen = ($rate_persen) ? $rate_persen : 0;
      
      $payment = htmlspecialchars($payment);
      $status = htmlspecialchars($status);
      $tipe = htmlspecialchars($tipe);
   //   $detail = htmlspecialchars($detail);
      $rate = htmlspecialchars($rate);
      $rate_persen = htmlspecialchars($rate_persen);
      $fee = htmlspecialchars($fee);
      $min_jumlah = htmlspecialchars($min_jumlah);
      $pake_rek = htmlspecialchars($pake_rek);
      $keterangan_rek = htmlspecialchars($keterangan_rek);
      if ($payment) {
          include 'lib/main_multi_upload_public.php';
          $query = mysqli_query($db, "INSERT INTO metode_pembayaran VALUES(null, '$payment', '$tipe', '$detail', '$result_gambar', '$config', '$rate', '$rate_persen', '$fee', '$min_jumlah', '$pake_rek', '$keterangan_rek', '$status')");
          $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil '.$nama_produk.'</div></div></div>';
      } else {
          $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
      }
  }
}

if ($aksi == 'hapus') {
  $id = mysqli_real_escape_string($db, $_GET['hapus']);
  $query = mysqli_query($db, "DELETE FROM metode_pembayaran WHERE id = '$id'");
}

if ($aksi == 'on') {
$id = mysqli_real_escape_string($db, $_GET['on']);
$query = mysqli_query($db, "UPDATE metode_pembayaran SET status = 0 WHERE id = '$id'");
}

if ($aksi == 'off') {
$id = mysqli_real_escape_string($db, $_GET['off']);
$query = mysqli_query($db, "UPDATE metode_pembayaran SET status = 1 WHERE id = '$id'");
}

?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
  <h2 class="section-title">Kategori</h2>

    <div class="section-body">
      <div class="row">
        <?=$msg;?>
        <?php if($aksi == 'edit' || $aksi == 'tambah'){ ?>
        <div class="col-12">
          <div class="card">
 
            <div class="card-header">
              <h5><?=ucwords($aksi);?></h5>
            </div>
            <div class="card-body">
              <form method="POST" enctype="multipart/form-data">
                <div class="form-group">
                  <label>Payment Method</label>
                  <input type="text" class="form-control" name="payment" placeholder="payment" value="<?=htmlspecialchars($data_edit['nama']);?>">
                </div>
                <div class="form-group">
                  <label>Gambar</label>
                  
                    <?php if($list_gambar[0]){ ?>
                    <div>
                      <small>Note : Jangan upload bila tidak ingin mengganti gambar</small>
                    <div class="gallery gallery-md mt-2">
                      <?php
                        foreach ($list_gambar as $ini_gambar) {
                            echo '<div class="gallery-item" data-image="../assets/img/'.$ini_gambar.'"></div>';
                        }
                      ?>
                    </div>
                    </div>
                  <?php } ?>
                  
                  <div class="input_fields_wrap">
                    <div class="input-group">
                      <input type="file" class="form-control" id="customFile" name="gambar[]">
                      <div class="input-group-append">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                      <label>Tipe</label>
                      <select class="form-control select2" name="tipe" value="<?=htmlspecialchars($data_edit['tipe']);?>">
                        <option value="auto">Auto</option>
                        <option value="manual">Manual</option>
                      </select>
                    </div>
                <div class="form-group">
                  <label>Detail</label>
                  <input type="text" class="form-control" name="detail" placeholder="detail" value="<?=htmlspecialchars($data_edit['detail']);?>">
                </div>
                <div class="form-group">
                  <label>Status</label>
                  <div class="selectgroup selectgroup-pills">
                    <label class="selectgroup-item">
                      <input type="radio" name="status" value="1" class="selectgroup-input" <?=$checked = ($data_edit['status'] == 1) ? 'checked' : '';?>>
                      <span class="selectgroup-button selectgroup-button-icon"><b>ON</b></span>
                    </label>
                    <label class="selectgroup-item">
                      <input type="radio" name="status" value="0" class="selectgroup-input" <?=$checked = ($data_edit['status'] == 0) ? 'checked' : '';?>>
                      <span class="selectgroup-button selectgroup-button-icon"><b>OFF</b></span>
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label>Config</label>
                  <textarea class="form-control" name="config" placeholder="config" rows="10"> <?=htmlspecialchars($data_edit['config']);?></textarea>
                </div>
                <div class="form-group">
                  <label>Rate</label>
                  <input type="text" class="form-control" name="rate" placeholder="rate" value="<?=htmlspecialchars($data_edit['rate']);?>">
                </div>
                <div class="form-group">
                  <label>Rate Persen</label>
                  <input type="text" class="form-control" name="rate_persen" placeholder="rate persen" value="<?=htmlspecialchars($data_edit['rate_persen']);?>">
                </div>
                <div class="form-group">
                  <label>Fee</label>
                  <input type="number" class="form-control" name="fee" placeholder="fee pembayaran" value="<?=htmlspecialchars($data_edit['fee']);?>">
                </div>
                <div class="form-group">
                  <label>Minimal Jumlah</label>
                  <input type="number" class="form-control" name="min_jumlah" placeholder="minimal jumlah pembayaran" value="<?=htmlspecialchars($data_edit['min_jumlah']);?>">
                </div>
                <div class="form-group">
                  <label>Client Rek</label>
                  <div class="selectgroup selectgroup-pills">
                    <label class="selectgroup-item">
                      <input type="radio" name="pake_rek" value="1" class="selectgroup-input" <?=($data_edit['pake_rek'] == 1) ? 'checked' : '';?>>
                      <span class="selectgroup-button selectgroup-button-icon"><b>ON</b></span>
                    </label>
                    <label class="selectgroup-item">
                      <input type="radio" name="pake_rek" value="0" class="selectgroup-input" <?=($data_edit['pake_rek'] == 0) ? 'checked' : '';?>>
                      <span class="selectgroup-button selectgroup-button-icon"><b>OFF</b></span>
                    </label>
                  </div>
                  <label>Keterangan Client Rek</label>
                  <input type="keterangan_rek" class="form-control" name="keterangan_rek" placeholder="masukan keterangan rek" value="<?=htmlspecialchars($data_edit['keterangan_rek']);?>">
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-danger" href="?">Tutup</a>
              </form>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="col-12">
          <div class="card">
          <div class="card-header">
              <a href="?tambah" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</a>
            </div>
            <div class="card-body">
              <div class="table-responsive table-body" id="messages">
                <table class="table table-striped" id="table_ajax" style="width: 100%">
                  <thead class="thead-light">
                    <tr>
                      <th>ID</th>
                      <th>Nama</th>
                      <th>Tipe</th>
                      <th>Detail</th>
                      <th>Rate</th>
                      <th>Rate Persen</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>
<script>
  $(document).ready(function () {
    $('#table_ajax').DataTable({
      "processing": false,
      "serverSide": true,
      "bInfo": false,
      "ajax": "ajax/list_metode_pembayaran",
      "language": {
        "paginate": {
          "previous": "<i class='fas fa-angle-left'></i>",
          "next": "<i class='fas fa-angle-right'></i>",
        }
      }
    });
  });
    function confirmation(ev) {
        ev.preventDefault();
        var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
        console.log(urlToRedirect); // verify if this is the right URL
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Your Data has been deleted!", {
               icon: "success",
               
              })
              window.location.href = urlToRedirect;
            } else {
              swal("Your Data is safe!");
            }
        });
    }
</script>