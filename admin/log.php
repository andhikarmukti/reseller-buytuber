<?php
require '../config.php';
$sub_judul = ' - Log Aktivitas';
require 'lib/header.php';
$aksi = key($_GET);

?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
  <h2 class="section-title">Log Aktivitas</h2>

    <div class="section-body">
      <div class="row">
        <?=$msg;?>
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive table-body" id="messages">
                <table class="table table-striped" id="table_ajax" style="width: 100%">
                  <thead class="thead-light">
                    <tr>
                      <th>Tanggal</th>
                      <th>Email</th>
                      <th>IP</th>
                      <th>Keterangan</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>
<script>
  $(document).ready(function () {
    $('#table_ajax').DataTable({
      "order": [[0, "desc" ]],
      "processing": false,
      "serverSide": true,
      "bInfo": false,
      "ajax": "ajax/list_log",
      "language": {
        "paginate": {
          "previous": "<i class='fas fa-angle-left'></i>",
          "next": "<i class='fas fa-angle-right'></i>",
        }
      }
    });
  });
  
      function confirmation(ev) {
        ev.preventDefault();
        var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
        console.log(urlToRedirect); // verify if this is the right URL
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Your Data has been deleted!", {
               icon: "success",
               
              })
              window.location.href = urlToRedirect;
            } else {
              swal("Your Data is safe!");
            }
        });
    }
</script>