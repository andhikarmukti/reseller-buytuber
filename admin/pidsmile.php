<?php

require '../config.php';
$sub_judul = ' - Pid Smile';
require '../lib/smile.php';
require 'lib/header.php';
$aksi = key($_GET);

if($aksi == 'edit'){
  $id = mysqli_real_escape_string($db, $_GET['edit']);
  if ($_POST) {
    $pid  = mysqli_real_escape_string($db, $_POST['pid']);
    $nama = mysqli_real_escape_string($db, $_POST['nama']);
    $soc = mysqli_real_escape_string($db, $_POST['soc']);
    $status = mysqli_real_escape_string($db, $_POST['status']);
    
    $pid = htmlspecialchars($pid);
    $nama = htmlspecialchars($nama);
    $soc = htmlspecialchars($soc);
    $status = htmlspecialchars($status);
    if ($pid) {
        $query = mysqli_query($db, "UPDATE pid_smile SET pid = '$pid', nama = '$nama', soc = '$soc', status = '$status' WHERE id = '$id'");
        $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
        echo("Errorcode: " . mysqli_error($db));
    
    } else {
        $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
    }
  }
  $data_edit = mysqli_query($db,"SELECT * FROM pid_smile WHERE id = '$id'");
  $data_edit = mysqli_fetch_array($data_edit);
}

if($aksi == 'tambah'){
  if ($_POST) {
    $pid  = mysqli_real_escape_string($db, $_POST['pid']);
    $nama = mysqli_real_escape_string($db, $_POST['nama']);
    $soc = mysqli_real_escape_string($db, $_POST['soc']);
    $status = mysqli_real_escape_string($db, $_POST['status']);
    
    $pid = htmlspecialchars($pid);
    $nama = htmlspecialchars($nama);
    $soc = htmlspecialchars($soc);
    $status = htmlspecialchars($status);
      if ($pid) {
          $query = mysqli_query($db, "INSERT INTO pid_smile VALUES(null, '$pid', '$nama', '$soc', '$status')");
          $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
      } else {
          $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
      }
  }
}

if($aksi == 'hapus'){
  $id = mysqli_real_escape_string($db, $_GET['hapus']);
  $query = mysqli_query($db,"DELETE FROM pid_smile WHERE id = '$id'");
}

if ($aksi == 'on') {
    $id = mysqli_real_escape_string($db, $_GET['on']);
    $query = mysqli_query($db, "UPDATE pid_smile SET status = 0 WHERE id = '$id'");
}

if ($aksi == 'off') {
  $total_fav = mysqli_query($db, "SELECT * FROM pid_smile WHERE status = 1");
  $total_fav = mysqli_num_rows($total_fav);
  if ($total_fav >=4) {
    $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Max 4 Favorite.</div></div></div>';
  }else{
      $id = mysqli_real_escape_string($db, $_GET['off']);
      $query = mysqli_query($db, "UPDATE pid_smile SET status = 1 WHERE id = '$id'");
  }
}

$id_provider = 3;

$provider = mysqli_query($db, "SELECT * FROM provider WHERE id = '$id_provider'");
$provider = mysqli_fetch_array($provider);

$smile = new smileone;
$smile->cookies = $provider['config'];
$list_pid = $smile->get_service();
$ceksaldo = $smile->saldo();
?>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">List PID Realtime</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body table-responsive">
        <table class="table">
          <thead>
            <th>ID</th>
            <th>Nama</th>
            <th>Harga SOC</th>
          </thead>
          <tbody>
            <?php
              foreach ($list_pid['data'] as $pid_data) {
                  echo "
                  <tr>
                    <td>$pid_data[id]</td>
                    <td>$pid_data[name] $pid_data[bonus]</td>
                    <td>$pid_data[amount]</td>
                  </tr>
                ";
              }
            ?>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
  <h2 class="section-title">Pid Smile</h2>
    <div class="section-body">
      <div class="row">
        <?=$msg;?>
        <?php if($aksi == 'edit' || $aksi == 'tambah'){ ?>
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h5><?=ucwords($aksi);?></h5>
            </div>
            <div class="card-body">
              <form method="POST">
                <div class="form-group">
                  <label>Pid</label>
                  <input type="text" class="form-control" name="pid" placeholder="Masukan Pid" value="<?=htmlspecialchars($data_edit['pid']);?>"><br>
                  <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">List Pid</a>
                </div>
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama" placeholder="Masukan nama layanan" value="<?=htmlspecialchars($data_edit['nama']);?>">
                </div>
                <div class="form-group">
                  <label>Soc</label>
                  <input type="text" class="form-control" name="soc" placeholder="Rate Soc" value="<?=htmlspecialchars($data_edit['soc']);?>">
                </div>
                <div class="form-group">
                  <label>Status</label>
                  <div class="selectgroup selectgroup-pills">
                    <label class="selectgroup-item">
                      <input type="radio" name="status" value="1" class="selectgroup-input" <?=$checked = ($data_edit['status'] == 1) ? 'checked' : '';?>>
                      <span class="selectgroup-button selectgroup-button-icon"><b>ON</b></span>
                    </label>
                    <label class="selectgroup-item">
                      <input type="radio" name="status" value="0" class="selectgroup-input" <?=$checked = ($data_edit['status'] == 0) ? 'checked' : '';?>>
                      <span class="selectgroup-button selectgroup-button-icon"><b>OFF</b></span>
                    </label>
                  </div>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-danger" href="?">Tutup</a>
              </form>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="col-12">
          <div class="card">
            <div class="card-header">

              <a href="?tambah" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</a>
              <a href="#" class="btn btn-primary ml-2" data-toggle="modal" data-target="#exampleModal">List Pid</a>
            </div>
            <div class="card-body">
            <div class="alert alert-primary alert-dismissible show fade">
                      <div class="alert-body">
                        <button class="close" data-dismiss="alert">
                          <span>&times;</span>
                        </button>
                        Saldo Smile : <?=$ceksaldo['saldo']?>
                      </div>
                    </div>
              <div class="table-responsive table-body" id="messages">
                <table class="table table-striped" id="table_ajax" style="width: 99%;">
                  <thead class="thead-light">
                    <tr>
                      <th>ID</th>
                      <th>Pid</th>
                      <th>Nama</th>
                      <th>Soc</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>
<script>
  $(document).ready(function () {
    $('#table_ajax').DataTable({
      "processing": false,
      "serverSide": true,
      "bInfo": false,
      "ajax": "ajax/list_pid.php",
      "language": {
        "paginate": {
          "previous": "<i class='fas fa-angle-left'></i>",
          "next": "<i class='fas fa-angle-right'></i>",
        }
      }
    });
  });
  
    function confirmation(ev) {
        ev.preventDefault();
        var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
        console.log(urlToRedirect); // verify if this is the right URL
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Your Data has been deleted!", {
               icon: "success",
               
              })
              window.location.href = urlToRedirect;
            } else {
              swal("Your Data is safe!");
            }
        });
    }
</script>