<?php
require '../config.php';
$sub_judul = ' - Ketentuan';
require 'lib/header.php';
$id = 1;
$aksi = key($_GET);
$pengaturan = mysqli_query($db, "SELECT * FROM pengaturan");
$data_pengaturan = mysqli_fetch_array($pengaturan);
if ($_POST) {
    $ketentuan = mysqli_real_escape_string($db, $_POST['ketentuan']);
    $ketentuan = htmlspecialchars($ketentuan);
    if (isset($_POST['about'])) {
        if ($ketentuan) {
            $query = mysqli_query($db, "UPDATE pengaturan SET terms = '$ketentuan' WHERE id = '$id'");          
            $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
        } else {
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
            //$msg = "Error: " . $query . "<br>" . mysqli_error($db);
        }
    }

}


?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
  <h2 class="section-title">Ketentuan</h2>

    <div class="section-body">
      <div class="row">
        <?=$msg;?>
        <div class="col-md-12 col-12">
          <div class="card">
            <div class="card-body">
              <form method="POST">

                <div class="form-group">
                  <label>Ketentuan</label>
                  <textarea class="form-control" rows="10" name="ketentuan" placeholder="ketentuan" style="height: 100px;"><?=htmlspecialchars($data_pengaturan['terms']);?></textarea>
                </div>
                <button type="submit" class="btn btn-primary" name="about">Submit</button>
              </form>
            </div>
          </div>
        </div>
      
  
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>