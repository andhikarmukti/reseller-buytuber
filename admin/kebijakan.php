<?php
require '../config.php';
$sub_judul = ' - Kebijakan';
require 'lib/header.php';
$id = 1;
$aksi = key($_GET);
$pengaturan = mysqli_query($db, "SELECT * FROM pengaturan");
$data_pengaturan = mysqli_fetch_array($pengaturan);
if ($_POST) {
    $privasi = mysqli_real_escape_string($db, $_POST['privasi']);
    $privasi = htmlspecialchars($privasi);
    if (isset($_POST['about'])) {
        if ($privasi) {
            $query = mysqli_query($db, "UPDATE pengaturan SET privacy = '$privasi' WHERE id = '$id'");          
            $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
        } else {
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
            //$msg = "Error: " . $query . "<br>" . mysqli_error($db);
        }
    }

}


?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
  <h2 class="section-title">Kebijakan Privasi</h2>

    <div class="section-body">
      <div class="row">
        <?=$msg;?>
        <div class="col-md-12 col-12">
          <div class="card">
            <div class="card-body">
              <form method="POST">

                <div class="form-group">
                  <label>Kebijakan</label>
                  <textarea class="form-control" rows="10" name="privasi" placeholder="privasi" style="height: 100px;"><?=htmlspecialchars($data_pengaturan['privacy']);?></textarea>
                </div>
                <button type="submit" class="btn btn-primary" name="about">Submit</button>
              </form>
            </div>
          </div>
        </div>
      
  
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>