<?php
require '../config.php';
$sub_judul = ' - Faq';
require 'lib/header.php';
$aksi = key($_GET);



if($aksi == 'edit'){
  $id = mysqli_real_escape_string($db, $_GET['edit']);
  if ($_POST) {
    $pertanyaan = mysqli_real_escape_string($db, $_POST['pertanyaan']);
    $jawaban = mysqli_real_escape_string($db, $_POST['jawaban']);
    
    //$pertanyaan = htmlspecialchars($pertanyaan);
   // $jawaban = htmlspecialchars($jawaban);
    if ($pertanyaan) {
        $query = mysqli_query($db,"UPDATE faq SET pertanyaan = '$pertanyaan', jawaban = '$jawaban' WHERE id = '$id'");
        $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
    } else {
        $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
    }
  }
  $data_faq = mysqli_query($db,"SELECT * FROM faq WHERE id = '$id'");
  $data_faq = mysqli_fetch_array($data_faq);
}

if($aksi == 'tambah'){
    if ($_POST) {
        $pertanyaan = mysqli_real_escape_string($db, $_POST['pertanyaan']);
        $jawaban = mysqli_real_escape_string($db, $_POST['jawaban']);
        
       // $pertanyaan = htmlspecialchars($pertanyaan);
      //  $jawaban = htmlspecialchars($jawaban);
        if ($jawaban) {
            $query = mysqli_query($db, "INSERT INTO faq VALUES(null, '$pertanyaan', '$jawaban')");
            $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
        } else {
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
        }
    }
  }

if($aksi == 'hapus'){
  $id = mysqli_real_escape_string($db, $_GET['hapus']);
  $query = mysqli_query($db,"DELETE FROM faq WHERE id = '$id'");
}

?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
  <h2 class="section-title">Faq</h2>

    <div class="section-body">
      <div class="row">
        <?=$msg;?>
        <?php if($aksi == 'edit' || $aksi == 'tambah'){ ?>
        <div class="col-12">
          <div class="card">
 
            <div class="card-header">
              <h5><?=ucwords($aksi);?></h5>
            </div>
            <div class="card-body">
              <form method="POST">
                <div class="form-group">
                  <label>Pertanyaan</label>
                  <input type="text" class="form-control" name="pertanyaan" placeholder="pertanyaan" value="<?=htmlspecialchars($data_faq['pertanyaan']);?>">
                </div>
                <div class="form-group">
                  <label>Jawaban</label>
                  <input type="text" class="form-control" name="jawaban" placeholder="jawaban" value="<?=htmlspecialchars($data_faq['jawaban']);?>">
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-danger" href="?">Tutup</a>
              </form>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="col-12">
          <div class="card">
          <div class="card-header">
              <a href="?tambah" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</a>
            </div>
            <div class="card-body">
              <div class="table-responsive table-body" id="messages">
                <table class="table table-striped" id="table_ajax" style="width: 100%">
                  <thead class="thead-light">
                    <tr>
                      <th>ID</th>
                      <th>Pertanyaan</th>
                      <th>Jawaban</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>
<script>
  $(document).ready(function () {
    $('#table_ajax').DataTable({
      "processing": false,
      "serverSide": true,
      "bInfo": false,
      "ajax": "ajax/list_faq",
      "language": {
        "paginate": {
          "previous": "<i class='fas fa-angle-left'></i>",
          "next": "<i class='fas fa-angle-right'></i>",
        }
      }
    });
  });
  
      function confirmation(ev) {
        ev.preventDefault();
        var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
        console.log(urlToRedirect); // verify if this is the right URL
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Your Data has been deleted!", {
               icon: "success",
               
              })
              window.location.href = urlToRedirect;
            } else {
              swal("Your Data is safe!");
            }
        });
    }
</script>