<?php
require '../config.php';
$sub_judul = ' - Pesanan';
require 'lib/header.php';
$aksi = key($_GET);

if ($aksi == 'edit') {
  $id = mysqli_real_escape_string($db, $_GET['edit']);
  if ($_POST) {
      $data = mysqli_real_escape_string($db, $_POST['data']);
      $keterangan = mysqli_real_escape_string($db, $_POST['keterangan']);
      
       $data = htmlspecialchars($data);
       $keterangan = htmlspecialchars($keterangan);
      if ($data) {
          $query = mysqli_query($db, "UPDATE pesanan SET 
              data   = '$data',
              keterangan = '$keterangan'
            WHERE id = '$id'");
          $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
      } else {
          $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
      }
  }
  $data_edit = mysqli_query($db, "SELECT * FROM pesanan WHERE id = '$id'");
  $data_edit = mysqli_fetch_array($data_edit);
}


if ($aksi == 'hapus') {
  $id = mysqli_real_escape_string($db, $_GET['hapus']);
  $query = mysqli_query($db, "DELETE FROM pesanan WHERE id = '$id'");
}

?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <h2 class="section-title">Pesanan</h2>

    <div class="section-body">
      <div class="row">
        <?=$msg;?>
        <?php if($aksi == 'edit'){ ?>
        <div class="col-12">
          <div class="card">

            <div class="card-header">
              <h5><?=ucwords($aksi);?></h5>
            </div>
            <div class="card-body">
              <form method="POST">
                <div class="form-group">
                  <label>Data</label>
                  <input type="text" class="form-control" name="data" placeholder="data" value="<?=htmlspecialchars($data_edit['data']);?>">
                </div>
                <div class="form-group">
                  <label>Keterangan</label>
                  <textarea class="form-control" name="keterangan" placeholder="keterangan"><?=htmlspecialchars($data_edit['keterangan']);?></textarea>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-danger" href="?">Tutup</a>
              </form>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive table-body" id="messages">
                <table class="table table-striped" id="table_ajax" style="width: 100%">
                  <thead class="thead-light">
                    <tr>
                      <th>ID</th>
                      <th>Pengguna</th>
                      <th>Layanan</th>
                      <th>Kategori</th>
                      <th>Data</th>
                      <th>Keterangan</th>
                      <th>Harga</th>
                      <th>Pembayaran</th>
                      <th>Invoice</th>
                      <th>Nomor</th>
                      <th>Status</th>
                      <th>Tanggal</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>
<script>
  $(document).ready(function () {
    $('#table_ajax').DataTable({
      "order": [[0, 'desc']],
      "processing": false,
      "serverSide": true,
      "bInfo": false,
      "ajax": "ajax/list_pesanan",
      "language": {
        "paginate": {
          "previous": "<i class='fas fa-angle-left'></i>",
          "next": "<i class='fas fa-angle-right'></i>",
        }
      }
    });
  });

  function confirmation(ev) {
    ev.preventDefault();
    var urlToRedirect = ev.currentTarget.getAttribute(
    'href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
    console.log(urlToRedirect); // verify if this is the right URL
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Data!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          swal("Poof! Your Data has been deleted!", {
            icon: "success",

          })
          window.location.href = urlToRedirect;
        } else {
          swal("Your Data is safe!");
        }
      });
  }
</script>