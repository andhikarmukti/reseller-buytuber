<?php
require '../config.php';
$sub_judul = ' - Pengaturan';
require 'lib/header.php';
$id = 1;
$aksi = key($_GET);

if ($_POST) {
    $judul = mysqli_real_escape_string($db, $_POST['judul']);
    $deskripsi_footer = mysqli_real_escape_string($db, $_POST['deskripsi_footer']);
    $maintenance = mysqli_real_escape_string($db, $_POST['maintenance']);
    $whatsapp = mysqli_real_escape_string($db, $_POST['whatsapp']);
    $facebook = mysqli_real_escape_string($db, $_POST['facebook']);
    $instagram = mysqli_real_escape_string($db, $_POST['instagram']);
    $keywords = mysqli_real_escape_string($db, $_POST['keywords']);
    $deskripsi_meta = mysqli_real_escape_string($db, $_POST['deskripsi_meta']);
    $author = mysqli_real_escape_string($db, $_POST['author']);
    $terms = mysqli_real_escape_string($db, $_POST['terms']);
    $privacy = mysqli_real_escape_string($db, $_POST['privacy']);
    $recaptcha_site_key = mysqli_real_escape_string($db, $_POST['recaptcha_site_key']);
    $recaptcha_secret_key = mysqli_real_escape_string($db, $_POST['recaptcha_secret_key']);
    $recaptcha_order_site_key = mysqli_real_escape_string($db, $_POST['recaptcha_order_site_key']);
    $recaptcha_order_secret_key = mysqli_real_escape_string($db, $_POST['recaptcha_order_secret_key']);
    
    
    $judul = htmlspecialchars($judul);
    $deskripsi_footer = htmlspecialchars($deskripsi_footer);
    $maintenance = htmlspecialchars($maintenance);
    $whatsapp = htmlspecialchars($whatsapp);
    $facebook = htmlspecialchars($facebook);
    $instagram = htmlspecialchars($instagram);
    $keywords = htmlspecialchars($keywords);
    $deskripsi_meta = htmlspecialchars($deskripsi_meta);
    $author = htmlspecialchars($author);
    $terms = htmlspecialchars($terms);
    $privacy = htmlspecialchars($privacy);
    $recaptcha_site_key = htmlspecialchars($recaptcha_site_key);
    $recaptcha_secret_key = htmlspecialchars($recaptcha_secret_key);
    $recaptcha_order_site_key = htmlspecialchars($recaptcha_order_site_key);
    $recaptcha_order_secret_key = htmlspecialchars($recaptcha_order_secret_key);

    if (isset($_POST['general'])) {
        if ($judul) {
            $query = mysqli_query($db, "UPDATE pengaturan SET 
            judul = '$judul',
            deskripsi_footer = '$deskripsi_footer',
            maintenance = '$maintenance',
            whatsapp = '$whatsapp',
            facebook = '$facebook',
            instagram = '$instagram'
          WHERE id = '$id'");
            if ($_FILES['logo']['name'][0]) {
                include 'lib/main_multi_upload_logo.php';
                $query = mysqli_query($db, "UPDATE pengaturan SET logo = '$result_logo' WHERE id = '$id'");
            }
            if ($_FILES['favicon']['name'][0]) {
              include 'lib/main_multi_upload_favicon.php';
              $query = mysqli_query($db, "UPDATE pengaturan SET favicon = '$result_favicon' WHERE id = '$id'");
          }
            $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
        } else {
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
        }
    }

    if (isset($_POST['metatag'])) {
        if ($keywords) {
            $query = mysqli_query($db, "UPDATE pengaturan SET 
          keywords = '$keywords',
          deskripsi_meta = '$deskripsi_meta',
          author = '$author'
        WHERE id = '$id'");
            $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
        } else {
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
        }
    }

    if (isset($_POST['recaptcha'])) {
        if ($recaptcha_site_key) {
            $query = mysqli_query($db, "UPDATE pengaturan SET 
                recaptcha_site_key = '$recaptcha_site_key',
                recaptcha_secret_key = '$recaptcha_secret_key'
                WHERE id = '$id'
            ");
            $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
        } else {
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
        }
    }
    
    if (isset($_POST['recaptcha_order'])) {
        if ($recaptcha_order_site_key) {
            $query = mysqli_query($db, "UPDATE pengaturan SET 
                recaptcha_order_site_key = '$recaptcha_order_site_key',
                recaptcha_order_secret_key = '$recaptcha_order_secret_key'
                WHERE id = '$id'
            ");
            $msg = '<div class="col-12"><div class="alert alert-primary alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Berhasil.</div></div></div>';
        } else {
            $msg = '<div class="col-12"><div class="alert alert-danger alert-dismissible show fade"><div class="alert-body"><button class="close" data-dismiss="alert"><span>×</span></button>Gagal data tidak lengkap.</div></div></div>';
        }
    }
}

if ($aksi == 'recaptcha_status_on') {
  $query = mysqli_query($db, "UPDATE pengaturan SET recaptcha_status = 1 WHERE id = '$id'");
}

if ($aksi == 'recaptcha_status_off') {
  $query = mysqli_query($db, "UPDATE pengaturan SET recaptcha_status = 0 WHERE id = '$id'");
}

if ($aksi == 'recaptcha_order_status_on') {
  $query = mysqli_query($db, "UPDATE pengaturan SET recaptcha_order_status = 1 WHERE id = '$id'");
}

if ($aksi == 'recaptcha_order_status_off') {
  $query = mysqli_query($db, "UPDATE pengaturan SET recaptcha_order_status = 0 WHERE id = '$id'");
}

$data_edit = mysqli_query($db, "SELECT * FROM pengaturan WHERE id = '$id'");
$data_edit = mysqli_fetch_array($data_edit);
$list_logo = json_decode($data_edit['logo'], 1);
$list_favicon = json_decode($data_edit['favicon'], 1);
print_r($list_logo);
$recaptcha_status_btn = ($data_edit['recaptcha_status']) ? '<a href="?recaptcha_status_off"><i class="fas fa-toggle-on" style="font-size: 30px;"></i></a>' : '<a href="?recaptcha_status_on"><i class="fas fa-toggle-off text-danger" style="font-size: 30px;"></i></a>';
$recaptcha_order_status_btn = ($data_edit['recaptcha_order_status']) ? '<a href="?recaptcha_order_status_off"><i class="fas fa-toggle-on" style="font-size: 30px;"></i></a>' : '<a href="?recaptcha_order_status_on"><i class="fas fa-toggle-off text-danger" style="font-size: 30px;"></i></a>';
?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
  <h2 class="section-title">Pengaturan</h2>

    <div class="section-body">
      <div class="row">
        <?=$msg;?>
        <div class="col-md-6 col-12">
          <div class="card">
            <div class="card-header">
              <h4>General</h4>
            </div>
            <div class="card-body">
              <form method="POST" enctype="multipart/form-data">
                <div class="form-group">
                  <label>Judul Website</label>
                  <input class="form-control" name="judul" placeholder="Masukan judul website kamu" value="<?=htmlspecialchars($data_edit['judul']);?>">
                </div>
                <div class="form-group">
                  <label>Logo</label>
                  
                    <?php if($list_logo[0]){ ?>
                    <div>
                      <small>Note : Jangan upload bila tidak ingin mengganti gambar</small>
                    <div class="gallery gallery-md mt-2">
                      <?php
                        foreach ($list_logo as $ini_logo) {
                            echo '<div class="gallery-item" data-image="../assets/img/'.$ini_logo.'"></div>';
                        }
                      ?>
                    </div>
                    </div>
                  <?php } ?>
                  
                  <div class="input_fields_wrap">
                    <div class="input-group">
                      <input type="file" class="form-control" id="customFile" name="logo[]">
                      <div class="input-group-append">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Favicon</label>
                  
                    <?php if($list_favicon[0]){ ?>
                    <div>
                      <small>Note : Jangan upload bila tidak ingin mengganti gambar</small>
                    <div class="gallery gallery-md mt-2">
                      <?php
                        foreach ($list_favicon as $ini_favicon) {
                            echo '<div class="gallery-item" data-image="../assets/img/'.$ini_favicon.'"></div>';
                        }
                      ?>
                    </div>
                    </div>
                  <?php } ?>
                  
                  <div class="input_fields_wrap">
                    <div class="input-group">
                      <input type="file" class="form-control" id="customFile" name="favicon[]">
                      <div class="input-group-append">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Deskripsi (Footer)</label>
                  <textarea class="form-control" rows="10" name="deskripsi_footer" placeholder="Deskripsikan website kamu disni" style="height: 100px;"><?=htmlspecialchars($data_edit['deskripsi_footer']);?></textarea>
                </div>
                <div class="form-group">
                  <label>Maintenance</label>
                  <div class="selectgroup selectgroup-pills">
                    <label class="selectgroup-item">
                      <input type="radio" name="maintenance" value="1" class="selectgroup-input" <?=$checked = ($data_edit['maintenance'] == 1) ? 'checked' : '';?>>
                      <span class="selectgroup-button selectgroup-button-icon"><b>YES</b></span>
                    </label>
                    <label class="selectgroup-item">
                      <input type="radio" name="maintenance" value="0" class="selectgroup-input" <?=$checked = ($data_edit['maintenance'] == 0) ? 'checked' : '';?>>
                      <span class="selectgroup-button selectgroup-button-icon"><b>NO</b></span>
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label>Contact</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fab fa-whatsapp"></i></div>
                    </div>
                    <input type="number" class="form-control currency" name="whatsapp" value="<?=htmlspecialchars($data_edit['whatsapp']);?>" placeholder="ex: 628xxxxxxxxx">
                  </div>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fab fa-facebook-f"></i></div>
                    </div>
                    <input type="text" class="form-control currency" name="facebook" value="<?=htmlspecialchars($data_edit['facebook']);?>" placeholder="ex: kodegud">
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fab fa-instagram"></i></div>
                    </div>
                    <input type="text" class="form-control currency" name="instagram" value="<?=htmlspecialchars($data_edit['instagram']);?>" placeholder="ex: kodegud">
                  </div>
                </div>
                <button type="submit" class="btn btn-primary" name="general">Submit</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-12">
          <div class="card">
            <div class="card-header">
              <h4>Metatag</h4>
            </div>
            <div class="card-body">
              <form method="POST">
                <div class="form-group">
                  <label>Keywords</label>
                  <input class="form-control" name="keywords" placeholder="keyword_1, keyword_2, etc" value="<?=htmlspecialchars($data_edit['keywords']);?>">
                </div>
                <div class="form-group">
                  <label>Meta Deskripsi</label>
                  <textarea class="form-control" rows="10" name="deskripsi_meta" placeholder="Deskripsikan website kamu disni" style="height: 100px;"><?=htmlspecialchars($data_edit['deskripsi_meta']);?></textarea>
                </div>
                <div class="form-group">
                  <label>Author</label>
                  <input class="form-control" name="author" placeholder="Masukan nama author" value="<?=htmlspecialchars($data_edit['author']);?>">
                </div>
                <button type="submit" class="btn btn-primary" name="metatag">Submit</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-12">
          <div class="card">
            <div class="card-header">
              <h4>Recaptcha Register</h4>
              <div class="card-header-action">
              <?=$recaptcha_status_btn;?>
              </div>
            </div>
            <div class="card-body">
              <?php if($data_edit['recaptcha_status']){ ?>
              <form method="POST">
                <div class="form-group">
                  <label>Site Key</label>
                  <input class="form-control" name="recaptcha_site_key" placeholder="Site key recaptcha" value="<?=htmlspecialchars($data_edit['recaptcha_site_key']);?>">
                </div>
                <div class="form-group">
                  <label>Secret Key</label>
                  <input class="form-control" name="recaptcha_secret_key" placeholder="Secret ket recaptcha" value="<?=htmlspecialchars($data_edit['recaptcha_secret_key']);?>">
                </div>
                <div>
                <button type="submit" class="btn btn-primary" name="recaptcha">Submit</button>
                </div>
              </form>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-12">
          <div class="card">
            <div class="card-header">
              <h4>Recaptcha Invisible Order</h4>
              <div class="card-header-action">
              <?=$recaptcha_order_status_btn;?>
              </div>
            </div>
            <div class="card-body">
              <?php if($data_edit['recaptcha_order_status']){ ?>
              <form method="POST">
                <div class="form-group">
                  <label>Site Key</label>
                  <input class="form-control" name="recaptcha_order_site_key" placeholder="Site key recaptcha" value="<?=htmlspecialchars($data_edit['recaptcha_order_site_key']);?>">
                </div>
                <div class="form-group">
                  <label>Secret Key</label>
                  <input class="form-control" name="recaptcha_order_secret_key" placeholder="Secret ket recaptcha" value="<?=htmlspecialchars($data_edit['recaptcha_order_secret_key']);?>">
                </div>
                <div>
                <button type="submit" class="btn btn-primary" name="recaptcha_order">Submit</button>
                </div>
              </form>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>