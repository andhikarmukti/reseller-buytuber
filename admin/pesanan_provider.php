<?php
require '../config.php';
require 'lib/cek_admin.php';
header("Content-Type: application/json"); 

$id = mysqli_real_escape_string($db, $_GET['id']);

$data_edit = mysqli_query($db, "SELECT * FROM pesanan WHERE id = '$id'");
$data_edit = mysqli_fetch_array($data_edit);

if($data_edit['res_provider']){
    print_r(json_decode(htmlspecialchars($data_edit['res_provider'],1)));
}else if($data_edit['id_provider']){
    if($data_edit['status'] == 'processing'){
        echo "jika masih processing dalam kurun waktu yang lama \nmohon cek transaksi secara manual pada provider masing-masing \napabila transaksi sukses mohon ganti status ke 'success'\nini terjadi dikarenakan timeout dari pihak provider / hosting anda";
    }
}