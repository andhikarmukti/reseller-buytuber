<?php
require '../config.php';

$sub_judul = ' - Transfer Saldo Pengguna';
require '../lib/model.php';
$model = new Model();


  $ez = mysqli_query($db, "SELECT * FROM users WHERE level='Admin'");
  $data=mysqli_fetch_array($ez);
  $saldo = $data['saldo'];


require 'lib/header.php';


?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
  <h2 class="section-title">Transfer Saldo Pengguna</h2>

    <div class="section-body">
      <div class="row">
    

        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h5>Kirim Saldo</h5>
            </div>
            <div class="card-body">
            <?php
                            if ($_POST) {
	$input_data = array('email', 'amount');
	if (check_input($_POST, $input_data) == false) {
		 $msg = '<div class="alert alert-danger bg-danger text-white border-0" role="alert">
                        <strong>Gagal!</strong> Input Tidak Sesuai.
                    </div>';
	} else {
		$input_post = array(
			'email' => trim($_POST['email']),
			'amount' => $_POST['amount'],
		);
		if (check_empty($input_post) == true) {
			 $msg = '<div class="alert alert-danger bg-danger text-white border-0" role="alert">
                        <strong>Gagal!</strong> Input Tidak boleh kosong.
                    </div>';
		} elseif ($_POST['amount'] < 1) {
			 $msg = '<div class="alert alert-danger bg-danger text-white border-0" role="alert">
                        <strong>Gagal!</strong> Minimal transfer saldo Rp 1.
                    </div>';
		}  else {
			$user_target = $model->db_query($db, "*", "users", "email = '".mysqli_real_escape_string($db, $input_post['email'])."'");
			if ($user_target['count'] == 0) {
				 $msg = '<div class="alert alert-danger bg-danger text-white border-0" role="alert">
                        <strong>Gagal!</strong> email tidak ditemukan.
                    </div>';
			} else {
			  // nulis log
			  $log_msg = "Transfer saldo $_POST[amount] ke $_POST[email]";
			  mysqli_query($db, "INSERT INTO log VALUES(null, '$data_user[email]', '$log_msg', '$_SERVER[REMOTE_ADDR]', '".date('Y-m-d H:i:s')."')");
			  
			  $model->db_update($db, "users", array('saldo' => $user_target['rows']['saldo'] + $_POST['amount']), "id = '".$user_target['rows']['id']."'");
			  $msg = '<div class="alert alert-success bg-success text-white border-0" role="alert">
                        <strong>Sukses!</strong> Transfer saldo berhasil <br />Penerima: '.$input_post['email'].'<br />Jumlah Saldo: '.$_POST['amount'].'
                    </div>';
			
			
			}
		}
	}
	echo $msg;
}
?>
              <form method="POST">
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" class="form-control" name="email">
                </div>
                <div class="form-group">
                  <label>Saldo</label>
                  <input type="number" class="form-control" name="amount">
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-danger" href="?">Tutup</a>
              </form>
            </div>
          </div>
        </div>
     
  
      </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>
