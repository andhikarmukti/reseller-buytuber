<?php
require 'config.php';
$sub_judul = " - Order";
require 'lib/order_config.php';

$uri_array = explode("/", $_SERVER['REQUEST_URI']);
$nama_kategori = mysqli_real_escape_string($db, ucwords(str_replace('-', ' ', $uri_array[2])));
$db_kategori = mysqli_query($db, "SELECT * FROM kategori WHERE nama='$nama_kategori'");
$kategori = mysqli_fetch_array($db_kategori);
$sub_judul = "Top Up " . $kategori['nama'] . " Murah | ";
$gambar_bantuan = json_decode($kategori['bantuan'], 1);
// fix + paling bawah $uri_array[3]
if (!$kategori['id']) {
    header('Location: ../');
    die();
}
if ($kategori['status'] == '0') {
    header('Location: ../');
    die();
}
require 'lib/header.php';
?>
<?php if ($web_data['recaptcha_order_status']) { ?>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <style type="text/css">
        .grecaptcha-badge {
            width: 70px !important;
            overflow: hidden !important;
            transition: all 0.3s ease !important;
            left: 4px !important;
        }

        .grecaptcha-badge:hover {
            width: 256px !important;
        }
    </style>
<?php } ?>
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-body">
                <div class="row">
                    <div class="col-md-4 col-12 mt-3">
                        <div class="row">
                            <div class="col-12 text-md-left text-center">
                                <img src="<?= $domain; ?>assets/img/kategori/<?= json_decode($kategori['gambar'], 1)[0]; ?>"
                                     class="rounded mr-3 img-fluid" style="border-radius: 10px;" width="200px"
                                     height="200px">
                            </div>
                            <div class="col-12 mt-3">
                                <h5 class="text-white"><?= $kategori['nama']; ?></h5>
                                <p style="color: #b3b3b3;"><?= $kategori['detail']; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-12 mt-3">
                        <form id="form_order" method="POST">
                            <div class="row">
                                <input type="hidden" name="kategori" id="kategori" value="<?= $kategori['id']; ?>">
                                <div class="col-12" id="form"></div>
                                <div class="col-12">
                                    <h5><span class="badge badge-primary badge-square mr-1"> </span> Pilih Nominal</h5>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row row-cols-2">
                                                <?php
                                                $db_layanan = mysqli_query($db, "SELECT * FROM layanan WHERE id_kategori = '$kategori[id]' AND status='1' ORDER BY harga ASC");
                                                while ($layanan = mysqli_fetch_array($db_layanan)) {
                                                    ?>
                                                    <div class="col-lg-4 mt-3">
                                                        <div class="list-group shadow h-100">
                                                            <input type="radio" id="layanan_<?= $layanan['id']; ?>"
                                                                   value="<?= $layanan['id']; ?>" name="layanan"
                                                                   data-type="diamond" required>
                                                            <label for="layanan_<?= $layanan['id']; ?>"
                                                                   class="list-group-item h-100">
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <div class="row">
                                                                            <div class="col nama-layanan-form"><?= $layanan['nama']; ?></div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col nominal-price">
                                                                                Rp <?= ceil($layanan['harga']); ?></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-3 col-1 m-auto">
                                                                        <?php
                                                                        if (!$layanan['icon_layanan']) { ?>

                                                                        <?php } else { ?>
                                                                            <img src="<?= $domain; ?>assets/img/kategori/<?= json_decode($layanan['icon_layanan'], 1)[0]; ?>"
                                                                                 width="32px"
                                                                                 style=" top: -1500%;right: 5%;position: absolute;"
                                                                                 alt="">
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <?php $i++;
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <h5><span class="badge badge-primary badge-square mr-1"> </span> Pilih Pembayaran
                                    </h5>
                                    <div class="card">
                                        <div id="accordion">
                                            <div class="card-body">

                                                <?php
                                                $db_jenis = mysqli_query($db, "SELECT * FROM jenis_pembayaran WHERE status='1'");
                                                while ($jenis = mysqli_fetch_array($db_jenis)) {
                                                    ?>

                                                    <!-- Payment  Category-->
                                                    <div class="accordion">
                                                        <div class="area-list-payment-method">
                                                            <div class="child-box payment-drawwer shadow">
                                                                <div class="header short-payment-support-info-head"
                                                                     role="button" data-toggle="collapse"
                                                                     data-target="#panel-body-<?= $jenis['id']; ?>">
                                                                    <div class="left">
                                                                        <b>
                                                                            <i class="<?= $jenis['icon']; ?>"></i> <?= $jenis['nama']; ?>
                                                                        </b>
                                                                    </div>
                                                                </div>
                                                                <div class="accordion-body collapse"
                                                                     id="panel-body-<?= $jenis['id']; ?>"
                                                                     data-parent="#accordion">

                                                                    <div class="row row-cols-2 row-cols-md-3 p-1">


                                                                        <?php
                                                                        $db_metode = mysqli_query($db, "SELECT * FROM metode_pembayaran WHERE jenis_pembayaran = '$jenis[id]' AND status='1' AND id NOT IN (1,2)");
                                                                        $i = 1;
                                                                        while ($metode = mysqli_fetch_array($db_metode)) {
                                                                            ?>
                                                                            <input required type="radio"
                                                                                   id="method_<?= $metode['id']; ?>"
                                                                                   class="radio-pembayaran"
                                                                                   name="metode"
                                                                                   value="<?= $metode['id']; ?>">
                                                                            <label for="method_<?= $metode['id']; ?>">
                                                                                <!--<i class="fas fa-check-square position-absolute" style="font-size: 15px;"></i>-->
                                                                                <div class="row py-3 mx-auto">
                                                                                    <div class="col-4">
                                                                                        <img src="<?= $domain; ?>assets/img/<?= json_decode($metode['gambar'], 1)[0]; ?>"
                                                                                             width="100px"></div>


                                                                                    <div class="col-8 text-right">
                                                                                        Harga<br>
                                                                                        <b id="<?= str_replace(' ', '_', $metode['nama']); ?>">Rp
                                                                                            0</b>
                                                                                    </div>


                                                                                </div>
                                                                            </label>
                                                                            <?php if ($metode['pake_rek']) { ?>

                                                                                <?php if ($metode['id'] == 6) { ?>
                                                                                    <div class="col-md-12 col-12">
                                                                                        <div class="rek"
                                                                                             id="rek_<?= $metode['id']; ?>"
                                                                                             style="display: none;">
                                                                                            <input type="text"
                                                                                                   class="form-control mt-1 mb-3"
                                                                                                   placeholder="<?= ucwords($metode['keterangan_rek']); ?>"
                                                                                                   name="rek" required>
                                                                                        </div>
                                                                                    </div>

                                                                                <?php } else if ($metode['id'] == 15) { ?>
                                                                                    <div class="col-md-12 col-12">
                                                                                        <div class="rek"
                                                                                             id="rek_<?= $metode['id']; ?>"
                                                                                             style="display: none;">
                                                                                            <input type="text"
                                                                                                   class="form-control mt-1 mb-3"
                                                                                                   placeholder="<?= ucwords($metode['keterangan_rek']); ?>"
                                                                                                   name="rek_tsel"
                                                                                                   required>
                                                                                        </div>
                                                                                    </div>
                                                                                <?php } else { ?>
                                                                                    <div class="col-md-12 col-12">
                                                                                        <div class="rek"
                                                                                             id="rek_<?= $metode['id']; ?>"
                                                                                             style="display: none;">
                                                                                            <input type="text"
                                                                                                   class="form-control mt-1 mb-3"
                                                                                                   placeholder="<?= ucwords($metode['keterangan_rek']); ?>"
                                                                                                   name="rek_xlaxis"
                                                                                                   required>
                                                                                        </div>
                                                                                    </div>

                                                                                <?php }
                                                                            } ?>
                                                                            <?php $i++;
                                                                        } ?>
                                                                    </div>
                                                                </div>
                                                                <div class="short-payment-support-info">
                                                                    <img src="<?= $domain; ?>assets/img/<?= $jenis['image']; ?>?x">


                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End Payment Category -->
                                                <?php } ?>

                                                <?php if (true) { ?>
                                                    <input required type="radio" id="method_saldo"
                                                           class="radio-pembayaran" name="metode" value="2">
                                                    <label for="method_saldo">
                                                        <div class="row py-3 mx-auto">
                                                            <div class="col-6">
                                                                <div style="display: inline-block;"><i
                                                                            class="fas fa-wallet bg-primary p-2 text-dark"
                                                                            style="font-size: 15px; border-radius:5px"></i>
                                                                </div>
                                                                <div style="display: inline-block;">Saldo</div>
                                                            </div>
                                                            <div class="col-6 text-right">
                                                                Harga<br>
                                                                <b id="Saldo">Rp 0</b>
                                                            </div>
                                                        </div>
                                                    </label>
                                                    <div class="rek" id="rek_2" style="display: none;">
                                                        <?php if ($data_user['id']) { ?>
                                                            <div class="alert alert-warning" role="alert">Saldo anda
                                                                saat ini <?= floor($data_user['saldo']); ?></div>
                                                        <?php } else { ?>
                                                            <div class="alert alert-danger" role="alert">Untuk
                                                                menggunakan metode ini anda harus <a
                                                                        href="<?= $domain; ?>login">masuk</a> terlebih
                                                                dahulu
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <h5><span class="badge badge-primary badge-square mr-1"> </span> Beli</h5>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="">
                                                <label>Nomor WhatsApp</label>
                                                <input required type="number" class="form-control"
                                                       placeholder="62814XXXXXX" id="email"
                                                       name="kontak">
                                            </div>
                                            <div class="mt-2">
                      <span class="text-muted">Dengan membeli otomatis saya menyutujui <a
                                  href="<?= $domain; ?>terms">Ketentuan Layanan</a></span>
                                            </div>
                                            <div class="mt-2">
                                                <input required class="btn btn-primary" id="btn_beli" type="button"
                                                       value="Beli Sekarang">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($web_data['recaptcha_order_status']) { ?>
                                    <div class="g-recaptcha"
                                         data-sitekey="<?= $web_data['recaptcha_order_site_key']; ?>"
                                         data-size="invisible">
                                    </div>
                                <?php } ?>
                            </div>
                        </form>
                    </div>
                </div>
        </section>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
        <div class="modal-dialog" role="document">
            <img src="<?= $domain ?>assets/img/idserver/<?= $gambar_bantuan['0'] ?>" class="img-fluid"
                 style="border-radius: 10px;"
                 width="450px" height="100px">
        </div>
    </div>
    </div>
<?php
require 'lib/footer.php';
?>
<?= $msg_js; ?>

    <script>
      $(document).ready(function () {
        $('input[name$=\'metode\']').click(function () {
          var test = $(this).val()
          $('div.rek').hide()
          $('#rek_' + test).show()
        })
      })

      function get_form () {
        $.ajax({
          type: 'post',
          url: "<?=$domain;?>ajax/form_order_html?id=<?=$kategori['id'];?>",
          success: function (dataResult) {
            $('#form').html(dataResult)
            load_badge_num()
          },
          error: function (data) {
            swal('Gagal load form, refresh halaman', '', 'error')
          }
        })
      }

      get_form()
      $(document).ready(function () {
        $('input[type=radio][name=layanan]').change(function () {
          var layanan = $('input[name=\'layanan\']:checked').val()
          //var qty = $('#quantity').val();
          if (layanan) {
            $.ajax({
              url: "<?=$domain;?>ajax/price.php",
              type: 'POST',
              dataType: 'json',
              data: {
                layanan: layanan,
                //qty: qty
              },
              success: function (data) {
                if (data.status == true) {
                  $.each(data.data, function (i, item) {
                    $('#' + item.nama).html(item.harga)
                  })
                } else {
                  $('#bca_price').html('')
                  $('#ovo_price').html('')
                  $('#gopay_price').html('')
                }
              },
              error: function (jqXHR, exception) {
                $('#bca_price').html('')
                $('#ovo_price').html('')
                $('#gopay_price').html('')
              }
            })
          }
        })
        $('#btn_beli').click(function () {
            <?=($web_data['recaptcha_order_status']) ? 'grecaptcha.execute();' : ''; ?>
          var required = $('input,select').filter('[required]:visible')
          var allRequired = true
          required.each(function () {
            if ($(this).val() == '') {
              allRequired = false
            }
          })
          if (!allRequired) {
            swal('Form tidak lengkap', '', 'error')
          } else {
            $('#form_order').text(function () {
              var pdata = $(this).serialize()
              $.ajax({
                url: "<?=$domain;?>/ajax/order_confirm.php",
                data: pdata,
                timeout: false,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                  $('#btn_beli').val('Beli Sekarang')
                  $('input').removeAttr('disabled', 'disabled')
                  if (data.status) {

                    swal({
                      text: data.msg,
                      buttons: {
                        cancel: 'Batal',
                        catch: 'Lanjutkan',
                      },
                    })
                      .then((value) => {
                        switch (value) {
                          case 'catch':
                            $('form').submit()
                            $('.preloader').fadeIn()
                            break

                          default:
                            break
                        }
                      })
                  } else {
                    swal(data.msg, '', 'error')
                  }
                },
                error: function (a, b, c) {
                  $('input').removeAttr('disabled', 'disabled')
                  $('#btn_beli').val('Beli Sekarang')
                  swal('Kesalahan Sistem', '', 'error')
                },
                beforeSend: function () {
                  $('input').attr('disabled', 'disabled')
                  $('#btn_beli').val('Please Wait')
                }
              })
            })
          }
        })
      })

      function load_badge_num () {
        var badge_num = 0
        $('.badge-square').each(function (i, obj) {
          badge_num++
          $(obj).html(badge_num)
        })
      }

      load_badge_num()
    </script>
<?php if (htmlspecialchars($uri_array[3])) { ?>
    <script> setTimeout(function () { $("input:radio[id='layanan_<?=htmlspecialchars($uri_array[3]);?>']").trigger('click') }, 1000)</script>
<?php } ?>