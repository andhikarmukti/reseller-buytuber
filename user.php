<?php
require 'config.php';
require 'lib/redirect_not_login.php';
require 'lib/header.php';
$id_user = $_SESSION['id'];
$data_pesanan = mysqli_query($db, "SELECT * FROM pesanan WHERE id_user = '$id_user'");
$data_edit_pesanan = mysqli_fetch_array($data_pesanan);

//jumlah pembelian
$jumlah_pembelian = mysqli_query($db,"SELECT SUM(harga) AS total FROM pesanan WHERE id_user ='$id_user' AND status='success' AND id_kategori NOT IN (0)");
$data_pembelian = mysqli_fetch_array($jumlah_pembelian);

//jumlah Top Up
$jumlah_topup = mysqli_query($db,"SELECT SUM(harga) AS total FROM pesanan WHERE id_user ='$id_user' AND status='success' AND id_kategori = '0'");
$data_topup = mysqli_fetch_array($jumlah_topup);
?> 
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content bg-dark">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Mau Upgrade Level?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p class="text-white">Jika kalian ingin upgrade ke level silver / gold / pro akan dikenakan biaya. untuk info lebih lanjut silahkan kontak ke admin jika kalian ingin upgrade</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Nanti Deh</button>
      </div>
    </div>
  </div>
</div>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <div class="row">
        <div class="col-12">
          <div class="card profile-widget">
            <div class="profile-widget-header">
              <img alt="image" src="assets/img/avatar/avatar-1.png" class="rounded-circle profile-widget-picture">
              <div class="profile-widget-items">
                <div class="profile-widget-item">
                  <div class="profile-widget-item-label"><a href="saldo">Saldo</a></div>
                  <div class="profile-widget-item-value"><a
                      href="saldo" style="color: #f9b216"><?="Rp ". number_format($data_user['saldo'], 0, ',', ',');?></a></div>
                </div>
                <div class="profile-widget-item">
                  <div class="profile-widget-item-label text-white">Pembelian</div>
                  <div class="profile-widget-item-value text-white">
                    <?="Rp ". number_format($data_pembelian['total'], 0, ',', ',');?></div>
                </div>
                <div class="profile-widget-item">
                  <div class="profile-widget-item-label text-white">Top Up</div>
                  <div class="profile-widget-item-value text-white">
                    <?="Rp ". number_format($data_topup['total'], 0, ',', ',');?></div>
                </div>
              </div>
            </div>
            <div class="profile-widget-description">
              <div class="profile-widget-name text-white"><?=ucwords($data_user['nama']);?> <div
                  class="text-muted d-inline font-weight-normal">
                  <div class="slash"></div> <?=htmlspecialchars(ucwords($data_user['level']));?>
                  <?=($data_user['level'] != 'pro') ? '<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-level-up-alt"></i> Upgrade</button>' : '';?>
                </div>
              </div>
              <p class="text-white">Klik jumlah saldo untuk topup saldo anda</p>
            </div>
          </div>
        </div>
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4>Riwayat Pesanan</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table_ajax" style="width: 100%;">
                  <thead>
                    <tr>
                      <th class="text-center text-white" style="width: 1px;">ID</th>
                      <th class="text-white">Kategori</th>
                      <th class="text-white">Layanan</th>
                      <th class="text-white">Harga</th>
                      <th class="text-white">Tanggal</th>
                      <th class="text-white">Status</th>
                      <th class="text-white">Action</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>

<script>
  $(document).ready(function () {
    $('#table_ajax').DataTable({
      "order": [[4, 'desc']],
      "processing": false,
      "serverSide": true,
      "bInfo": false,
      "ajax": "<?=$domain;?>ajax/list_pesanan.php",
      "language": {
        "paginate": {
          "previous": "<i class='fas fa-angle-left'></i>",
          "next": "<i class='fas fa-angle-right'></i>",
        }
      }
    });
  });

  function detail_pesanan(id) {
    $.ajax({
      url: "<?=$domain;?>/ajax/detail_pesanan.php",
      data: 'id='+id,
      timeout: false,
      type: 'POST',
      dataType: 'json',
      success: function (data) {
        $(".btn-pesanan").val('Detail');
        $("input").removeAttr("disabled", "disabled");
        $("button").removeAttr("disabled", "disabled");
        if (data.status) {
          swal({
            html:true,
            text: data.msg,
            button: "OK"
          });
        } else {
          swal(data.msg, '', 'error');
        }
      },
      error: function (a, b, c) {
        $("input").removeAttr("disabled", "disabled");
        $("button").removeAttr("disabled", "disabled");
        $(".btn-pesanan").val('Detail');
        swal('Kesalahan Sistem', '', 'error');
      },
      beforeSend: function () {
        $("input").attr("disabled", "disabled");
        $("button").attr("disabled", "disabled");
        $(".btn-pesanan").val('  <i class="fas fa-spinner fa-pulse"></i>');
      }
    });
  }
</script>