<?php
date_default_timezone_set('Asia/Jakarta');
/**
 * BCA Grab Mutasi
 *
 * @author MrDxdiag
 *
 * Release Date: June, 2018
 * @version 0.1.0
 *
 *
 */

function toIDR($money) {
    $m = explode('.', $money);
    return str_replace(',', '', $m[0]);
}

function fix_angka($string)
{
    $string = str_replace(',', '', $string);
    $string = strtok($string, '.');
    return $string;
}

/**
 * Coded by someone
 * Just edited some lines
 */
function grab_bca($user, $pass, $tgl) {
    $user_ip = '68.188.59.198';
    $ua = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36";
    $cookie = 'bca-cookie.txt';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
    curl_setopt($ch, CURLOPT_USERAGENT, $ua);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, 'https://ibank.klikbca.com');
    $info = curl_exec($ch);
    $a = strstr($info, 'var s = document.createElement(\'script\'), attrs = { src: (window.location.protocol ==', 1);
    $a = strstr($a, 'function getCurNum(){');
    $b = array(
        'return "',
        'function getCurNum(){',
        '";',
        '}',
        '{',
        '(function()'
    );
    $b = str_replace($b, '', $a);
    $curnum = trim($b);
    $params = 'value%28actions%29=login&value%28user_id%29=' . $user . '&value%28CurNum%29=' . $curnum . '&value%28user_ip%29=' . $user_ip . '&value%28browser_info%29=' . $ua . '&value%28mobile%29=false&value%28pswd%29=' . $pass . '&value%28Submit%29=LOGIN';
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, 'https://ibank.klikbca.com/authentication.do');
    curl_setopt($ch, CURLOPT_REFERER, 'https://ibank.klikbca.com');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_POST, 1);
    $info = curl_exec($ch);
    //print_r($info);
    // Buka menu
    curl_setopt($ch, CURLOPT_URL, 'https://ibank.klikbca.com/nav_bar_indo/menu_bar.htm');
    curl_setopt($ch, CURLOPT_REFERER, 'https://ibank.klikbca.com/authentication.do');
   // $info = curl_exec($ch);
    // Buka Informasi Rekening
    curl_setopt($ch, CURLOPT_URL, 'https://ibank.klikbca.com/nav_bar_indo/account_information_menu.htm');
    curl_setopt($ch, CURLOPT_REFERER, 'https://ibank.klikbca.com/authentication.do');
   // $info = curl_exec($ch);
    // Buka Mutasi Rekening
    curl_setopt($ch, CURLOPT_URL, 'https://ibank.klikbca.com/accountstmt.do?value(actions)=acct_stmt');
    curl_setopt($ch, CURLOPT_REFERER, 'https://ibank.klikbca.com/nav_bar_indo/account_information_menu.htm');
    curl_setopt($ch, CURLOPT_POST, 1);
    $info = curl_exec($ch);
    //print_r($info);
    // Parameter untuk Lihat Mutasi Rekening
    $params = array();
    $jkt_time = time() + (3600 * 7);
    $t1 = explode('-', $tgl);
    $t0 = explode('-', $tgl);
    $params[] = 'value%28startDt%29=' . $t0[2];
    $params[] = 'value%28startMt%29=' . $t0[1];
    $params[] = 'value%28startYr%29=' . $t0[0];
    $params[] = 'value%28endDt%29=' . $t1[2];
    $params[] = 'value%28endMt%29=' . $t1[1];
    $params[] = 'value%28endYr%29=' . $t1[0];
    $params[] = 'value%28D1%29=0';
    $params[] = 'value%28r1%29=1';
    $params[] = 'value%28fDt%29=';
    $params[] = 'value%28tDt%29=';
    $params[] = 'value%28submit1%29=Lihat+Mutasi+Rekening';
    $params = implode('&', $params);
    // Buka Lihat Mutasi Rekening & simpan hasilnya di $source
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, 'https://ibank.klikbca.com/accountstmt.do?value(actions)=acctstmtview');
    curl_setopt($ch, CURLOPT_REFERER, 'https://ibank.klikbca.com/nav_bar_indo/account_information_menu.htm');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_POST, 1);
    $source = curl_exec($ch);
    //print_r($source);
    // Logout, cURL close, hapus cookies
    curl_setopt($ch, CURLOPT_URL, 'https://ibank.klikbca.com/authentication.do?value(actions)=logout');
    curl_setopt($ch, CURLOPT_REFERER, 'https://ibank.klikbca.com/nav_bar_indo/account_information_menu.htm');
    $info = curl_exec($ch);
    curl_close($ch);
    @unlink($cookie);
    return $source;
}

function fetchResponseFrom($str) {
    //file_put_contents(date('d-m-y h:i:s.html'), $str);
    $exp = explode('<table border="0" cellpadding="0" cellspacing="0" width="590">', $str);
    $exp = explode('</table>', $exp[2]);
    $all = explode('<font face="verdana" size="1" color="#0000bb">', $exp[1]);

    $strings = [];
    foreach($all as $el) {
        $strings[] = trim(strip_tags($el));
    }

    $col = [];
    $total = count($strings) - 1;
    $cIt = 0;
    for ($it = 1; $total >= $it; $it++) {
        if ($it % 6 == 0) {
            $col[$cIt][] = $strings[$it];
            $cIt++;
        } else {
            $col[$cIt][] = $strings[$it];
        }
    }

    $result = array_map(function($r) {
        return [
          'tgl'         => $r[0],
          'keterangan'  => preg_replace('/\s+/', ' ', $r[1]),
          'cabang'      => $r[2],
          'mutasi'      => fix_angka($r[3]),
          'jenis'       => $r[4],
          'saldo'       => $r[5]
        ];
      }, $col);
    return $result;
}
ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
// Execute
$source = fetchResponseFrom(grab_bca('', '', date('Y-m-d')));

print_r($source);

if (count($source) > 0) {
  $db = mysqli_connect('localhost', '', '', '');
    foreach ($source as $g) {
            $keterangan = mysqli_real_escape_string($db, $g['keterangan']);
            echo "$g[saldo] : $keterangan => ";
            $find   =   mysqli_query($db, "SELECT * FROM mutasi_bca WHERE note = '$keterangan' AND mkey ='{$g['saldo']}'");
            if (mysqli_num_rows($find)) {
                //print(json_encode(['code' => 303, 'msg' => 'mutation already exists']));
                echo "exist \n";
                print_r(mysqli_error($db));
            } else {
                $query = "INSERT INTO mutasi_bca (bank, type, date, note, amount, status, mkey) VALUES ('BCA', '{$g['jenis']}', '{$g['tgl']}', '$keterangan', '{$g['mutasi']}', 'Pending', '{$g['saldo']}')";
                $exc = mysqli_query($db, $query);
                print_r(mysqli_error($db));
                echo "insert \n";
            }

    }
} else {
    print(json_encode(['code' => 500, 'msg' => 'error grab from bca']));
}
