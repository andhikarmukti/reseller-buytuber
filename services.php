<?php
require 'config.php';
$sub_judul = 'Daftar Layanan | ';
require 'lib/header.php';

$_SESSION['set_category'] = 'Semua Kategori';
?>
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-body">
      <h2 class="section-title">Daftar Layanan</h2>
      <div class="row">
        <div class="col-12 mb-2">
          <select name="kategori" id="kategori" class="form-control" onChange="setCategory(this)">
            <option>Semua Kategori</option>
            <?php
              $db_kategori = mysqli_query($db, "SELECT * FROM kategori WHERE status='1'");
               while ($kategori = mysqli_fetch_array($db_kategori)) {
                 echo "<option>$kategori[nama]</option>";
               }
            ?>
          </select>
        </div>
        <div class="col-12">
          <div class="card bg-dark-3">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-dark table-striped table-hover text-white" id="table-services" style="width: 100%;">
                <thead class="text-white">
                  <th class="text-white" style="max-width: 150px; text-align: center;">Nama</th>
                  <th class="text-white" style="text-align: center;">Kategori</th>
                  <th class="text-white" style="text-align: center;">Harga</th>
                  <th class="text-white" style="text-align: center;">Harga_Silver</th>
                  <th class="text-white" style="text-align: center;">Harga_Gold</th>
                  <th class="text-white" style="text-align: center;">Harga_Pro</th>
                  <th class="text-white" style="text-align: center;">Status</th>
                  <th class="text-white" style="text-align: center;">Aksi</th>
                </thead>
                <tbody></tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>
<?php
require 'lib/footer.php';
?>
<script>
  $(document).ready(function () {
        var groupColumn = 1;
        var table = $('#table-services').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": groupColumn
            }],
            "ajax": "<?=$domain;?>ajax/list_layanan.php",
            "order": [
                [groupColumn, 'asc']
            ],
            "displayLength": 25,
            "language": {
                "paginate": {
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "next": "<i class='fa fa-angle-right'></i>",
                }
            },
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;

                api.column(groupColumn, {
                    page: 'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="group"><td colspan="7">' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        // Order by the grouping
        $('#table-services tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === groupColumn && currentOrder[1] === 'asc') {
                table.order([groupColumn, 'desc']).draw();
            } else {
                table.order([groupColumn, 'asc']).draw();
            }
        });
    });
    
    function setCategory(cat) {
        $.ajax({
          url: "ajax/set_layanan.php",
          type: "post",
          data: 'kategori='+cat.value,
          dataType: 'json',
          success: function (response) {
            if (response.status) {
              $('#table-services').DataTable().ajax.reload();
            }else{
              $('#table-services').DataTable().ajax.reload();
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
             console.log(textStatus, errorThrown);
          }
      });
    }
</script>