<?php
require 'config.php';
session_start();
session_destroy();
setcookie('TOKEN', '', time() - (3600 * 168), '/');
header("Location: ".$domain."login");